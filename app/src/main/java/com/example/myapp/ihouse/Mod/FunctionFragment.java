package com.example.myapp.ihouse.mod;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RatingBar;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.util.ArrayList;


public class FunctionFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    NumberPicker np1;
    NumberPicker np2;
    NumberPicker np3;
    NumberPicker np4;
    NumberPicker np5;
    NumberPicker np6;
    protected PageFragmentCallbacks mCallbacks;
    private int mKey;
    Hweight hw = ModActivity.hweight;

    public static FunctionFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        FunctionFragment fragment = new FunctionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getInt(ARG_PAGE);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.functionfragment1, container, false);


        integers = new ArrayList<>();
        integers.add(0, -1);
        integers.add(1, -1);
        integers.add(2, -1);
        integers.add(3, -1);
        integers.add(4, -1);
        integers.add(5, -1);
        integers.add(6, -1);
        integers.add(7, -1);
        integers.add(8, -1);
        integers.add(9, -1);
        integers.add(10, -1);
        integers.add(11, -1);

        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.RatingBarId);
        final RatingBar ratingBar2 = (RatingBar) view.findViewById(R.id.RatingBarId2);
        final RatingBar ratingBar3 = (RatingBar) view.findViewById(R.id.RatingBarId3);
        final RatingBar ratingBar4 = (RatingBar) view.findViewById(R.id.RatingBarId4);
        final RatingBar ratingBar5 = (RatingBar) view.findViewById(R.id.RatingBarId5);
        final RatingBar ratingBar6 = (RatingBar) view.findViewById(R.id.RatingBarId6);
        final RatingBar ratingBar7 = (RatingBar) view.findViewById(R.id.RatingBarId7);
        final RatingBar ratingBar8 = (RatingBar) view.findViewById(R.id.RatingBarId8);
        final RatingBar ratingBar9 = (RatingBar) view.findViewById(R.id.RatingBarId9);
        final RatingBar ratingBar10 = (RatingBar) view.findViewById(R.id.RatingBarId10);

        ratingBar.setRating(hw.getLFPS());
        ratingBar2.setRating(hw.getLFHS());
        ratingBar3.setRating(hw.getLFSS());
        ratingBar4.setRating(hw.getLFDS());
        ratingBar5.setRating(hw.getEUES());
        ratingBar6.setRating(hw.getEUJS());
        ratingBar7.setRating(hw.getEUHS());
        ratingBar8.setRating(hw.getEULB());
        ratingBar9.setRating(hw.getLCPS());
        ratingBar10.setRating(hw.getLCSS());

        np1 = (NumberPicker) view.findViewById(R.id.np1);
        np2 = (NumberPicker) view.findViewById(R.id.np2);
        np3 = (NumberPicker) view.findViewById(R.id.np3);
        np4 = (NumberPicker) view.findViewById(R.id.np4);
        np5 = (NumberPicker) view.findViewById(R.id.np5);
        np6 = (NumberPicker) view.findViewById(R.id.np6);

        final String[] values = new String[]{"0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800", "1900", "2000"};

        np1.setMaxValue(values.length - 1);
        np1.setDisplayedValues(values);
        np1.setMinValue(0);
        np1.setValue(FindIndex(values, hw.getLFmin()));

        np2.setMaxValue(values.length - 1);
        np2.setMinValue(0);
        np2.setDisplayedValues(values);
        np2.setValue(FindIndex(values, hw.getLFmax()));

        np3.setMaxValue(values.length - 1);
        np3.setMinValue(0);
        np3.setDisplayedValues(values);
        np3.setValue(FindIndex(values, hw.getEUmin()));

        np4.setMaxValue(values.length - 1);
        np4.setMinValue(0);
        np4.setDisplayedValues(values);
        np4.setValue(FindIndex(values, hw.getEUmax()));

        np5.setMaxValue(values.length - 1);
        np5.setMinValue(0);
        np5.setDisplayedValues(values);
        np5.setValue(FindIndex(values, hw.getLCmin()));

        np6.setMaxValue(values.length - 1);
        np6.setMinValue(0);
        np6.setDisplayedValues(values);
        np6.setValue(FindIndex(values, hw.getLCmax()));


        np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setLFmin(Integer.valueOf(values[newVal]));
            }
        });

        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setLFmax(Integer.valueOf(values[newVal]));
            }
        });
        np3.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setEUmin(Integer.valueOf(values[newVal]));
            }
        });
        np4.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setEUmax(Integer.valueOf(values[newVal]));
            }
        });
        np5.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setLCmin(Integer.valueOf(values[newVal]));
            }
        });
        np6.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setLCmax(Integer.valueOf(values[newVal]));
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setLFPS((int) rating);

            }
        });

        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setLFHS((int) rating);
            }
        });

        ratingBar3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setLFSS((int) rating);

            }
        });

        ratingBar4.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setLFDS((int) rating);

            }
        });
        ratingBar5.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                hw.setEUES((int) rating);
            }
        });
        ratingBar6.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setEUJS((int) rating);

            }
        });
        ratingBar7.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setEUHS((int) rating);

            }
        });
        ratingBar8.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setEULB((int) rating);

            }
        });
        ratingBar9.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setLCPS((int) rating);

            }
        });
        ratingBar10.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setLCSS((int) rating);

            }
        });


        Button clear = (Button) view.findViewById(R.id.clear);

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar.setRating(0);
                hw.setLFPS(0);
            }
        });

        Button clear2 = (Button) view.findViewById(R.id.clear2);

        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar2.setRating(0);
                hw.setLFHS(0);
            }
        });

        Button clear3 = (Button) view.findViewById(R.id.clear3);

        clear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar3.setRating(0);
                hw.setLFSS(0);
            }
        });

        Button clear4 = (Button) view.findViewById(R.id.clear4);

        clear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar4.setRating(0);
                hw.setLFDS(0);
            }
        });

        Button clear5 = (Button) view.findViewById(R.id.clear5);

        clear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar5.setRating(0);
                hw.setEUES(0);
            }
        });

        Button clear6 = (Button) view.findViewById(R.id.clear6);

        clear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar6.setRating(0);
                hw.setEUJS(0);
            }
        });

        Button clear7 = (Button) view.findViewById(R.id.clear7);

        clear7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar7.setRating(0);
                hw.setEUHS(0);

            }
        });

        Button clear8 = (Button) view.findViewById(R.id.clear8);

        clear8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar8.setRating(0);
                hw.setEULB(0);

            }
        });

        Button clear9 = (Button) view.findViewById(R.id.clear9);

        clear9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar9.setRating(0);
                hw.setLCPS(0);
            }
        });

        Button clear10 = (Button) view.findViewById(R.id.clear10);

        clear10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar10.setRating(0);
                hw.setLCSS(0);
            }
        });


        return view;


    }

    public int FindIndex(String[] values, Integer num) {
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(num.toString())) {
                index = i;
            }

        }
        return index;
    }

}