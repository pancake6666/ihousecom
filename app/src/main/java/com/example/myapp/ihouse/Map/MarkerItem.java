package com.example.myapp.ihouse.map;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MarkerItem implements ClusterItem {
    public int houseId;
    public int price;
    public Bitmap bitmap;
    private LatLng latLng;
    public Double space;
    public boolean hide;
    public int type;

    //, int total, int price, BitmapDescriptor bitmapColor, String date, Double space, int x, int y
    public MarkerItem(double lat, double lng, boolean hide, int price, Bitmap bitmap, int houseId, int type) {
        this.price = price;
        this.bitmap = bitmap;
        this.hide = hide;
        latLng = new LatLng(lat, lng);
        this.houseId = houseId;
        this.type = type;
    }


    @Override
    public LatLng getPosition() {
        return this.latLng;
    }

}
