package com.example.myapp.ihouse.httpGet;

import java.util.ArrayList;

public class RealEstateDate implements java.io.Serializable {
    private static final long serialVersionUID = 4478940728555738198L;
    private ArrayList<RealEstateDateLs> ls;
    private String THSR_StationDistance;
    private Object River_Name;
    private String River_Distance;
    private String Address;
    private String B002_Distance;
    private String ml_MIDScore;
    private String Univ_Name;
    private String B002_ID;
    private String TRA_Name;
    private String ml_SportScore;
    private String L003_ID;
    private String E002_Distance;
    private String THSR_LID;
    private String ml_Score;
    private String L002_Distance;
    private String Fault_Order;
    private String B003_Name;
    private String ml_FWAYScore;
    private String ID;
    private String MRT_ID;
    private String Fault_Distance;
    private String ml_ELEScore;
    private String ParkR_ID;
    private String THSR_ID;
    private String E001_Distance;
    private String TotalFloor;
    private String Lon;
    private String ParkingType;
    private Object THSR_Name;
    private String ml_FireScore;
    private String HouseMaterials;
    private String River_Orders;
    private String A003_ID;
    private String HouseUse;
    private String L001_Distance;
    private Object Fway_Name;
    private String B001_Name;
    private Object Rzone;
    private String RealAddr;
    private String Fway_LineDistance;
    private String HouseRoom_3;
    private String HouseRoom_2;
    private String HouseRoom_1;
    private String THSR_Orders;
    private String Guards;
    private Object NonUrbanZone;
    private String ml_HouseScore;
    private String A004_Distance;
    private Object A003_Name;
    private String LandArea;
    private String HouseType;
    private String E002_Name;
    private String AddLoveCheck;
    private String HouseRoom_4;
    private String TRA_LineDistance;
    private String Univ_Distance;
    private String THSR_LOrders;
    private String B002_Name;
    private String ml_TRAScore;
    private String ParkB_ID;
    private String ml_DisasterScore;
    private String B003_ID;
    private String L002_ID;
    private String MRT_Orders;
    private String Univ_ID;
    private String ParkR_Distance;
    private String ml_MRTScore;
    private String A001_ID;
    private String ParkB_Name;
    private String Flood_ID;
    private String TransDate;
    private String TotalPrice;
    private String TransFloor;
    private String x;
    private String y;
    private String A002_Distance;
    private String Lat;
    private String E002_ID;
    private String L004_Distance;
    private String ml_HosScore;
    private String MRT_StationDistance;
    private String SoilLiq_ID;
    private Object NonUrbanLand;
    private String parkingLot;
    private String L002_Name;
    private String ZoneUse;
    private String ml_HighScore;
    private String TRA_LOrders;
    private String ml_AirScore;
    private String SoilLiq_Distance;
    private String MRT_LID;
    private String MRT_LOrders;
    private String Fway_EntranceDistance;
    private String TRA_LID;
    private String ParkingArea;
    private String ml_PostScore;
    private Object Fault_Name;
    private String ml_GasScore;
    private String L003_Name;
    private String Airport_Distance;
    private String A004_ID;
    private String THSR_LineDistance;
    private String TRA_Orders;
    private String ml_THSRScore;
    private String Fway_ID;
    private String Fway_Orders;
    private String TRA_StationDistance;
    private String ml_MarketScore;
    private String HouseArea;
    private String MRT_LineDistance;
    private String B001_ID;
    private String MRT_Name;
    private String ml_DieScore;
    private Object A002_Name;
    private String L004_ID;
    private String ParkR_Name;
    private String TRA_ID;
    private String Airport_ID;
    private String ParkingPrice;
    private String Price;
    private String Airport_Name;
    private String Pcode5_ID;
    private String L003_Distance;
    private String Fway_LOrders;
    private String A004_Name;
    private String HouseDate;
    private String TransType;
    private String L001_ID;
    private String house;
    private String ml_LibScore;
    private String A001_Distance;
    private String B001_Distance;
    private String A002_ID;
    private String ml_LeisureScore;
    private String ml_LifeScore;
    private String land;
    private String E001_ID;
    private String River_ID;
    private String Fway_LID;
    private String A003_Distance;
    private String Fault_ID;
    private String ml_DepartScore;
    private String B003_Distance;
    private String E001_Name;
    private String L004_Name;
    private String L001_Name;
    private String ml_EduScore;
    private String ml_ParkScore;
    private String A001_Name;
    private String ParkB_Distance;

    public String getTHSR_StationDistance() {
        return this.THSR_StationDistance;
    }

    public void setTHSR_StationDistance(String THSR_StationDistance) {
        this.THSR_StationDistance = THSR_StationDistance;
    }

    public Object getRiver_Name() {
        return this.River_Name;
    }

    public void setRiver_Name(Object River_Name) {
        this.River_Name = River_Name;
    }

    public String getRiver_Distance() {
        return this.River_Distance;
    }

    public void setRiver_Distance(String River_Distance) {
        this.River_Distance = River_Distance;
    }

    public String getAddress() {
        return this.Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getB002_Distance() {
        return this.B002_Distance;
    }

    public void setB002_Distance(String B002_Distance) {
        this.B002_Distance = B002_Distance;
    }

    public String getMl_MIDScore() {
        return this.ml_MIDScore;
    }

    public void setMl_MIDScore(String ml_MIDScore) {
        this.ml_MIDScore = ml_MIDScore;
    }

    public String getUniv_Name() {
        return this.Univ_Name;
    }

    public void setUniv_Name(String Univ_Name) {
        this.Univ_Name = Univ_Name;
    }

    public String getB002_ID() {
        return this.B002_ID;
    }

    public void setB002_ID(String B002_ID) {
        this.B002_ID = B002_ID;
    }

    public String getTRA_Name() {
        return this.TRA_Name;
    }

    public void setTRA_Name(String TRA_Name) {
        this.TRA_Name = TRA_Name;
    }

    public String getMl_SportScore() {
        return this.ml_SportScore;
    }

    public void setMl_SportScore(String ml_SportScore) {
        this.ml_SportScore = ml_SportScore;
    }

    public String getL003_ID() {
        return this.L003_ID;
    }

    public void setL003_ID(String L003_ID) {
        this.L003_ID = L003_ID;
    }

    public String getE002_Distance() {
        return this.E002_Distance;
    }

    public void setE002_Distance(String E002_Distance) {
        this.E002_Distance = E002_Distance;
    }

    public String getTHSR_LID() {
        return this.THSR_LID;
    }

    public void setTHSR_LID(String THSR_LID) {
        this.THSR_LID = THSR_LID;
    }

    public String getMl_Score() {
        return this.ml_Score;
    }

    public void setMl_Score(String ml_Score) {
        this.ml_Score = ml_Score;
    }

    public String getL002_Distance() {
        return this.L002_Distance;
    }

    public void setL002_Distance(String L002_Distance) {
        this.L002_Distance = L002_Distance;
    }

    public String getFault_Order() {
        return this.Fault_Order;
    }

    public void setFault_Order(String Fault_Order) {
        this.Fault_Order = Fault_Order;
    }

    public String getB003_Name() {
        return this.B003_Name;
    }

    public void setB003_Name(String B003_Name) {
        this.B003_Name = B003_Name;
    }

    public String getMl_FWAYScore() {
        return this.ml_FWAYScore;
    }

    public void setMl_FWAYScore(String ml_FWAYScore) {
        this.ml_FWAYScore = ml_FWAYScore;
    }

    public String getID() {
        return this.ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMRT_ID() {
        return this.MRT_ID;
    }

    public void setMRT_ID(String MRT_ID) {
        this.MRT_ID = MRT_ID;
    }

    public String getFault_Distance() {
        return this.Fault_Distance;
    }

    public void setFault_Distance(String Fault_Distance) {
        this.Fault_Distance = Fault_Distance;
    }

    public String getMl_ELEScore() {
        return this.ml_ELEScore;
    }

    public void setMl_ELEScore(String ml_ELEScore) {
        this.ml_ELEScore = ml_ELEScore;
    }

    public String getParkR_ID() {
        return this.ParkR_ID;
    }

    public void setParkR_ID(String ParkR_ID) {
        this.ParkR_ID = ParkR_ID;
    }

    public String getTHSR_ID() {
        return this.THSR_ID;
    }

    public void setTHSR_ID(String THSR_ID) {
        this.THSR_ID = THSR_ID;
    }

    public String getE001_Distance() {
        return this.E001_Distance;
    }

    public void setE001_Distance(String E001_Distance) {
        this.E001_Distance = E001_Distance;
    }

    public String getTotalFloor() {
        return this.TotalFloor;
    }

    public void setTotalFloor(String TotalFloor) {
        this.TotalFloor = TotalFloor;
    }

    public String getLon() {
        return this.Lon;
    }

    public void setLon(String Lon) {
        this.Lon = Lon;
    }

    public String getParkingType() {
        return this.ParkingType;
    }

    public void setParkingType(String ParkingType) {
        this.ParkingType = ParkingType;
    }

    public Object getTHSR_Name() {
        return this.THSR_Name;
    }

    public void setTHSR_Name(Object THSR_Name) {
        this.THSR_Name = THSR_Name;
    }

    public String getMl_FireScore() {
        return this.ml_FireScore;
    }

    public void setMl_FireScore(String ml_FireScore) {
        this.ml_FireScore = ml_FireScore;
    }

    public String getHouseMaterials() {
        return this.HouseMaterials;
    }

    public void setHouseMaterials(String HouseMaterials) {
        this.HouseMaterials = HouseMaterials;
    }

    public String getRiver_Orders() {
        return this.River_Orders;
    }

    public void setRiver_Orders(String River_Orders) {
        this.River_Orders = River_Orders;
    }

    public String getA003_ID() {
        return this.A003_ID;
    }

    public void setA003_ID(String A003_ID) {
        this.A003_ID = A003_ID;
    }

    public String getHouseUse() {
        return this.HouseUse;
    }

    public void setHouseUse(String HouseUse) {
        this.HouseUse = HouseUse;
    }

    public String getL001_Distance() {
        return this.L001_Distance;
    }

    public void setL001_Distance(String L001_Distance) {
        this.L001_Distance = L001_Distance;
    }

    public Object getFway_Name() {
        return this.Fway_Name;
    }

    public void setFway_Name(Object Fway_Name) {
        this.Fway_Name = Fway_Name;
    }

    public String getB001_Name() {
        return this.B001_Name;
    }

    public void setB001_Name(String B001_Name) {
        this.B001_Name = B001_Name;
    }

    public Object getRzone() {
        return this.Rzone;
    }

    public void setRzone(Object Rzone) {
        this.Rzone = Rzone;
    }

    public String getRealAddr() {
        return this.RealAddr;
    }

    public void setRealAddr(String RealAddr) {
        this.RealAddr = RealAddr;
    }

    public String getFway_LineDistance() {
        return this.Fway_LineDistance;
    }

    public void setFway_LineDistance(String Fway_LineDistance) {
        this.Fway_LineDistance = Fway_LineDistance;
    }

    public String getHouseRoom_3() {
        return this.HouseRoom_3;
    }

    public void setHouseRoom_3(String HouseRoom_3) {
        this.HouseRoom_3 = HouseRoom_3;
    }

    public String getHouseRoom_2() {
        return this.HouseRoom_2;
    }

    public void setHouseRoom_2(String HouseRoom_2) {
        this.HouseRoom_2 = HouseRoom_2;
    }

    public String getHouseRoom_1() {
        return this.HouseRoom_1;
    }

    public void setHouseRoom_1(String HouseRoom_1) {
        this.HouseRoom_1 = HouseRoom_1;
    }

    public String getTHSR_Orders() {
        return this.THSR_Orders;
    }

    public void setTHSR_Orders(String THSR_Orders) {
        this.THSR_Orders = THSR_Orders;
    }

    public String getGuards() {
        return this.Guards;
    }

    public void setGuards(String Guards) {
        this.Guards = Guards;
    }

    public Object getNonUrbanZone() {
        return this.NonUrbanZone;
    }

    public void setNonUrbanZone(Object NonUrbanZone) {
        this.NonUrbanZone = NonUrbanZone;
    }

    public String getMl_HouseScore() {
        return this.ml_HouseScore;
    }

    public void setMl_HouseScore(String ml_HouseScore) {
        this.ml_HouseScore = ml_HouseScore;
    }

    public String getA004_Distance() {
        return this.A004_Distance;
    }

    public void setA004_Distance(String A004_Distance) {
        this.A004_Distance = A004_Distance;
    }

    public Object getA003_Name() {
        return this.A003_Name;
    }

    public void setA003_Name(Object A003_Name) {
        this.A003_Name = A003_Name;
    }

    public String getLandArea() {
        return this.LandArea;
    }

    public void setLandArea(String LandArea) {
        this.LandArea = LandArea;
    }

    public String getHouseType() {
        return this.HouseType;
    }

    public void setHouseType(String HouseType) {
        this.HouseType = HouseType;
    }

    public String getE002_Name() {
        return this.E002_Name;
    }

    public void setE002_Name(String E002_Name) {
        this.E002_Name = E002_Name;
    }

    public String getAddLoveCheck() {
        return this.AddLoveCheck;
    }

    public void setAddLoveCheck(String AddLoveCheck) {
        this.AddLoveCheck = AddLoveCheck;
    }

    public String getHouseRoom_4() {
        return this.HouseRoom_4;
    }

    public void setHouseRoom_4(String HouseRoom_4) {
        this.HouseRoom_4 = HouseRoom_4;
    }

    public String getTRA_LineDistance() {
        return this.TRA_LineDistance;
    }

    public void setTRA_LineDistance(String TRA_LineDistance) {
        this.TRA_LineDistance = TRA_LineDistance;
    }

    public String getUniv_Distance() {
        return this.Univ_Distance;
    }

    public void setUniv_Distance(String Univ_Distance) {
        this.Univ_Distance = Univ_Distance;
    }

    public String getTHSR_LOrders() {
        return this.THSR_LOrders;
    }

    public void setTHSR_LOrders(String THSR_LOrders) {
        this.THSR_LOrders = THSR_LOrders;
    }

    public String getB002_Name() {
        return this.B002_Name;
    }

    public void setB002_Name(String B002_Name) {
        this.B002_Name = B002_Name;
    }

    public String getMl_TRAScore() {
        return this.ml_TRAScore;
    }

    public void setMl_TRAScore(String ml_TRAScore) {
        this.ml_TRAScore = ml_TRAScore;
    }

    public String getParkB_ID() {
        return this.ParkB_ID;
    }

    public void setParkB_ID(String ParkB_ID) {
        this.ParkB_ID = ParkB_ID;
    }

    public String getMl_DisasterScore() {
        return this.ml_DisasterScore;
    }

    public void setMl_DisasterScore(String ml_DisasterScore) {
        this.ml_DisasterScore = ml_DisasterScore;
    }

    public String getB003_ID() {
        return this.B003_ID;
    }

    public void setB003_ID(String B003_ID) {
        this.B003_ID = B003_ID;
    }

    public String getL002_ID() {
        return this.L002_ID;
    }

    public void setL002_ID(String L002_ID) {
        this.L002_ID = L002_ID;
    }

    public String getMRT_Orders() {
        return this.MRT_Orders;
    }

    public void setMRT_Orders(String MRT_Orders) {
        this.MRT_Orders = MRT_Orders;
    }

    public String getUniv_ID() {
        return this.Univ_ID;
    }

    public void setUniv_ID(String Univ_ID) {
        this.Univ_ID = Univ_ID;
    }

    public String getParkR_Distance() {
        return this.ParkR_Distance;
    }

    public void setParkR_Distance(String ParkR_Distance) {
        this.ParkR_Distance = ParkR_Distance;
    }

    public String getMl_MRTScore() {
        return this.ml_MRTScore;
    }

    public void setMl_MRTScore(String ml_MRTScore) {
        this.ml_MRTScore = ml_MRTScore;
    }

    public String getA001_ID() {
        return this.A001_ID;
    }

    public void setA001_ID(String A001_ID) {
        this.A001_ID = A001_ID;
    }

    public String getParkB_Name() {
        return this.ParkB_Name;
    }

    public void setParkB_Name(String ParkB_Name) {
        this.ParkB_Name = ParkB_Name;
    }

    public String getFlood_ID() {
        return this.Flood_ID;
    }

    public void setFlood_ID(String Flood_ID) {
        this.Flood_ID = Flood_ID;
    }

    public String getTransDate() {
        return this.TransDate;
    }

    public void setTransDate(String TransDate) {
        this.TransDate = TransDate;
    }

    public String getTotalPrice() {
        return this.TotalPrice;
    }

    public void setTotalPrice(String TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    public String getTransFloor() {
        return this.TransFloor;
    }

    public void setTransFloor(String TransFloor) {
        this.TransFloor = TransFloor;
    }

    public String getX() {
        return this.x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return this.y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getA002_Distance() {
        return this.A002_Distance;
    }

    public void setA002_Distance(String A002_Distance) {
        this.A002_Distance = A002_Distance;
    }

    public String getLat() {
        return this.Lat;
    }

    public void setLat(String Lat) {
        this.Lat = Lat;
    }

    public String getE002_ID() {
        return this.E002_ID;
    }

    public void setE002_ID(String E002_ID) {
        this.E002_ID = E002_ID;
    }

    public String getL004_Distance() {
        return this.L004_Distance;
    }

    public void setL004_Distance(String L004_Distance) {
        this.L004_Distance = L004_Distance;
    }

    public String getMl_HosScore() {
        return this.ml_HosScore;
    }

    public void setMl_HosScore(String ml_HosScore) {
        this.ml_HosScore = ml_HosScore;
    }

    public String getMRT_StationDistance() {
        return this.MRT_StationDistance;
    }

    public void setMRT_StationDistance(String MRT_StationDistance) {
        this.MRT_StationDistance = MRT_StationDistance;
    }

    public String getSoilLiq_ID() {
        return this.SoilLiq_ID;
    }

    public void setSoilLiq_ID(String SoilLiq_ID) {
        this.SoilLiq_ID = SoilLiq_ID;
    }

    public Object getNonUrbanLand() {
        return this.NonUrbanLand;
    }

    public void setNonUrbanLand(Object NonUrbanLand) {
        this.NonUrbanLand = NonUrbanLand;
    }

    public String getParkingLot() {
        return this.parkingLot;
    }

    public void setParkingLot(String parkingLot) {
        this.parkingLot = parkingLot;
    }

    public String getL002_Name() {
        return this.L002_Name;
    }

    public void setL002_Name(String L002_Name) {
        this.L002_Name = L002_Name;
    }

    public String getZoneUse() {
        return this.ZoneUse;
    }

    public void setZoneUse(String ZoneUse) {
        this.ZoneUse = ZoneUse;
    }

    public String getMl_HighScore() {
        return this.ml_HighScore;
    }

    public void setMl_HighScore(String ml_HighScore) {
        this.ml_HighScore = ml_HighScore;
    }

    public String getTRA_LOrders() {
        return this.TRA_LOrders;
    }

    public void setTRA_LOrders(String TRA_LOrders) {
        this.TRA_LOrders = TRA_LOrders;
    }

    public String getMl_AirScore() {
        return this.ml_AirScore;
    }

    public void setMl_AirScore(String ml_AirScore) {
        this.ml_AirScore = ml_AirScore;
    }

    public String getSoilLiq_Distance() {
        return this.SoilLiq_Distance;
    }

    public void setSoilLiq_Distance(String SoilLiq_Distance) {
        this.SoilLiq_Distance = SoilLiq_Distance;
    }

    public String getMRT_LID() {
        return this.MRT_LID;
    }

    public void setMRT_LID(String MRT_LID) {
        this.MRT_LID = MRT_LID;
    }

    public String getMRT_LOrders() {
        return this.MRT_LOrders;
    }

    public void setMRT_LOrders(String MRT_LOrders) {
        this.MRT_LOrders = MRT_LOrders;
    }

    public String getFway_EntranceDistance() {
        return this.Fway_EntranceDistance;
    }

    public void setFway_EntranceDistance(String Fway_EntranceDistance) {
        this.Fway_EntranceDistance = Fway_EntranceDistance;
    }

    public String getTRA_LID() {
        return this.TRA_LID;
    }

    public void setTRA_LID(String TRA_LID) {
        this.TRA_LID = TRA_LID;
    }

    public String getParkingArea() {
        return this.ParkingArea;
    }

    public void setParkingArea(String ParkingArea) {
        this.ParkingArea = ParkingArea;
    }

    public String getMl_PostScore() {
        return this.ml_PostScore;
    }

    public void setMl_PostScore(String ml_PostScore) {
        this.ml_PostScore = ml_PostScore;
    }

    public Object getFault_Name() {
        return this.Fault_Name;
    }

    public void setFault_Name(Object Fault_Name) {
        this.Fault_Name = Fault_Name;
    }

    public String getMl_GasScore() {
        return this.ml_GasScore;
    }

    public void setMl_GasScore(String ml_GasScore) {
        this.ml_GasScore = ml_GasScore;
    }

    public String getL003_Name() {
        return this.L003_Name;
    }

    public void setL003_Name(String L003_Name) {
        this.L003_Name = L003_Name;
    }

    public String getAirport_Distance() {
        return this.Airport_Distance;
    }

    public void setAirport_Distance(String Airport_Distance) {
        this.Airport_Distance = Airport_Distance;
    }

    public String getA004_ID() {
        return this.A004_ID;
    }

    public void setA004_ID(String A004_ID) {
        this.A004_ID = A004_ID;
    }

    public String getTHSR_LineDistance() {
        return this.THSR_LineDistance;
    }

    public void setTHSR_LineDistance(String THSR_LineDistance) {
        this.THSR_LineDistance = THSR_LineDistance;
    }

    public String getTRA_Orders() {
        return this.TRA_Orders;
    }

    public void setTRA_Orders(String TRA_Orders) {
        this.TRA_Orders = TRA_Orders;
    }

    public String getMl_THSRScore() {
        return this.ml_THSRScore;
    }

    public void setMl_THSRScore(String ml_THSRScore) {
        this.ml_THSRScore = ml_THSRScore;
    }

    public String getFway_ID() {
        return this.Fway_ID;
    }

    public void setFway_ID(String Fway_ID) {
        this.Fway_ID = Fway_ID;
    }

    public String getFway_Orders() {
        return this.Fway_Orders;
    }

    public void setFway_Orders(String Fway_Orders) {
        this.Fway_Orders = Fway_Orders;
    }

    public String getTRA_StationDistance() {
        return this.TRA_StationDistance;
    }

    public void setTRA_StationDistance(String TRA_StationDistance) {
        this.TRA_StationDistance = TRA_StationDistance;
    }

    public String getMl_MarketScore() {
        return this.ml_MarketScore;
    }

    public void setMl_MarketScore(String ml_MarketScore) {
        this.ml_MarketScore = ml_MarketScore;
    }

    public String getHouseArea() {
        return this.HouseArea;
    }

    public void setHouseArea(String HouseArea) {
        this.HouseArea = HouseArea;
    }

    public String getMRT_LineDistance() {
        return this.MRT_LineDistance;
    }

    public void setMRT_LineDistance(String MRT_LineDistance) {
        this.MRT_LineDistance = MRT_LineDistance;
    }

    public String getB001_ID() {
        return this.B001_ID;
    }

    public void setB001_ID(String B001_ID) {
        this.B001_ID = B001_ID;
    }

    public String getMRT_Name() {
        return this.MRT_Name;
    }

    public void setMRT_Name(String MRT_Name) {
        this.MRT_Name = MRT_Name;
    }

    public String getMl_DieScore() {
        return this.ml_DieScore;
    }

    public void setMl_DieScore(String ml_DieScore) {
        this.ml_DieScore = ml_DieScore;
    }

    public Object getA002_Name() {
        return this.A002_Name;
    }

    public void setA002_Name(Object A002_Name) {
        this.A002_Name = A002_Name;
    }

    public String getL004_ID() {
        return this.L004_ID;
    }

    public void setL004_ID(String L004_ID) {
        this.L004_ID = L004_ID;
    }

    public String getParkR_Name() {
        return this.ParkR_Name;
    }

    public void setParkR_Name(String ParkR_Name) {
        this.ParkR_Name = ParkR_Name;
    }

    public String getTRA_ID() {
        return this.TRA_ID;
    }

    public void setTRA_ID(String TRA_ID) {
        this.TRA_ID = TRA_ID;
    }

    public String getAirport_ID() {
        return this.Airport_ID;
    }

    public void setAirport_ID(String Airport_ID) {
        this.Airport_ID = Airport_ID;
    }

    public String getParkingPrice() {
        return this.ParkingPrice;
    }

    public void setParkingPrice(String ParkingPrice) {
        this.ParkingPrice = ParkingPrice;
    }

    public String getPrice() {
        return this.Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getAirport_Name() {
        return this.Airport_Name;
    }

    public void setAirport_Name(String Airport_Name) {
        this.Airport_Name = Airport_Name;
    }

    public String getPcode5_ID() {
        return this.Pcode5_ID;
    }

    public void setPcode5_ID(String Pcode5_ID) {
        this.Pcode5_ID = Pcode5_ID;
    }

    public String getL003_Distance() {
        return this.L003_Distance;
    }

    public void setL003_Distance(String L003_Distance) {
        this.L003_Distance = L003_Distance;
    }

    public String getFway_LOrders() {
        return this.Fway_LOrders;
    }

    public void setFway_LOrders(String Fway_LOrders) {
        this.Fway_LOrders = Fway_LOrders;
    }

    public String getA004_Name() {
        return this.A004_Name;
    }

    public void setA004_Name(String A004_Name) {
        this.A004_Name = A004_Name;
    }

    public String getHouseDate() {
        return this.HouseDate;
    }

    public void setHouseDate(String HouseDate) {
        this.HouseDate = HouseDate;
    }

    public String getTransType() {
        return this.TransType;
    }

    public void setTransType(String TransType) {
        this.TransType = TransType;
    }

    public String getL001_ID() {
        return this.L001_ID;
    }

    public void setL001_ID(String L001_ID) {
        this.L001_ID = L001_ID;
    }

    public String getHouse() {
        return this.house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getMl_LibScore() {
        return this.ml_LibScore;
    }

    public void setMl_LibScore(String ml_LibScore) {
        this.ml_LibScore = ml_LibScore;
    }

    public String getA001_Distance() {
        return this.A001_Distance;
    }

    public void setA001_Distance(String A001_Distance) {
        this.A001_Distance = A001_Distance;
    }

    public String getB001_Distance() {
        return this.B001_Distance;
    }

    public void setB001_Distance(String B001_Distance) {
        this.B001_Distance = B001_Distance;
    }

    public String getA002_ID() {
        return this.A002_ID;
    }

    public void setA002_ID(String A002_ID) {
        this.A002_ID = A002_ID;
    }

    public String getMl_LeisureScore() {
        return this.ml_LeisureScore;
    }

    public void setMl_LeisureScore(String ml_LeisureScore) {
        this.ml_LeisureScore = ml_LeisureScore;
    }

    public String getMl_LifeScore() {
        return this.ml_LifeScore;
    }

    public void setMl_LifeScore(String ml_LifeScore) {
        this.ml_LifeScore = ml_LifeScore;
    }

    public String getLand() {
        return this.land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getE001_ID() {
        return this.E001_ID;
    }

    public void setE001_ID(String E001_ID) {
        this.E001_ID = E001_ID;
    }

    public String getRiver_ID() {
        return this.River_ID;
    }

    public void setRiver_ID(String River_ID) {
        this.River_ID = River_ID;
    }

    public String getFway_LID() {
        return this.Fway_LID;
    }

    public void setFway_LID(String Fway_LID) {
        this.Fway_LID = Fway_LID;
    }

    public String getA003_Distance() {
        return this.A003_Distance;
    }

    public void setA003_Distance(String A003_Distance) {
        this.A003_Distance = A003_Distance;
    }

    public String getFault_ID() {
        return this.Fault_ID;
    }

    public void setFault_ID(String Fault_ID) {
        this.Fault_ID = Fault_ID;
    }

    public String getMl_DepartScore() {
        return this.ml_DepartScore;
    }

    public void setMl_DepartScore(String ml_DepartScore) {
        this.ml_DepartScore = ml_DepartScore;
    }

    public String getB003_Distance() {
        return this.B003_Distance;
    }

    public void setB003_Distance(String B003_Distance) {
        this.B003_Distance = B003_Distance;
    }

    public String getE001_Name() {
        return this.E001_Name;
    }

    public void setE001_Name(String E001_Name) {
        this.E001_Name = E001_Name;
    }

    public String getL004_Name() {
        return this.L004_Name;
    }

    public void setL004_Name(String L004_Name) {
        this.L004_Name = L004_Name;
    }

    public String getL001_Name() {
        return this.L001_Name;
    }

    public void setL001_Name(String L001_Name) {
        this.L001_Name = L001_Name;
    }

    public String getMl_EduScore() {
        return this.ml_EduScore;
    }

    public void setMl_EduScore(String ml_EduScore) {
        this.ml_EduScore = ml_EduScore;
    }

    public String getMl_ParkScore() {
        return this.ml_ParkScore;
    }

    public void setMl_ParkScore(String ml_ParkScore) {
        this.ml_ParkScore = ml_ParkScore;
    }

    public String getA001_Name() {
        return this.A001_Name;
    }

    public void setA001_Name(String A001_Name) {
        this.A001_Name = A001_Name;
    }

    public String getParkB_Distance() {
        return this.ParkB_Distance;
    }

    public void setParkB_Distance(String ParkB_Distance) {
        this.ParkB_Distance = ParkB_Distance;
    }


    public ArrayList<RealEstateDateLs> getLs() {
        return ls;
    }

    public void setLs(ArrayList<RealEstateDateLs> ls) {
        this.ls = ls;
    }
}
