package com.example.myapp.ihouse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.myapp.ihouse.contact_us.Contactus;
import com.example.myapp.ihouse.favorite.MyLoveActivity;
import com.example.myapp.ihouse.map.MapsActivity;
import com.example.myapp.ihouse.mod.ModActivity;

public class MainActivity extends AppCompatActivity implements AnimationListener {
    Animation slideLeft, slideRight, h_move, i_move, ii_move;

    ImageView iB, iB2, iB3, iB4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);

        iB = (ImageView) findViewById(R.id.imageButton);
        iB2 = (ImageView) findViewById(R.id.imageButton2);
        iB3 = (ImageView) findViewById(R.id.imageButton3);
        iB4 = (ImageView) findViewById(R.id.imageButton4);

        slideLeft = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_left);
        slideLeft.setAnimationListener(this);
        slideRight = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_right);
        slideRight.setAnimationListener(this);


        iB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent is what you use to start another activity
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        });

        iB2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyLoveActivity.class));
            }
        });

        iB3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ModActivity.class));
            }
        });

        iB4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Contactus.class));
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();


        iB.setVisibility(View.VISIBLE);
        iB.startAnimation(slideLeft);
        iB2.setVisibility(View.VISIBLE);
        iB2.startAnimation(slideRight);
        iB3.setVisibility(View.VISIBLE);
        iB3.startAnimation(slideLeft);
        iB4.setVisibility(View.VISIBLE);
        iB4.startAnimation(slideRight);

    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
}