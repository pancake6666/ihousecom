package com.example.myapp.ihouse.favorite;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.httpGet.MyLoveSample;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener, CompoundButton.OnCheckedChangeListener {

    private List<MyLoveSample> mItems;
    private Listener mListener;


    public CardAdapter(List<MyLoveSample> items, Listener listener) {
        if (items == null) {
            items = new ArrayList<>();
        }
        mItems = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.mylove_view_card_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        MyLoveSample loveItem = mItems.get(i);
        System.out.println(loveItem.getMl_address());
        viewHolder.tvAddress.setText(loveItem.getMl_address());
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date(loveItem.getMl_time());
        viewHolder.tvTime.setText(formatter.format(date));
        viewHolder.tvScore.setText(loveItem.getMl_Score());
        System.out.println(loveItem.getMl_type());
        if (Boolean.parseBoolean(loveItem.getMl_type())) {
            viewHolder.view.setBackgroundColor(Color.BLUE);
        } else {
            viewHolder.view.setBackgroundColor(Color.RED);
        }


        if (mListener != null) {
            viewHolder.comparebtn.setTag(loveItem);
            viewHolder.comparebtn.setOnClickListener(this);

            viewHolder.analButton.setTag(loveItem);
            viewHolder.analButton.setOnClickListener(this);

            viewHolder.delete.setTag(loveItem);
            viewHolder.delete.setOnClickListener(this);

            viewHolder.cardView.setTag(loveItem);
            viewHolder.cardView.setOnClickListener(this);
            viewHolder.cardView.setOnLongClickListener(this);

        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_view:
                if (v instanceof CardView) {
                    MyLoveSample myLoveItem = (MyLoveSample) v.getTag();
                    mListener.onItemClicked(myLoveItem);
                }
                break;
            case R.id.comparebtn:
                if (v instanceof Button) {
                    MyLoveSample myLoveItem = (MyLoveSample) v.getTag();
                    mListener.onButtonCompare(myLoveItem);
                }
                break;
            case R.id.deletebtn:
                if (v instanceof Button) {
                    MyLoveSample myLoveItem = (MyLoveSample) v.getTag();
                    int type = (Boolean.parseBoolean(myLoveItem.getMl_type()) ? 1 : 0);
                    mListener.onButtonClicked(Integer.parseInt(myLoveItem.getMl_id()), Integer.parseInt(myLoveItem.getMl_rid()), type);
                }
                break;
            case R.id.analbtn:
                if (v instanceof Button) {
                    MyLoveSample myLoveItem = (MyLoveSample) v.getTag();
                    mListener.onButtonAnalysis(myLoveItem);
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (view instanceof CardView) {
            MyLoveSample myLoveItem = (MyLoveSample) view.getTag();
            mListener.onItemLongClicked(myLoveItem);
        }
        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton instanceof CheckBox) {
            Long id = (Long) compoundButton.getTag();
            mListener.onChecked(id, b);
        }
    }

    public List<MyLoveSample> getItems() {
        return mItems;
    }


    public interface Listener {
        void onItemClicked(MyLoveSample myLoveItem);

        void onChecked(Long id, boolean b);

        void onItemLongClicked(MyLoveSample myLoveItem);

        void onButtonClicked(int Mlid, int realid, int type);

        void onButtonCompare(MyLoveSample myLoneItem);

        void onButtonAnalysis(MyLoveSample myLoveItem);


    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvType;
        public TextView tvAddress;
        public TextView tvTime;
        public CardView cardView;
        public Button analButton;
        public Button comparebtn;
        public View view;
        public TextView tvScore;
        public Button delete;

        public ViewHolder(View itemView) {
            super(itemView);
            tvType = (TextView) itemView.findViewById(R.id.type);
            comparebtn = (Button) itemView.findViewById(R.id.comparebtn);
            tvAddress = (TextView) itemView.findViewById(R.id.address);
            analButton = (Button) itemView.findViewById(R.id.analbtn);
            view = itemView.findViewById(R.id.typeColor);
            tvTime = (TextView) itemView.findViewById(R.id.addtime);
            tvScore = (TextView) itemView.findViewById(R.id.score);
            delete = (Button) itemView.findViewById(R.id.deletebtn);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }


}
