package com.example.myapp.ihouse.register;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

/**
 * Created by changyuen on 2016/9/4.
 */
public class StepFunctionPage extends Page {
    public static String FUNC_DIS_MIN = "func_dis_min";
    public static String FUNC_DIS_MAX = "func_dis_max";
    public static String FUNC_DIS_MIN_VAL = "func_dis_min_v";
    public static String FUNC_DIS_MAX_VAL = "func_dis_max_v";
    public static String POST_STAR = "post_star";
    public static String HOS_STAR = "hos_star";
    public static String MARKET_STAR = "market_star";
    public static String DEPART_STAR = "depart_star";

    public static String EDU_DIS_MIN = "edu_dis_min";
    public static String EDU_DIS_MAX = "edu_dis_max";
    public static String EDU_DIS_MIN_VAL = "edu_dis_min_v";
    public static String EDU_DIS_MAX_VAL = "edu_dis_max_v";
    public static String JS_STAR = "js_star";
    public static String JC_STAR = "jc_star";
    public static String JK_STAR = "jk_star";
    public static String LIB_STAR = "lib_star";

    public static String LEI_DIS_MIN = "lei_dis_min";
    public static String LEI_DIS_MAX = "lei_dis_max";
    public static String LEI_DIS_MIN_VAL = "lei_dis_min_v";
    public static String LEI_DIS_MAX_VAL = "lei_dis_max_v";
    public static String PARK_STAR = "part_star";
    public static String SPORT_STAR = "sport_star";

    public StepFunctionPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return FunctionFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        setFuncdata();
        dest.add(new ReviewItem("項目", "生活機能", getKey(), -1));
        dest.add(new ReviewItem("生活", String.format("距離:%d~%d 權重: 郵局:%d,醫院:%d,大賣場:%d,百貨公司:%d"
                , mData.getInt(FUNC_DIS_MIN), mData.getInt(FUNC_DIS_MAX)
                , mData.getInt(POST_STAR), mData.getInt(HOS_STAR)
                , mData.getInt(MARKET_STAR), mData.getInt(DEPART_STAR)), getKey(), -1));
        dest.add(new ReviewItem("教育", String.format("距離:%d~%d 權重: 國小:%d,國中:%d,高中:%d,圖書館:%d"
                , mData.getInt(EDU_DIS_MIN), mData.getInt(EDU_DIS_MAX)
                , mData.getInt(JS_STAR), mData.getInt(JC_STAR)
                , mData.getInt(JK_STAR), mData.getInt(LIB_STAR)), getKey(), -1));
        dest.add(new ReviewItem("休閒", String.format("距離:%d~%d 權重: 公園:%d,運動中心:%d"
                , mData.getInt(LEI_DIS_MIN), mData.getInt(LEI_DIS_MAX)
                , mData.getInt(PARK_STAR), mData.getInt(SPORT_STAR)), getKey(), -1));
    }


    @Override
    public boolean isCompleted() {
        return mData.getInt(StepFunctionPage.FUNC_DIS_MAX) > mData.getInt(StepFunctionPage.FUNC_DIS_MIN) &&
                mData.getInt(StepFunctionPage.EDU_DIS_MAX) > mData.getInt(StepFunctionPage.EDU_DIS_MIN) &&
                mData.getInt(StepFunctionPage.LEI_DIS_MAX) > mData.getInt(StepFunctionPage.LEI_DIS_MIN) &&
                mData.getInt(StepFunctionPage.POST_STAR) != 0 &&
                mData.getInt(StepFunctionPage.HOS_STAR) != 0 &&
                mData.getInt(StepFunctionPage.MARKET_STAR) != 0 &&
                mData.getInt(StepFunctionPage.DEPART_STAR) != 0 &&
                mData.getInt(StepFunctionPage.JS_STAR) != 0 &&
                mData.getInt(StepFunctionPage.JC_STAR) != 0 &&
                mData.getInt(StepFunctionPage.JK_STAR) != 0 &&
                mData.getInt(StepFunctionPage.LIB_STAR) != 0 &&
                mData.getInt(StepFunctionPage.PARK_STAR) != 0 &&
                mData.getInt(StepFunctionPage.SPORT_STAR) != 0;
    }

    public void setFuncdata() {
//        funcdata = new ArrayList<>();
//        funcdata.add(mData.getInt(FUNC_DIS_MIN));
//        funcdata.add(mData.getInt(FUNC_DIS_MAX));
//        funcdata.add(mData.getInt(POST_STAR));
//        funcdata.add(mData.getInt(HOS_STAR));
//        funcdata.add(mData.getInt(MARKET_STAR));
//        funcdata.add(mData.getInt(DEPART_STAR));
//        funcdata.add(mData.getInt(EDU_DIS_MIN));
//        funcdata.add(mData.getInt(EDU_DIS_MAX));
//        funcdata.add(mData.getInt(JS_STAR));
//        funcdata.add(mData.getInt(JC_STAR));
//        funcdata.add(mData.getInt(JK_STAR));
//        funcdata.add(mData.getInt(LIB_STAR));
//        funcdata.add(mData.getInt(LEI_DIS_MIN));
//        funcdata.add(mData.getInt(LEI_DIS_MAX));
//        funcdata.add(mData.getInt(PARK_STAR));
//        funcdata.add(mData.getInt(SPORT_STAR));

        StepActivity.hweight.setLFmin(mData.getInt(FUNC_DIS_MIN));
        StepActivity.hweight.setLFmax(mData.getInt(FUNC_DIS_MAX));
        StepActivity.hweight.setLFPS(mData.getInt(POST_STAR));
        StepActivity.hweight.setLFHS(mData.getInt(HOS_STAR));
        StepActivity.hweight.setLFSS(mData.getInt(MARKET_STAR));
        StepActivity.hweight.setLFDS(mData.getInt(DEPART_STAR));
        StepActivity.hweight.setEUmin(mData.getInt(EDU_DIS_MIN));
        StepActivity.hweight.setEUmax(mData.getInt(EDU_DIS_MAX));
        StepActivity.hweight.setEUES(mData.getInt(JS_STAR));
        StepActivity.hweight.setEUJS(mData.getInt(JC_STAR));
        StepActivity.hweight.setEUHS(mData.getInt(JK_STAR));
        StepActivity.hweight.setEULB(mData.getInt(LIB_STAR));
        StepActivity.hweight.setLCmin(mData.getInt(LEI_DIS_MIN));
        StepActivity.hweight.setLCmax(mData.getInt(LEI_DIS_MAX));
        StepActivity.hweight.setLCPS(mData.getInt(PARK_STAR));
        StepActivity.hweight.setLCSS(mData.getInt(SPORT_STAR));

    }
}
