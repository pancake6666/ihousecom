package com.example.myapp.ihouse.analysis;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.MyData;
import com.example.myapp.ihouse.httpGet.MyLoveSample;
import com.example.myapp.ihouse.httpGet.Part2;
import com.example.myapp.ihouse.httpGet.RealEstateDate;

import java.util.ArrayList;


public class LifeTotal_ExpListAdapter extends BaseExpandableListAdapter {

    private ArrayList<ArrayList<String>> mGroups;
    private Context mContext;
    private ArrayList<String> mGroupsName;
    public static RealEstateDate realEstateDate;
    public static MyData myData;
    public static Part2 part2;
    public static MyData bySample;
    public static RealHouse realHouse;
    public static MyLoveSample myLoveSample;
    public static Hweight hweight;

    public LifeTotal_ExpListAdapter(Context context, ArrayList<ArrayList<String>> groups, ArrayList<String> mGroupsName) {
        mContext = context;
        mGroups = groups;
        this.mGroupsName = mGroupsName;
    }


    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mGroups.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mGroups.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group, null);
        }

        if (isExpanded) {

        } else {
        }
        TextView textGroup = (TextView) convertView.findViewById(R.id.textGroup);
        textGroup.setText(mGroupsName.get(groupPosition));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.lifetotal_child, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.lifetotaltv1);
        RatingBar bar = (RatingBar) convertView.findViewById(R.id.ratingBar);
        bar.setClickable(false);
        bar.setStepSize(1);
        TextView distance = (TextView) convertView.findViewById(R.id.lifetotalideald);
        TextView realDis = (TextView) convertView.findViewById(R.id.lifetotalreald);
        realDis.setTextColor(Color.BLACK);
        textView.setText(mGroups.get(groupPosition).get(childPosition));
        textView.setTextColor(Color.BLACK);
        if (Activity_Analysis.realEstateDate != null) {
            if (groupPosition == 0) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getMRTstar());
                    distance.setText(String.format("%d~%d", hweight.getMRTmin(), hweight.getMRTmax()));
                    if (realEstateDate.getMRT_StationDistance().equals("999") || realEstateDate.getMRT_StationDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getMRT_StationDistance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getTRAstar());
                    distance.setText(String.format("%d~%d", hweight.getTRAmin(), hweight.getTRAmax()));
                    if (realEstateDate.getTRA_StationDistance().equals("999") || realEstateDate.getTRA_StationDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getTRA_StationDistance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getTHSRstar());
                    distance.setText(String.format("%d~%d", hweight.getTHSRmin(), hweight.getTHSRmax()));
                    if (realEstateDate.getTHSR_StationDistance().equals("999") || realEstateDate.getTHSR_StationDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getTHSR_StationDistance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getFwaystar());
                    distance.setText(String.format("%d~%d", hweight.getFwaymin(), hweight.getFwaymax()));
                    if (realEstateDate.getFway_EntranceDistance().equals("999") || realEstateDate.getFway_EntranceDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getFway_EntranceDistance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 1) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getLFPS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (realEstateDate.getL003_Distance().equals("999") || realEstateDate.getL003_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getL003_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getLFHS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (realEstateDate.getL004_Distance().equals("999") || realEstateDate.getL004_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getL004_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getLFSS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (realEstateDate.getL001_Distance().equals("999") || realEstateDate.getL001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getL001_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getLFDS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (realEstateDate.getL002_Distance().equals("999") || realEstateDate.getL002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getL002_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 2) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getEUES());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (realEstateDate.getA001_Distance().equals("999") || realEstateDate.getA001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getA001_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getEUJS());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (realEstateDate.getA002_Distance().equals("999") || realEstateDate.getA002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getA002_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getEUHS());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (realEstateDate.getA003_Distance().equals("999") || realEstateDate.getA003_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getA003_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getEULB());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (realEstateDate.getA004_Distance().equals("999") || realEstateDate.getA004_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getA004_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 3) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getLCPS());
                    distance.setText(String.format("%d~%d", hweight.getLCmin(), hweight.getLCmax()));
                    if (realEstateDate.getE002_Distance().equals("999") || realEstateDate.getE002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getE002_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getLCSS());
                    distance.setText(String.format("%d~%d", hweight.getLCmin(), hweight.getLCmax()));
                    if (realEstateDate.getE001_Distance().equals("999") || realEstateDate.getE001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getE001_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 4) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getHTGS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (realEstateDate.getB001_Distance().equals("999") || realEstateDate.getB001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getB001_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getHTAS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (realEstateDate.getAirport_Distance().equals("999") || realEstateDate.getAirport_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getAirport_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getHTIS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (realEstateDate.getB002_Distance().equals("999") || realEstateDate.getB002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getB002_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getHTMS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (realEstateDate.getB003_Distance().equals("999") || realEstateDate.getB003_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(realEstateDate.getB003_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
        } else if (Activity_Analysis.myData != null) {
            if (groupPosition == 0) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getMRTstar());
                    distance.setText(String.format("%d~%d", hweight.getMRTmin(), hweight.getMRTmax()));
                    if (myData.getMRT_StationDistance().equals("999") || myData.getMRT_StationDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getMRT_StationDistance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getTRAstar());
                    distance.setText(String.format("%d~%d", hweight.getTRAmin(), hweight.getTRAmax()));
                    if (myData.getTRA_StationDistance().equals("999") || myData.getTRA_StationDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getTRA_StationDistance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getTHSRstar());
                    distance.setText(String.format("%d~%d", hweight.getTHSRmin(), hweight.getTHSRmax()));
                    if (myData.getTHSR_StationDistance().equals("999") || myData.getTHSR_StationDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getTHSR_StationDistance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getFwaystar());
                    distance.setText(String.format("%d~%d", hweight.getFwaymin(), hweight.getFwaymax()));
                    if (myData.getFway_EntranceDistance().equals("999") || myData.getFway_EntranceDistance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getFway_EntranceDistance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 1) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getLFPS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (myData.getL003_Distance().equals("999") || myData.getL003_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getL003_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getLFHS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (myData.getL004_Distance().equals("999") || myData.getL004_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getL004_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getLFSS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (myData.getL001_Distance().equals("999") || myData.getL001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getL001_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getLFDS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (myData.getL002_Distance().equals("999") || myData.getL002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getL002_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 2) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getEUES());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (myData.getA001_Distance().equals("999") || myData.getA001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getA001_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getEUJS());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (myData.getA002_Distance().equals("999") || myData.getA002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getA002_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getEUHS());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (myData.getA003_Distance().equals("999") || myData.getA003_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getA003_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getEULB());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (myData.getA004_Distance().equals("999") || myData.getA004_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getA004_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 3) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getLCPS());
                    distance.setText(String.format("%d~%d", hweight.getLCmin(), hweight.getLCmax()));
                    if (myData.getE002_Distance().equals("999") || myData.getE002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getE002_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getLCSS());
                    distance.setText(String.format("%d~%d", hweight.getLCmin(), hweight.getLCmax()));
                    if (myData.getE001_Distance().equals("999") || myData.getE001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getE001_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 4) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getHTGS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (myData.getB001_Distance().equals("999") || myData.getB001_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getB001_Distance()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getHTAS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (myData.getAirport_Distance().equals("999") || myData.getAirport_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getAirport_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getHTIS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (myData.getB002_Distance().equals("999") || myData.getB002_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getB002_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getHTMS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (myData.getB003_Distance().equals("999") || myData.getB003_Distance().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(myData.getB003_Distance()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
        } else if (Activity_Analysis.myLoveSample != null) {
            if (groupPosition == 0) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getMRTstar());
                    distance.setText(String.format("%d~%d", hweight.getMRTmin(), hweight.getMRTmax()));
                    if (part2.getMRTD().equals("999") || part2.getMRTD().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getMRTD()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getTRAstar());
                    distance.setText(String.format("%d~%d", hweight.getTRAmin(), hweight.getTRAmax()));
                    if (part2.getTRAD().equals("999") || part2.getTRAD().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getTRAD()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getTHSRstar());
                    distance.setText(String.format("%d~%d", hweight.getTHSRmin(), hweight.getTHSRmax()));
                    if (part2.getTHSRD().equals("999") || part2.getTHSRD().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getTHSRD()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getFwaystar());
                    distance.setText(String.format("%d~%d", hweight.getFwaymin(), hweight.getFwaymax()));
                    if (part2.getFWAYD().equals("999") || part2.getFWAYD().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getFWAYD()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 1) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getLFPS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (part2.getL003D().equals("999") || part2.getL003D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getL003D()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getLFHS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (part2.getL004D().equals("999") || part2.getL004D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getL004D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getLFSS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (part2.getL001D().equals("999") || part2.getL001D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getL001D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getLFDS());
                    distance.setText(String.format("%d~%d", hweight.getLFmin(), hweight.getLFmax()));
                    if (part2.getL002D().equals("999") || part2.getL002D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getL002D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 2) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getEUES());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (part2.getA001D().equals("999") || part2.getA001D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getA001D()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getEUJS());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (part2.getA002D().equals("999") || part2.getA002D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getA002D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getEUHS());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (part2.getA003D().equals("999") || part2.getA003D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getA003D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getEULB());
                    distance.setText(String.format("%d~%d", hweight.getEUmin(), hweight.getEUmax()));
                    if (part2.getA004D().equals("999") || part2.getA004D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getA004D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 3) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getLCPS());
                    distance.setText(String.format("%d~%d", hweight.getLCmin(), hweight.getLCmax()));
                    if (part2.getE002D().equals("999") || part2.getE002D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getE002D()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getLCSS());
                    distance.setText(String.format("%d~%d", hweight.getLCmin(), hweight.getLCmax()));
                    if (part2.getE001D().equals("999") || part2.getE001D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getE001D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
            if (groupPosition == 4) {
                if (childPosition == 0) {
                    bar.setRating(hweight.getHTGS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (part2.getB001D().equals("999") || part2.getB001D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getB001D()) * 1000.0);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 1) {
                    bar.setRating(hweight.getHTAS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (part2.getAportD().equals("999") || part2.getAportD().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getAportD()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 2) {
                    bar.setRating(hweight.getHTIS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (part2.getB002D().equals("999") || part2.getB002D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getB002D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
                if (childPosition == 3) {
                    bar.setRating(hweight.getHTMS());
                    distance.setText(String.format("%d~%d", hweight.getHTmin(), hweight.getHTmax()));
                    if (part2.getB003D().equals("999") || part2.getB003D().equals("")) {
                        realDis.setText("超出2KM");
                    } else {
                        int dis = (int) (Double.parseDouble(part2.getB003D()) * 1000);
                        realDis.setText(String.valueOf(dis));
                    }
                }
            }
        }
        return convertView;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}