package com.example.myapp.ihouse.httpGet;

import android.content.SharedPreferences;

import com.example.myapp.ihouse.computeScore.RealHouse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


/**
 * Created by changyuen on 16/9/7.
 */
public class GetURL {
    InputStream inputStream = null;
    int m_id;
    String json = "";
    String url;
    ArrayList<String> real;
    SharedPreferences member_id;
    private static final String data = "DATA";
    private static final String id = "ID";

    public int getM_id() {
        return m_id;
    }

    public void setM_id(int m_id) {
        this.m_id = m_id;
    }

    public GetURL() {
    }

    public Hweight getHweight(int m_id) { //取得權重
        url = "http://blackwidowfju.azurewebsites.net/api/Values/gethweights?memberid=" + m_id;
        System.out.println(url);
        Hweight hweight = new Hweight();
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            JSONParser parser = new JSONParser();
            Object objects = parser.parse(sbuild.toString());
            JSONObject jb = (JSONObject) objects;
            hweight.setMid(Integer.parseInt(jb.get("mid").toString()));
            hweight.setType(Integer.parseInt(jb.get("type").toString()));
            hweight.setPricemin(Integer.parseInt(jb.get("Pricemin").toString()));
            hweight.setPricemax(Integer.parseInt(jb.get("Pricemax").toString()));
            hweight.setSpacemin(Integer.parseInt(jb.get("Spacemin").toString()));
            hweight.setSpacemax(Integer.parseInt(jb.get("Spacemax").toString()));
            hweight.setYearmin(Integer.parseInt(jb.get("Yearmin").toString()));
            hweight.setYearmax(Integer.parseInt(jb.get("Yearmax").toString()));
            hweight.setRoom(Integer.parseInt(jb.get("Room").toString()));
            hweight.setLRoom(Integer.parseInt(jb.get("LRoom").toString()));
            hweight.setWC(Integer.parseInt(jb.get("WC").toString()));
            hweight.setElevator(Integer.parseInt(jb.get("Elevator").toString()));
            hweight.setCar(Integer.parseInt(jb.get("Car").toString()));
            hweight.setMRTstar(Integer.parseInt(jb.get("MRTstar").toString()));
            hweight.setMRTmin(Integer.parseInt(jb.get("MRTmin").toString()));
            hweight.setMRTmax(Integer.parseInt(jb.get("MRTmax").toString()));
            hweight.setTRAstar(Integer.parseInt(jb.get("TRAstar").toString()));
            hweight.setTRAmin(Integer.parseInt(jb.get("TRAmin").toString()));
            hweight.setTRAmax(Integer.parseInt(jb.get("TRAmax").toString()));
            hweight.setTHSRstar(Integer.parseInt(jb.get("THSRstar").toString()));
            hweight.setTHSRmin(Integer.parseInt(jb.get("THSRmin").toString()));
            hweight.setTHSRmax(Integer.parseInt(jb.get("THSRmax").toString()));
            hweight.setFwaystar(Integer.parseInt(jb.get("Fwaystar").toString()));
            hweight.setFwaymin(Integer.parseInt(jb.get("Fwaymin").toString()));
            hweight.setFwaymax(Integer.parseInt(jb.get("Fwaymax").toString()));
            hweight.setLFmin(Integer.parseInt(jb.get("LFmin").toString()));
            hweight.setLFmax(Integer.parseInt(jb.get("LFmax").toString()));
            hweight.setLFPS(Integer.parseInt(jb.get("LFPS").toString()));
            hweight.setLFHS(Integer.parseInt(jb.get("LFHS").toString()));
            hweight.setLFSS(Integer.parseInt(jb.get("LFSS").toString()));
            hweight.setLFDS(Integer.parseInt(jb.get("LFDS").toString()));
            hweight.setEUmin(Integer.parseInt(jb.get("EUmin").toString()));
            hweight.setEUmax(Integer.parseInt(jb.get("EUmax").toString()));
            hweight.setEUES(Integer.parseInt(jb.get("EUES").toString()));
            hweight.setEUJS(Integer.parseInt(jb.get("EUJS").toString()));
            hweight.setEUHS(Integer.parseInt(jb.get("EUHS").toString()));
            hweight.setEULB(Integer.parseInt(jb.get("EULB").toString()));
            hweight.setLCmin(Integer.parseInt(jb.get("LCmin").toString()));
            hweight.setLCmax(Integer.parseInt(jb.get("LCmax").toString()));
            hweight.setLCPS(Integer.parseInt(jb.get("LCPS").toString()));
            hweight.setLCSS(Integer.parseInt(jb.get("LCSS").toString()));
            hweight.setHTmin(Integer.parseInt(jb.get("HTmin").toString()));
            hweight.setHTmax(Integer.parseInt(jb.get("HTmax").toString()));
            hweight.setHTAS(Integer.parseInt(jb.get("HTAS").toString()));
            hweight.setHTGS(Integer.parseInt(jb.get("HTGS").toString()));
            hweight.setHTIS(Integer.parseInt(jb.get("HTIS").toString()));
            hweight.setHTMS(Integer.parseInt(jb.get("HTMS").toString()));
            hweight.setSection(jb.get("Section").toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hweight;
    }


    public int Login(String email, String token, int type) { //使用者登入、加入、驗證
        url = "http://blackwidowfju.azurewebsites.net/api/Values/Login?email=" + email + "&token=" + token + "&type=" + type;
        System.out.println(url);
        int m_id = -1;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            m_id = Integer.parseInt(sbuild.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return m_id;
    }

    public int checkMyhouse(int m_id) { //檢查權重
        int isExist = -1;
        url = "http://blackwidowfju.azurewebsites.net/api/Values/CheckHweights?membersid=" + m_id;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            isExist = Integer.parseInt(sbuild.toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isExist;
    }

    public SaleHouse sale_detail(int sid) {
        url = "http://blackwidowfju.azurewebsites.net/api/Values/GetDetailedSellHouse?shdid=" + sid;
        System.out.println(url);
        SaleHouse data = new SaleHouse();
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            inputStream.close();
            json = sbuild.toString();
            System.out.println(sbuild);
            JSONParser parser = new JSONParser();
            Object objects = parser.parse(json);
            JSONObject jb = (JSONObject) objects;

            data.setSellHouse_Address(jb.get("SellHouse_Address").toString());
            data.setSellHouse_Price(jb.get("SellHouse_Price").toString());
            data.setSellHouse_Space(jb.get("SellHouse_Space").toString());
            data.setSellHouse_Year(jb.get("SellHouse_Year").toString());
            data.setSellHouse_Elevator(jb.get("SellHouse_Elevator").toString());
            data.setSellHouse_Car(jb.get("SellHouse_Car").toString());
            data.setSellHouse_Room(jb.get("SellHouse_Room").toString());
            data.setSellHouse_LRoom(jb.get("SellHouse_LRoom").toString());
            data.setSellHouse_WC(jb.get("SellHouse_WC").toString());
            data.setSellHouse_URL(jb.get("SellHouse_URL").toString());
            data.setSellHouse_Type(jb.get("SellHouse_Type").toString());
            JSONArray array = new JSONArray(jb.get("ls").toString());
            ArrayList<SaleHouseLs> ls = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                SaleHouseLs p = new SaleHouseLs();
                p.setP_Name(array.getJSONObject(i).get("P_Name").toString());
                p.setP_Type(array.getJSONObject(i).get("P_Type").toString());
                ls.add(p);
            }
            data.setLs(ls);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public RealEstateDate real_detail(int rid, int m_id) { //實價登錄詳細
        url = "http://blackwidowfju.azurewebsites.net/api/Values?rid=" + rid + "&mem=" + m_id;
        System.out.println(url);
        RealEstateDate data = new RealEstateDate();
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            inputStream.close();
            json = sbuild.toString();
            System.out.println(sbuild);
            JSONParser parser = new JSONParser();
            Object objects = parser.parse(json);
            JSONObject jb = (JSONObject) objects;
            data.setAddLoveCheck(jb.get("AddLoveCheck").toString());
            data.setRealAddr(jb.get("RealAddr").toString());
            data.setHouseArea(jb.get("HouseArea").toString());
            data.setParkingArea(jb.get("ParkingArea").toString());
            data.setMl_HouseScore(jb.get("ml_HouseScore").toString());
            data.setMl_MRTScore(jb.get("ml_MRTScore").toString());
            data.setMl_TRAScore(jb.get("ml_TRAScore").toString());
            data.setMl_THSRScore(jb.get("ml_THSRScore").toString());
            data.setMl_FWAYScore(jb.get("ml_FWAYScore").toString());
            data.setMl_LifeScore(jb.get("ml_LifeScore").toString());
            data.setMl_PostScore(jb.get("ml_PostScore").toString());
            data.setMl_HosScore(jb.get("ml_HosScore").toString());
            data.setMl_MarketScore(jb.get("ml_MarketScore").toString());
            data.setMl_DepartScore(jb.get("ml_DepartScore").toString());
            data.setMl_EduScore(jb.get("ml_EduScore").toString());
            data.setMl_ELEScore(jb.get("ml_ELEScore").toString());
            data.setMl_MIDScore(jb.get("ml_MIDScore").toString());
            data.setMl_HighScore(jb.get("ml_HighScore").toString());
            data.setMl_LibScore(jb.get("ml_LibScore").toString());
            data.setMl_LeisureScore(jb.get("ml_LeisureScore").toString());
            data.setMl_SportScore(jb.get("ml_SportScore").toString());
            data.setMl_ParkScore(jb.get("ml_ParkScore").toString());
            data.setMl_AirScore(jb.get("ml_AirScore").toString());
            data.setMl_FireScore(jb.get("ml_FireScore").toString());
            data.setMl_DieScore(jb.get("ml_DieScore").toString());
            data.setMl_GasScore(jb.get("ml_GasScore").toString());
            data.setMl_DisasterScore(jb.get("ml_DisasterScore").toString());
            data.setLon(jb.get("Lon").toString());
            data.setLat(jb.get("Lat").toString());
            data.setMl_Score(jb.get("ml_Score").toString());
            data.setHouseType(jb.get("HouseType").toString());
            data.setPrice(jb.get("Price").toString());
            data.setLandArea(jb.get("LandArea").toString());
            data.setHouseDate(jb.get("HouseDate").toString());
            data.setHouseRoom_1(jb.get("HouseRoom_1").toString());
            data.setHouseRoom_2(jb.get("HouseRoom_2").toString());
            data.setHouseRoom_3(jb.get("HouseRoom_3").toString());
            data.setMRT_ID(jb.get("MRT_ID").toString());
            data.setMRT_Orders(jb.get("MRT_Orders").toString());
            data.setMRT_StationDistance(jb.get("MRT_StationDistance").toString());
            data.setFway_ID(jb.get("Fway_ID").toString());
            data.setFway_Orders(jb.get("Fway_Orders").toString());
            data.setFway_EntranceDistance(jb.get("Fway_EntranceDistance").toString());
            data.setTRA_ID(jb.get("TRA_ID").toString());
            data.setTRA_Orders(jb.get("TRA_Orders").toString());
            data.setTRA_StationDistance(jb.get("TRA_StationDistance").toString());
            data.setTHSR_ID(jb.get("THSR_ID").toString());
            data.setTHSR_Orders(jb.get("THSR_Orders").toString());
            data.setTHSR_StationDistance(jb.get("THSR_StationDistance").toString());
            data.setA001_ID(jb.get("A001_ID").toString());
            data.setA001_Distance(jb.get("A001_Distance").toString());
            data.setA002_ID(jb.get("A002_ID").toString());
            data.setA002_Distance(jb.get("A002_Distance").toString());
            data.setA003_ID(jb.get("A003_ID").toString());
            data.setA003_Distance(jb.get("A003_Distance").toString());
            data.setA004_ID(jb.get("A004_ID").toString());
            data.setA004_Distance(jb.get("A004_Distance").toString());
            data.setL001_ID(jb.get("L001_ID").toString());
            data.setL001_Distance(jb.get("L001_Distance").toString());
            data.setL002_ID(jb.get("L002_ID").toString());
            data.setL002_Distance(jb.get("L002_Distance").toString());
            data.setL003_ID(jb.get("L003_ID").toString());
            data.setL003_Distance(jb.get("L003_Distance").toString());
            data.setL004_ID(jb.get("L004_ID").toString());
            data.setL004_Distance(jb.get("L004_Distance").toString());
            data.setE001_ID(jb.get("E001_ID").toString());
            data.setE001_Distance(jb.get("E001_Distance").toString());
            data.setE002_ID(jb.get("E002_ID").toString());
            data.setE002_Distance(jb.get("E002_Distance").toString());
            data.setB001_ID(jb.get("B001_ID").toString());
            data.setB001_Distance(jb.get("B001_Distance").toString());
            data.setB002_ID(jb.get("B002_ID").toString());
            data.setB002_Distance(jb.get("B002_Distance").toString());
            data.setB003_ID(jb.get("B003_ID").toString());
            data.setB003_Distance(jb.get("B003_Distance").toString());
            data.setAirport_ID(jb.get("Airport_ID").toString());
            data.setAirport_Distance(jb.get("Airport_Distance").toString());
            data.setUniv_ID(jb.get("Univ_ID").toString());
            data.setUniv_Distance(jb.get("Univ_Distance").toString());
            data.setParkR_ID(jb.get("ParkR_ID").toString());
            data.setParkR_Distance(jb.get("ParkR_Distance").toString());
            data.setFlood_ID(jb.get("Flood_ID").toString());
            data.setSoilLiq_ID(jb.get("SoilLiq_ID").toString());
            data.setFault_ID(jb.get("Fault_ID").toString());
            data.setID(jb.get("ID").toString());
            JSONArray array = new JSONArray(jb.get("ls").toString());
            ArrayList<RealEstateDateLs> ls = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                RealEstateDateLs p = new RealEstateDateLs();
                p.setP_Name(array.getJSONObject(i).get("P_Name").toString());
                p.setP_Type(array.getJSONObject(i).get("P_Type").toString());
                ls.add(p);
            }
            data.setLs(ls);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public void putHweight(Hweight myhouse) {  //新增權重
        try {
            String pre = "http://blackwidowfju.azurewebsites.net/api/Values/NewHWeights?nlist=%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s";
            url = String.format(pre, myhouse.getMid(), myhouse.getType(), myhouse.getPricemin(), myhouse.getPricemax(),
                    myhouse.getSpacemin(), myhouse.getSpacemax(), myhouse.getYearmin(), myhouse.getYearmax(), myhouse.getRoom(), myhouse.getLRoom(),
                    myhouse.getWC(), myhouse.getElevator(), myhouse.getCar(), myhouse.getMRTstar(), myhouse.getMRTmin(), myhouse.getMRTmax(),
                    myhouse.getTRAstar(), myhouse.getTRAmin(), myhouse.getTRAmax(), myhouse.getTHSRstar(), myhouse.getTHSRmin(), myhouse.getTHSRmax(),
                    myhouse.getFwaystar(), myhouse.getFwaymin(), myhouse.getFwaymax(), myhouse.getLFmin(), myhouse.getLFmax(), myhouse.getLFPS(),
                    myhouse.getLFHS(), myhouse.getLFSS(), myhouse.getLFDS(), myhouse.getEUmin(), myhouse.getEUmax(), myhouse.getEUES(), myhouse.getEUJS(),
                    myhouse.getEUHS(), myhouse.getEULB(), myhouse.getLCmin(), myhouse.getLCmax(), myhouse.getLCPS(), myhouse.getLCSS(),
                    myhouse.getHTmin(), myhouse.getHTmax(), myhouse.getHTAS(), myhouse.getHTGS(), myhouse.getHTIS(), myhouse.getHTMS(), myhouse.getSection());
            System.out.println(url);
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public ArrayList<MyLoveSample> getMylovelist(int m_id, String order) { //取得我的最愛列表
        url = "http://blackwidowfju.azurewebsites.net/api/Values/GainLove?mid=" + m_id + "&ordertype=" + order;
        ArrayList<MyLoveSample> myLoveArrayList = new ArrayList<>();
        MyLoveSample myLoveSample;
        System.out.println(url);
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            inputStream.close();
            JSONArray array = new JSONArray(sbuild.toString());
            for (int i = 0; i < array.length(); i++) {
                org.json.JSONObject object = array.getJSONObject(i);
                myLoveSample = new MyLoveSample();
                myLoveSample.setMl_id(object.getString("ml_id"));
                myLoveSample.setMid(object.getString("mid"));
                myLoveSample.setMl_rid(object.getString("ml_rid"));
                myLoveSample.setMl_address(object.getString("ml_address"));
                myLoveSample.setMl_time(object.getString("ml_time"));
                myLoveSample.setMl_type(object.getString("ml_type"));
                myLoveSample.setMl_Score(object.getString("ml_Score"));
                myLoveSample.setMl_type(object.getString("ml_type"));
                myLoveSample.setMl_lat(object.getString("ml_lat"));
                myLoveSample.setMl_lon(object.getString("ml_lon"));
                myLoveSample.setMl_HouseScore(object.getString("ml_HouseScore"));
                myLoveSample.setMl_MRTScore(object.getString("ml_MRTScore"));
                myLoveSample.setMl_TRAScore(object.getString("ml_TRAScore"));
                myLoveSample.setMl_THSRScore(object.getString("ml_THSRScore"));
                myLoveSample.setMl_FWAYScore(object.getString("ml_FWAYScore"));
                myLoveSample.setMl_LifeScore(object.getString("ml_LifeScore"));
                myLoveSample.setMl_PostScore(object.getString("ml_PostScore"));
                myLoveSample.setMl_HosScore(object.getString("ml_HosScore"));
                myLoveSample.setMl_MarketScore(object.getString("ml_MarketScore"));
                myLoveSample.setMl_DepartScore(object.getString("ml_DepartScore"));
                myLoveSample.setMl_EduScore(object.getString("ml_EduScore"));
                myLoveSample.setMl_ELEScore(object.getString("ml_ELEScore"));
                myLoveSample.setMl_MIDScore(object.getString("ml_MIDScore"));
                myLoveSample.setMl_HighScore(object.getString("ml_HighScore"));
                myLoveSample.setMl_LibScore(object.getString("ml_LibScore"));
                myLoveSample.setMl_LeisureScore(object.getString("ml_LeisureScore"));
                myLoveSample.setMl_SportScore(object.getString("ml_SportScore"));
                myLoveSample.setMl_ParkScore(object.getString("ml_ParkScore"));
                myLoveSample.setMl_AirScore(object.getString("ml_AirScore"));
                myLoveSample.setMl_FireScore(object.getString("ml_FireScore"));
                myLoveSample.setMl_DieScore(object.getString("ml_DieScore"));
                myLoveSample.setMl_GasScore(object.getString("ml_GasScore"));
                myLoveSample.setMl_DisasterScore(object.getString("ml_DisasterScore"));
                myLoveArrayList.add(myLoveSample);
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return myLoveArrayList;
    }

    public MyData getScoreAndData(String type, String address, int m_id, RealHouse realHouse) {  //取得分數與查詢資料
        url = "http://blackwidowfju.azurewebsites.net/api/Values?type=" + type + "&address=" + address + "&memb=" + m_id + "&hs=" + realHouse.getType() + "," + realHouse.getPrice() + "," + realHouse.getSpace() + "," + realHouse.getYears() + "," + realHouse.getRoom() + "," + realHouse.getL_room() + "," + realHouse.getBath() + "," + realHouse.getCar();
        System.out.println(url);
        MyData myData = new MyData();
        real = new ArrayList<>();
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            inputStream.close();
            json = sbuild.toString();
            System.out.println(sbuild);
            JSONParser parser = new JSONParser();
            Object objects = parser.parse(json);
            JSONObject jb = (JSONObject) objects;
            myData.setRealAddr(jb.get("RealAddr").toString());
            myData.setAddLoveCheck(jb.get("AddLoveCheck").toString());
            myData.setMl_HouseScore(jb.get("ml_HouseScore").toString());
            myData.setMl_MRTScore(jb.get("ml_MRTScore").toString());
            myData.setMl_TRAScore(jb.get("ml_TRAScore").toString());
            myData.setMl_THSRScore(jb.get("ml_THSRScore").toString());
            myData.setMl_FWAYScore(jb.get("ml_FWAYScore").toString());
            myData.setMl_LifeScore(jb.get("ml_LifeScore").toString());
            myData.setMl_PostScore(jb.get("ml_PostScore").toString());
            myData.setMl_HosScore(jb.get("ml_HosScore").toString());
            myData.setMl_MarketScore(jb.get("ml_MarketScore").toString());
            myData.setMl_DepartScore(jb.get("ml_DepartScore").toString());
            myData.setMl_EduScore(jb.get("ml_EduScore").toString());
            myData.setMl_ELEScore(jb.get("ml_ELEScore").toString());
            myData.setMl_MIDScore(jb.get("ml_MIDScore").toString());
            myData.setMl_HighScore(jb.get("ml_HighScore").toString());
            myData.setMl_LibScore(jb.get("ml_LibScore").toString());
            myData.setMl_LeisureScore(jb.get("ml_LeisureScore").toString());
            myData.setMl_SportScore(jb.get("ml_SportScore").toString());
            myData.setMl_ParkScore(jb.get("ml_ParkScore").toString());
            myData.setMl_AirScore(jb.get("ml_AirScore").toString());
            myData.setMl_FireScore(jb.get("ml_FireScore").toString());
            myData.setMl_DieScore(jb.get("ml_DieScore").toString());
            myData.setMl_GasScore(jb.get("ml_GasScore").toString());
            myData.setMl_DisasterScore(jb.get("ml_DisasterScore").toString());
            myData.setLon(jb.get("Lon").toString());
            myData.setLat(jb.get("Lat").toString());
            myData.setMl_Score(jb.get("ml_Score").toString());
            myData.setHouseType(jb.get("HouseType").toString());
            myData.setPrice(jb.get("Price").toString());
            myData.setHouseArea(jb.get("HouseArea"));
            //myData.setLandArea(jb.get("LandArea").toString());
            myData.setHouseDate(jb.get("HouseDate").toString());
            myData.setHouseRoom_1(jb.get("HouseRoom_1").toString());
            myData.setHouseRoom_2(jb.get("HouseRoom_2").toString());
            myData.setHouseRoom_3(jb.get("HouseRoom_3").toString());
            myData.setMRT_ID(jb.get("MRT_ID").toString());
            myData.setMRT_Orders(jb.get("MRT_Orders").toString());
            myData.setMRT_StationDistance(jb.get("MRT_StationDistance").toString());
            myData.setFway_ID(jb.get("Fway_ID").toString());
            myData.setFway_Orders(jb.get("Fway_Orders").toString());
            myData.setFway_EntranceDistance(jb.get("Fway_EntranceDistance").toString());
            myData.setTRA_ID(jb.get("TRA_ID").toString());
            myData.setTRA_Orders(jb.get("TRA_Orders").toString());
            myData.setTRA_StationDistance(jb.get("TRA_StationDistance").toString());
            myData.setTHSR_ID(jb.get("THSR_ID").toString());
            myData.setTHSR_Orders(jb.get("THSR_Orders").toString());
            myData.setTHSR_StationDistance(jb.get("THSR_StationDistance").toString());
            myData.setA001_ID(jb.get("A001_ID").toString());
            myData.setA001_Distance(jb.get("A001_Distance").toString());
            myData.setA002_ID(jb.get("A002_ID").toString());
            myData.setA002_Distance(jb.get("A002_Distance").toString());
            myData.setA003_ID(jb.get("A003_ID").toString());
            myData.setA003_Distance(jb.get("A003_Distance").toString());
            myData.setA004_ID(jb.get("A004_ID").toString());
            myData.setA004_Distance(jb.get("A004_Distance").toString());
            myData.setL001_ID(jb.get("L001_ID").toString());
            myData.setL001_Distance(jb.get("L001_Distance").toString());
            myData.setL002_ID(jb.get("L002_ID").toString());
            myData.setL002_Distance(jb.get("L002_Distance").toString());
            myData.setL003_ID(jb.get("L003_ID").toString());
            myData.setL003_Distance(jb.get("L003_Distance").toString());
            myData.setL004_ID(jb.get("L004_ID").toString());
            myData.setL004_Distance(jb.get("L004_Distance").toString());
            myData.setE001_ID(jb.get("E001_ID").toString());
            myData.setE001_Distance(jb.get("E001_Distance").toString());
            myData.setE002_ID(jb.get("E002_ID").toString());
            myData.setE002_Distance(jb.get("E002_Distance").toString());
            myData.setB001_ID(jb.get("B001_ID").toString());
            myData.setB001_Distance(jb.get("B001_Distance").toString());
            myData.setB002_ID(jb.get("B002_ID").toString());
            myData.setB002_Distance(jb.get("B002_Distance").toString());
            myData.setB003_ID(jb.get("B003_ID").toString());
            myData.setB003_Distance(jb.get("B003_Distance").toString());
            myData.setAirport_ID(jb.get("Airport_ID").toString());
            myData.setAirport_Distance(jb.get("Airport_Distance").toString());
            myData.setUniv_ID(jb.get("Univ_ID").toString());
            myData.setUniv_Distance(jb.get("Univ_Distance").toString());
            myData.setParkR_ID(jb.get("ParkR_ID").toString());
            myData.setParkR_Distance(jb.get("ParkR_Distance").toString());
            myData.setFlood_ID(jb.get("Flood_ID").toString());
            myData.setSoilLiq_ID(jb.get("SoilLiq_ID").toString());
            myData.setFault_ID(jb.get("Fault_ID").toString());


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return myData;
    }

    public void addMyLoveReal(int m_id, MyData myData, int elev, int car) { //加入我的最愛自訂房屋
        //String pre = "http://blackwidowfju.azurewebsites.net/api/Values/AddLove?love=" + m_id + "," + myData.getRealAddr() + ",1,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,99999999,%f,%f,%d&rh=%d,%f,%f,%d,%d,%d,%d,%d,%d,%d,%d,%f,%d,%d,%f,%d,%d,%f,%d,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%d,%d";
        String pre = "http://blackwidowfju.azurewebsites.net/api/Values/AddLove?love=" + m_id + "," + myData.getRealAddr() + ",1,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,-999999,%s,%s,%s&rh=%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s";
        url = String.format(pre, myData.getMl_HouseScore(), myData.getMl_MRTScore(), myData.getMl_TRAScore(), myData.getMl_THSRScore(), myData.getMl_FWAYScore(),
                myData.getMl_LifeScore(), myData.getMl_PostScore(), myData.getMl_HosScore(), myData.getMl_MarketScore(), myData.getMl_DepartScore(),
                myData.getMl_EduScore(), myData.getMl_ELEScore(), myData.getMl_MIDScore(), myData.getMl_HighScore(), myData.getMl_LibScore(),
                myData.getMl_LeisureScore(), myData.getMl_SportScore(), myData.getMl_ParkScore(),
                myData.getMl_AirScore(), myData.getMl_FireScore(), myData.getMl_DieScore(), myData.getMl_GasScore(), myData.getMl_DisasterScore(),
                myData.getLon(), myData.getLat(), myData.getMl_Score(),
                myData.getHouseType(), myData.getPrice(), myData.getHouseArea(), myData.getHouseDate(), myData.getHouseRoom_1(), myData.getHouseRoom_2(), myData.getHouseRoom_3(), elev, car, myData.getMRT_ID(), myData.getMRT_Orders(), myData.getMRT_StationDistance(), myData.getFway_ID(), myData.getFway_Orders(), myData.getFway_EntranceDistance(), myData.getTRA_ID(), myData.getTRA_Orders(), myData.getTRA_StationDistance(), myData.getTHSR_ID(), myData.getTHSR_Orders(), myData.getTHSR_StationDistance(), myData.getL001_ID(), myData.getL001_Distance(), myData.getL002_ID(), myData.getL002_Distance(), myData.getL003_ID(), myData.getL003_Distance(), myData.getL004_ID(), myData.getL004_Distance(),
                myData.getA001_ID(), myData.getA001_Distance(), myData.getA002_ID(), myData.getA002_Distance(), myData.getA003_ID(), myData.getA003_Distance(), myData.getA004_ID(), myData.getA004_Distance(), myData.getE001_ID(), myData.getE001_Distance(), myData.getE002_ID(), myData.getE002_Distance(), myData.getB001_ID(), myData.getB001_Distance(), myData.getB002_ID(), myData.getB002_Distance(), myData.getB003_ID(), myData.getB003_Distance(),
                myData.getAirport_ID(), myData.getAirport_Distance(), myData.getUniv_ID(), myData.getUniv_Distance(), myData.getParkR_ID(), myData.getParkR_Distance(), myData.getFlood_ID(), myData.getSoilLiq_ID(), myData.getFault_ID());
        System.out.println(url);
        System.out.println(myData.getMl_Score());
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public void addMyLoveEstate(int m_id, RealEstateDate myData) { //加入我的最愛實價登錄
        //String pre = "http://blackwidowfju.azurewebsites.net/api/Values/AddLove?love=" + m_id + "," + myData.getRealAddr() + ",1,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,99999999,%f,%f,%d&rh=%d,%f,%f,%d,%d,%d,%d,%d,%d,%d,%d,%f,%d,%d,%f,%d,%d,%f,%d,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%f,%d,%d,%d";
        String pre = "http://blackwidowfju.azurewebsites.net/api/Values/AddLove?love=" + m_id + "," + myData.getRealAddr() + ",0,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s&rh=-1";
        url = String.format(pre, myData.getMl_HouseScore(), myData.getMl_MRTScore(), myData.getMl_TRAScore(), myData.getMl_THSRScore(), myData.getMl_FWAYScore(),
                myData.getMl_LifeScore(), myData.getMl_PostScore(), myData.getMl_HosScore(), myData.getMl_MarketScore(), myData.getMl_DepartScore(),
                myData.getMl_EduScore(), myData.getMl_ELEScore(), myData.getMl_MIDScore(), myData.getMl_HighScore(), myData.getMl_LibScore(),
                myData.getMl_LeisureScore(), myData.getMl_SportScore(), myData.getMl_ParkScore(),
                myData.getMl_AirScore(), myData.getMl_FireScore(), myData.getMl_DieScore(), myData.getMl_GasScore(), myData.getMl_DisasterScore(), myData.getID(),
                myData.getLon(), myData.getLat(), myData.getMl_Score());
        System.out.println(url);
        System.out.println(myData.getMl_Score());
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public void DeleteMyLove(int myloveid, int realid, int type) {
        url = "http://blackwidowfju.azurewebsites.net/api/Values/DeleteMyLove?mylovelist=" + myloveid + "&realidlist=" + realid + "&typelist=" + type;
        System.out.println(url);
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public Part2 getRealLove(String realid, String thetype) {
        Part2 part2 = new Part2();
        url = "http://blackwidowfju.azurewebsites.net/api/Values/getRealLove?realid=" + realid + "&thetype=" + thetype;
        System.out.println(url);
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet(url);
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sbuild = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuild.append(line);
            }
            inputStream.close();
            json = sbuild.toString();
            System.out.println(json);
            JSONParser parser = new JSONParser();
            Object objects = parser.parse(json);
            JSONObject jb = (JSONObject) objects;
            part2.setFaultID(jb.get("FaultID").toString());
            part2.setFloodID(jb.get("FloodID").toString());
            part2.setSoilLiqID(jb.get("SoilLiqID").toString());
            part2.setType(jb.get("Type").toString());
            part2.setSpace(jb.get("Space").toString());
            part2.setPrice(jb.get("Price").toString());
            part2.setRoom(jb.get("Room").toString());
            part2.setLRoom(jb.get("LRoom").toString());
            part2.setWC(jb.get("WC").toString());
            part2.setYear(jb.get("Year").toString());
            part2.setCar(jb.get("Car").toString());
            part2.setElevator(jb.get("Elevator").toString());
            part2.setMRTD(jb.get("MRTD").toString());
            part2.setTRAD(jb.get("TRAD").toString());
            part2.setTHSRD(jb.get("THSRD").toString());
            part2.setFWAYD(jb.get("FWAYD").toString());
            part2.setL003D(jb.get("L003D").toString());
            part2.setL004D(jb.get("L004D").toString());
            part2.setL001D(jb.get("L001D").toString());
            part2.setL002D(jb.get("L002D").toString());
            part2.setA001D(jb.get("A001D").toString());
            part2.setA002D(jb.get("A002D").toString());
            part2.setA003D(jb.get("A003D").toString());
            part2.setA004D(jb.get("A004D").toString());
            part2.setE001D(jb.get("E001D").toString());
            part2.setE002D(jb.get("E002D").toString());
            part2.setB001D(jb.get("B001D").toString());
            part2.setAportD(jb.get("AportD").toString());
            part2.setB002D(jb.get("B002D").toString());
            part2.setB003D(jb.get("B003D").toString());

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return part2;
    }


}
