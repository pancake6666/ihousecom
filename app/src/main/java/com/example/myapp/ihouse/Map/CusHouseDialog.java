package com.example.myapp.ihouse.map;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.analysis.Activity_Analysis;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.MyData;


public class CusHouseDialog extends DialogFragment {
    public int price;
    public Double lng;
    public Double lat;
    public String address;
    public Double space;
    public boolean check = false;
    int price_Input;
    String address_Input;
    double space_Input;
    ArrayAdapter<String> typeList;
    ArrayAdapter<Integer> roomlist;
    ArrayAdapter<Integer> lroomlist;
    ArrayAdapter<Integer> bathlist;
    public static RealHouse realHouse;
    public static String analysistype;
    public static MyData myData;
    public static GetURL httpGet;
    private Hweight hweight;
    int m_id;
    ProgressDialog progressDialog;
    Intent intent;
    public CustomMapActivity activity;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        intent = new Intent(getActivity(), Activity_Analysis.class);
        m_id = preferences.getInt("ID", 0);
        final View v = inflater.inflate(R.layout.cushousedialog, null);
        final EditText addressE = (EditText) v.findViewById(R.id.addressinput);
        final EditText priceE = (EditText) v.findViewById(R.id.priceinput);
        final EditText spaceE = (EditText) v.findViewById(R.id.spaceinput);
        final EditText yearsE = (EditText) v.findViewById(R.id.yearsinput);
        final Spinner typeSpinner = (Spinner) v.findViewById(R.id.typechoise);
        realHouse = new RealHouse();
        String[] type = {"公寓", "住宅大樓", "套房", "透天厝", "華廈"};
        realHouse.setType(1);
        realHouse.setElec(0);
        typeList = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, type);
        typeSpinner.setAdapter(typeList);
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        realHouse.setType(1);
                        realHouse.setElec(0);
                        break;
                    case 1:
                        realHouse.setType(2);
                        realHouse.setElec(1);
                        break;
                    case 2:
                        realHouse.setType(4);
                        realHouse.setElec(0);
                        break;
                    case 3:
                        realHouse.setType(5);
                        realHouse.setElec(0);
                        break;
                    case 4:
                        realHouse.setType(6);
                        realHouse.setElec(1);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final Spinner room = (Spinner) v.findViewById(R.id.roomcount);
        Integer[] roomcount = {0, 1, 2, 3, 4, 5, 6};
        roomlist = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, roomcount);
        realHouse.setRoom(0);
        room.setAdapter(roomlist);
        room.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                realHouse.setRoom(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final Spinner lroom = (Spinner) v.findViewById(R.id.livecount);
        Integer[] lroomcount = {0, 1, 2, 3};
        lroomlist = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, lroomcount);
        realHouse.setL_room(0);
        lroom.setAdapter(lroomlist);
        lroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                realHouse.setL_room(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final Spinner bath = (Spinner) v.findViewById(R.id.bathcount);
        Integer[] bathcount = {0, 1, 2, 3, 4};
        bathlist = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, bathcount);
        realHouse.setBath(0);
        bath.setAdapter(bathlist);
        bath.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                realHouse.setBath(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final RadioGroup carGroup = (RadioGroup) v.findViewById(R.id.cargroup);
        carGroup.check(R.id.nocar);
        realHouse.setCar(0);
        carGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.havecar:
                        realHouse.setCar(1);
                        break;
                    case R.id.nocar:
                        realHouse.setCar(0);
                        break;
                }
            }
        });
        final RadioGroup analysis = (RadioGroup) v.findViewById(R.id.analysistype);
        analysis.check(R.id.fast);
        analysistype = "a";
        analysis.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.fast:
                        analysistype = "a";
                        break;
                    case R.id.full:
                        analysistype = "f";
                        break;

                }
            }
        });

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("房屋資訊輸入")
                .setPositiveButton("送出並分析", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!addressE.getText().toString().isEmpty() && !priceE.getText().toString().isEmpty() && !spaceE.getText().toString().isEmpty()) {
                            realHouse.setAddress(addressE.getText().toString());
                            realHouse.setPrice(Double.parseDouble(priceE.getText().toString()) * 10000);
                            realHouse.setSpace(Double.parseDouble(spaceE.getText().toString()));
                            realHouse.setYears(Integer.parseInt(yearsE.getText().toString()));
                            LoadAndCompute loadAndCompute = new LoadAndCompute(getActivity());
                            loadAndCompute.execute(m_id);
                        }
                    }
                })
                .setNegativeButton("關閉", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dismiss();
                    }
                })
                .create();
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setSpace(Double space) {
        this.space = space;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }


    class LoadAndCompute extends AsyncTask<Integer, Integer, Boolean> {

        Context context;

        private LoadAndCompute(Context context) {
            this.context = context.getApplicationContext();
        }

        @Override
        protected Boolean doInBackground(Integer... integers) {
            httpGet = new GetURL();
            myData = httpGet.getScoreAndData(analysistype, realHouse.getAddress(), integers[0], realHouse);
            hweight = httpGet.getHweight(integers[0]);
            System.out.println(myData.getRealAddr());
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "");
        }

        @Override
        protected void onPostExecute(Boolean finish) {
            super.onPreExecute();
            progressDialog.dismiss();
            Bundle myDATA = new Bundle();
            myDATA.putSerializable("fromCus", myData);
            myDATA.putSerializable("fromCusReal", realHouse);
            myDATA.putSerializable("hweight", hweight);
            intent.putExtras(myDATA);
            context.startActivity(new Intent(context, Activity_Analysis.class).putExtras(myDATA).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
