package com.example.myapp.ihouse.register;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.NumberPicker;
import android.widget.RadioGroup;

import com.example.myapp.ihouse.R;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

/**
 * Created by changyuen on 2016/8/10.
 */
public class HouseFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private MaterialBetterSpinner area_spinner;
    private NumberPicker np;
    private NumberPicker np2;
    private NumberPicker np3;
    private NumberPicker np4;
    private NumberPicker np5;
    private NumberPicker np6;
    private NumberPicker np7;
    private NumberPicker np8;
    private NumberPicker np9;
    private CheckBox ch12;
    private CheckBox ch34;
    private CheckBox ch56;
    private CheckBox ch789;
    private RadioGroup radioGroup;
    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private StepHousePage mPage;
    protected PageFragmentCallbacks mCallbacks;
    private String mKey;


    public static HouseFragment newInstance(String pageNo) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, pageNo);
        HouseFragment fragment = new HouseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_PAGE);
        mPage = (StepHousePage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register, container, false);

        area_spinner = (MaterialBetterSpinner) view.findViewById(R.id.area_spinner);
        final String[] areaList = {"不限", "中正區", "大同區", "中山區", "松山區", "大安區", "萬華區", "信義區", "士林區", "北投區", "內湖區", "南港區", "文山區"};
        final ArrayAdapter<String> areaAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, areaList);

        area_spinner.setAdapter(areaAdapter);
        area_spinner.setText(mPage.getData().getString(StepHousePage.AREA_DATA_KEY));
        area_spinner.setHintTextColor(Color.BLACK);
        area_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (area_spinner.getText().toString().equals("不限")) {
                    mPage.getData().putString(StepHousePage.AREA_DATA_KEY, "不限");

                } else {
                    mPage.getData().putString(StepHousePage.AREA_DATA_KEY, area_spinner.getText().toString());
                }
            }
        });

        radioGroup = (RadioGroup) view.findViewById(R.id.rgroup1);
        mPage.getData().putInt(StepHousePage.TYPE_DATA_KEY, mPage.getData().getInt(StepHousePage.TYPE_DATA_KEY));
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.apartment:
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_KEY, 1);
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.building:
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_KEY, 2);
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.villa:
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_KEY, 4);
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.sky:
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_KEY, 5);
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.Landmark:
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_KEY, 6);
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.other:
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_KEY, -1);
                        mPage.getData().putInt(StepHousePage.TYPE_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;

                }
            }
        });

        radioGroup1 = (RadioGroup) view.findViewById(R.id.rgroup2);
        if (mPage.getData().getInt(StepHousePage.ELEV_DATA_KEY) != 0) {
            mPage.getData().putInt(StepHousePage.ELEV_DATA_KEY, mPage.getData().getInt(StepHousePage.ELEV_DATA_KEY));
        } else {
            mPage.getData().putInt(StepHousePage.ELEV_DATA_KEY, 0);
        }
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.elevyes:
                        mPage.getData().putInt(StepHousePage.ELEV_DATA_KEY, 1);
                        mPage.getData().putInt(StepHousePage.ELEV_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.elevno:
                        mPage.getData().putInt(StepHousePage.ELEV_DATA_KEY, 0);
                        mPage.getData().putInt(StepHousePage.ELEV_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.elevnomatter:
                        mPage.getData().putInt(StepHousePage.ELEV_DATA_KEY, -1);
                        mPage.getData().putInt(StepHousePage.ELEV_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                }
            }
        });

        radioGroup2 = (RadioGroup) view.findViewById(R.id.rgroup3);
        if (mPage.getData().getInt(StepHousePage.CAR_DATA_KEY) != 0) {
            mPage.getData().putInt(StepHousePage.CAR_DATA_KEY, mPage.getData().getInt(StepHousePage.CAR_DATA_KEY));
        } else {
            mPage.getData().putInt(StepHousePage.CAR_DATA_KEY, 0);
        }
        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.pspaceyes:
                        mPage.getData().putInt(StepHousePage.CAR_DATA_KEY, 1);
                        mPage.getData().putInt(StepHousePage.CAR_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.pspaceno:
                        mPage.getData().putInt(StepHousePage.CAR_DATA_KEY, 0);
                        mPage.getData().putInt(StepHousePage.CAR_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                    case R.id.pspacenomatter:
                        mPage.getData().putInt(StepHousePage.CAR_DATA_KEY, -1);
                        mPage.getData().putInt(StepHousePage.CAR_DATA_VALUE, i);
                        mPage.notifyDataChanged();
                        break;
                }
            }
        });

        np = (NumberPicker) view.findViewById(R.id.numberpicker1); //單價下限值
        final String[] values = new String[]{"5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "0"};
        np.setMaxValue(values.length - 1);
        np.setMinValue(0);
        np.setValue(mPage.getData().getInt(StepHousePage.PRICE_MIN_DATA_VALUE));
        np.setDisplayedValues(values);
        np.setFocusable(true);
        np.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.PRICE_MIN_DATA_KEY, String.valueOf(values[np.getValue()]));
        mPage.notifyDataChanged();
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.PRICE_MIN_DATA_KEY, String.valueOf(values[i1]));
                mPage.getData().putInt(StepHousePage.PRICE_MIN_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        np2 = (NumberPicker) view.findViewById(R.id.numberpicker2); //單價上限值
        final String[] values2 = new String[]{"10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "70", "80", "90", "100", "110"};
        np2.setMaxValue(values2.length - 1);
        np2.setMinValue(0);
        np2.setValue(mPage.getData().getInt(StepHousePage.PRICE_MAX_DATA_VALUE));
        np2.setDisplayedValues(values2);
        np2.setFocusable(true);
        np2.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.PRICE_MAX_DATA_KEY, String.valueOf(values2[np2.getValue()]));
        mPage.notifyDataChanged();
        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.PRICE_MAX_DATA_KEY, String.valueOf(values2[i1]));
                mPage.getData().putInt(StepHousePage.PRICE_MAX_DATA_VALUE, i1);
                mPage.notifyDataChanged();

            }
        });
        this.setNumberPickerValue(0);

        ch12 = (CheckBox) view.findViewById(R.id.checkboxnp12);
        if (mPage.getData().getInt(StepHousePage.PRICE_MIN_DATA_VALUE) == 0 && mPage.getData().getInt(StepHousePage.PRICE_MAX_DATA_VALUE) == values2.length - 1) {
            ch12.setChecked(true);
            mPage.getData().putString(StepHousePage.PRICE_MIN_DATA_KEY, String.valueOf(0));
            mPage.getData().putString(StepHousePage.PRICE_MAX_DATA_KEY, String.valueOf(values2[values2.length - 1]));
            mPage.getData().putInt(StepHousePage.PRICE_MIN_DATA_VALUE, 0);
            mPage.getData().putInt(StepHousePage.PRICE_MAX_DATA_VALUE, values2.length - 1);
            mPage.notifyDataChanged();
            np.setEnabled(false);
            np2.setEnabled(false);
        }
        ch12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch12.isChecked()) {
                    mPage.getData().putString(StepHousePage.PRICE_MIN_DATA_KEY, String.valueOf(values[0]));
                    mPage.getData().putString(StepHousePage.PRICE_MAX_DATA_KEY, String.valueOf(values2[values2.length - 1]));
                    mPage.getData().putInt(StepHousePage.PRICE_MIN_DATA_VALUE, 0);
                    mPage.getData().putInt(StepHousePage.PRICE_MAX_DATA_VALUE, values2.length - 1);
                    mPage.notifyDataChanged();
                    np.setEnabled(false);
                    np2.setEnabled(false);
                } else {
                    mPage.getData().putString(StepHousePage.PRICE_MIN_DATA_KEY, String.valueOf(values[np.getValue()]));
                    mPage.getData().putString(StepHousePage.PRICE_MAX_DATA_KEY, String.valueOf(values2[np2.getValue()]));
                    mPage.getData().putInt(StepHousePage.PRICE_MIN_DATA_VALUE, np.getValue());
                    mPage.getData().putInt(StepHousePage.PRICE_MAX_DATA_VALUE, np2.getValue());
                    mPage.notifyDataChanged();
                    np.setEnabled(true);
                    np2.setEnabled(true);
                }
            }
        });

        np3 = (NumberPicker) view.findViewById(R.id.numberpicker3); //坪數下限值
        final String[] values3 = new String[]{"0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"};
        np3.setMaxValue(values3.length - 1);
        np3.setMinValue(0);
        np3.setValue(mPage.getData().getInt(StepHousePage.SPACE_MIN_DATA_VALUE));
        np3.setDisplayedValues(values3);
        np3.setFocusable(true);
        np3.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.SPACE_MIN_DATA_KEY, String.valueOf(values3[np3.getValue()]));
        mPage.notifyDataChanged();
        np3.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.SPACE_MIN_DATA_KEY, String.valueOf(values3[i1]));
                mPage.getData().putInt(StepHousePage.SPACE_MIN_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        np4 = (NumberPicker) view.findViewById(R.id.numberpicker4); //坪數上限值
        final String[] values4 = new String[]{"10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"};
        np4.setMaxValue(values4.length - 1);
        np4.setMinValue(0);
        np4.setValue(mPage.getData().getInt(StepHousePage.SPACE_MAX_DATA_VALUE));
        np4.setDisplayedValues(values4);
        np4.setFocusable(true);
        np4.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.SPACE_MAX_DATA_KEY, String.valueOf(values2[np4.getValue()]));
        mPage.notifyDataChanged();
        np4.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.SPACE_MAX_DATA_KEY, String.valueOf(values4[i1]));
                mPage.getData().putInt(StepHousePage.SPACE_MAX_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        ch34 = (CheckBox) view.findViewById(R.id.checkboxnp34);
        if (mPage.getData().getInt(StepHousePage.SPACE_MAX_DATA_VALUE) == 0 && mPage.getData().getInt(StepHousePage.SPACE_MIN_DATA_VALUE) == values4.length - 1) {
            ch34.setChecked(true);
            mPage.getData().putString(StepHousePage.SPACE_MIN_DATA_KEY, String.valueOf(0));
            mPage.getData().putString(StepHousePage.SPACE_MAX_DATA_KEY, String.valueOf(values4[values4.length - 1]));
            mPage.getData().putInt(StepHousePage.SPACE_MIN_DATA_VALUE, 0);
            mPage.getData().putInt(StepHousePage.SPACE_MAX_DATA_VALUE, values4.length - 1);
            mPage.notifyDataChanged();
            np3.setEnabled(false);
            np4.setEnabled(false);
        }
        ch34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch34.isChecked()) {
                    mPage.getData().putString(StepHousePage.SPACE_MIN_DATA_KEY, String.valueOf(0));
                    mPage.getData().putString(StepHousePage.SPACE_MAX_DATA_KEY, String.valueOf(values4[values4.length - 1]));
                    mPage.getData().putInt(StepHousePage.SPACE_MIN_DATA_VALUE, 0);
                    mPage.getData().putInt(StepHousePage.SPACE_MAX_DATA_VALUE, values4.length - 1);
                    mPage.notifyDataChanged();
                    np3.setEnabled(false);
                    np4.setEnabled(false);
                } else {
                    mPage.getData().putString(StepHousePage.SPACE_MIN_DATA_KEY, String.valueOf(values3[np3.getValue()]));
                    mPage.getData().putString(StepHousePage.SPACE_MAX_DATA_KEY, String.valueOf(values4[np4.getValue()]));
                    mPage.getData().putInt(StepHousePage.SPACE_MIN_DATA_VALUE, np3.getValue());
                    mPage.getData().putInt(StepHousePage.SPACE_MAX_DATA_VALUE, np4.getValue());
                    mPage.notifyDataChanged();
                    np3.setEnabled(true);
                    np4.setEnabled(true);
                }
            }
        });

        np5 = (NumberPicker) view.findViewById(R.id.numberpicker5); //屋齡下限值
        final String[] values5 = new String[]{"0", "5", "10", "15", "20", "25", "30", "35", "40"};
        np5.setMaxValue(values5.length - 1);
        np5.setMinValue(0);
        np5.setValue(mPage.getData().getInt(StepHousePage.YEARS_MIN_DATA_VALUE));
        np5.setDisplayedValues(values5);
        np5.setFocusable(true);
        np5.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.YEARS_MIN_DATA_KEY, String.valueOf(values5[np5.getValue()]));
        mPage.notifyDataChanged();
        np5.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.YEARS_MIN_DATA_KEY, String.valueOf(values5[i1]));
                mPage.getData().putInt(StepHousePage.YEARS_MIN_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        np6 = (NumberPicker) view.findViewById(R.id.numberpicker6); //屋齡上限值
        final String[] values6 = new String[]{"10", "15", "20", "25", "30", "35", "40"};
        np6.setMaxValue(values6.length - 1);
        np6.setMinValue(0);
        np6.setValue(mPage.getData().getInt(StepHousePage.YEARS_MAX_DATA_VALUE));
        np6.setDisplayedValues(values6);
        np6.setFocusable(true);
        np6.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.YEARS_MAX_DATA_KEY, String.valueOf(values2[np6.getValue()]));
        mPage.notifyDataChanged();
        np6.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.YEARS_MAX_DATA_KEY, String.valueOf(values6[i1]));
                mPage.getData().putInt(StepHousePage.YEARS_MAX_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        ch56 = (CheckBox) view.findViewById(R.id.checkboxnp56);
        if (mPage.getData().getInt(StepHousePage.YEARS_MAX_DATA_VALUE) == values6.length - 1 && mPage.getData().getInt(StepHousePage.YEARS_MIN_DATA_VALUE) == 0) {
            mPage.getData().putString(StepHousePage.YEARS_MIN_DATA_KEY, String.valueOf(0));
            mPage.getData().putString(StepHousePage.YEARS_MAX_DATA_KEY, String.valueOf(values6[values6.length - 1]));
            mPage.getData().putInt(StepHousePage.YEARS_MIN_DATA_VALUE, 0);
            mPage.getData().putInt(StepHousePage.YEARS_MAX_DATA_VALUE, values6.length - 1);
            np5.setEnabled(false);
            np6.setEnabled(false);
        }
        ch56.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch56.isChecked()) {
                    mPage.getData().putString(StepHousePage.YEARS_MIN_DATA_KEY, String.valueOf(0));
                    mPage.getData().putString(StepHousePage.YEARS_MAX_DATA_KEY, String.valueOf(values6.length - 1));
                    mPage.getData().putInt(StepHousePage.YEARS_MIN_DATA_VALUE, 0);
                    mPage.getData().putInt(StepHousePage.YEARS_MAX_DATA_VALUE, values6.length - 1);
                    np5.setEnabled(false);
                    np6.setEnabled(false);
                } else {
                    mPage.getData().putString(StepHousePage.YEARS_MIN_DATA_KEY, String.valueOf(values5[np5.getValue()]));
                    mPage.getData().putString(StepHousePage.YEARS_MAX_DATA_KEY, String.valueOf(values6[np6.getValue()]));
                    mPage.getData().putInt(StepHousePage.YEARS_MIN_DATA_VALUE, np5.getValue());
                    mPage.getData().putInt(StepHousePage.YEARS_MAX_DATA_VALUE, np6.getValue());
                    np5.setEnabled(true);
                    np6.setEnabled(true);
                }
            }
        });

        np7 = (NumberPicker) view.findViewById(R.id.numberpicker7); //房數
        final String[] values7 = new String[]{"0", "1", "2", "3", "4", "5", "6"};
        np7.setMaxValue(values7.length - 1);
        np7.setMinValue(0);
        np7.setValue(mPage.getData().getInt(StepHousePage.ROOM_DATA_VALUE));
        np7.setDisplayedValues(values7);
        np7.setFocusable(true);
        np7.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.ROOM_DATA_KEY, String.valueOf(values7[np7.getValue()]));
        mPage.notifyDataChanged();
        np7.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.ROOM_DATA_KEY, String.valueOf(values7[i1]));
                mPage.getData().putInt(StepHousePage.ROOM_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        np8 = (NumberPicker) view.findViewById(R.id.numberpicker8); //廳數
        final String[] values8 = new String[]{"0", "1", "2", "3"};
        np8.setMaxValue(values8.length - 1);
        np8.setMinValue(0);
        np8.setValue(mPage.getData().getInt(StepHousePage.LROOM_DATA_VALUE));
        np8.setDisplayedValues(values8);
        np8.setFocusable(true);
        np8.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.LROOM_DATA_KEY, String.valueOf(values8[np8.getValue()]));
        mPage.notifyDataChanged();
        np8.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.LROOM_DATA_KEY, String.valueOf(values8[i1]));
                mPage.getData().putInt(StepHousePage.LROOM_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        np9 = (NumberPicker) view.findViewById(R.id.numberpicker9); //衛數
        final String[] values9 = new String[]{"0", "1", "2", "3", "4"};
        np9.setMaxValue(values9.length - 1);
        np9.setMinValue(0);
        np9.setValue(mPage.getData().getInt(StepHousePage.BATH_DATA_VALUE));
        np9.setDisplayedValues(values9);
        np9.setFocusable(true);
        np9.setFocusableInTouchMode(true);
        mPage.getData().putString(StepHousePage.BATH_DATA_KEY, String.valueOf(values9[np9.getValue()]));
        mPage.notifyDataChanged();
        np9.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                mPage.getData().putString(StepHousePage.BATH_DATA_KEY, String.valueOf(values9[i1]));
                mPage.getData().putInt(StepHousePage.BATH_DATA_VALUE, i1);
                mPage.notifyDataChanged();
            }
        });
        this.setNumberPickerValue(0);

        ch789 = (CheckBox) view.findViewById(R.id.checkboxnp789);
        if (mPage.getData().getInt(StepHousePage.ROOM_DATA_VALUE) == -1 && mPage.getData().getInt(StepHousePage.LROOM_DATA_VALUE) == -1 && mPage.getData().getInt(StepHousePage.BATH_DATA_VALUE) == -1) {
            mPage.getData().putString(StepHousePage.ROOM_DATA_KEY, String.valueOf(-1));
            mPage.getData().putString(StepHousePage.LROOM_DATA_KEY, String.valueOf(-1));
            mPage.getData().putString(StepHousePage.BATH_DATA_KEY, String.valueOf(-1));
            mPage.getData().putInt(StepHousePage.ROOM_DATA_VALUE, -1);
            mPage.getData().putInt(StepHousePage.LROOM_DATA_VALUE, -1);
            mPage.getData().putInt(StepHousePage.BATH_DATA_VALUE, -1);
            np7.setEnabled(false);
            np8.setEnabled(false);
            np9.setEnabled(false);
        }
        ch789.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch789.isChecked()) {
                    mPage.getData().putString(StepHousePage.ROOM_DATA_KEY, String.valueOf(-1));
                    mPage.getData().putString(StepHousePage.LROOM_DATA_KEY, String.valueOf(-1));
                    mPage.getData().putString(StepHousePage.BATH_DATA_KEY, String.valueOf(-1));
                    mPage.getData().putInt(StepHousePage.ROOM_DATA_VALUE, -1);
                    mPage.getData().putInt(StepHousePage.LROOM_DATA_VALUE, -1);
                    mPage.getData().putInt(StepHousePage.BATH_DATA_VALUE, -1);
                    np7.setEnabled(false);
                    np8.setEnabled(false);
                    np9.setEnabled(false);
                } else {
                    mPage.getData().putString(StepHousePage.ROOM_DATA_KEY, String.valueOf(values7[np7.getValue()]));
                    mPage.getData().putString(StepHousePage.LROOM_DATA_KEY, String.valueOf(values8[np8.getValue()]));
                    mPage.getData().putString(StepHousePage.BATH_DATA_KEY, String.valueOf(values9[np9.getValue()]));
                    mPage.getData().putInt(StepHousePage.ROOM_DATA_VALUE, np7.getValue());
                    mPage.getData().putInt(StepHousePage.LROOM_DATA_VALUE, np8.getValue());
                    mPage.getData().putInt(StepHousePage.BATH_DATA_VALUE, np9.getValue());
                    np7.setEnabled(true);
                    np8.setEnabled(true);
                    np9.setEnabled(true);
                }
            }
        });

        return view;
    }

    public void setNumberPickerValue(int val) {
        if (np != null) {
            np.setValue(val / 10 - 1);
        }
    }

}
