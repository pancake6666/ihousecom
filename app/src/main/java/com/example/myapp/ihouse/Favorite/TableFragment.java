package com.example.myapp.ihouse.favorite;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.sqlConnect.House;
import com.example.myapp.ihouse.sqlConnect.HouseDAO;

import java.util.ArrayList;

public class TableFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    HouseDAO houseDAO;


    public static TableFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        TableFragment fragment = new TableFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        houseDAO = new HouseDAO(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_table, container, false);
        TextView ida = (TextView) view.findViewById(R.id.ida);
        TextView idb = (TextView) view.findViewById(R.id.idb);
        TextView idap = (TextView) view.findViewById(R.id.idaprice);
        TextView idbp = (TextView) view.findViewById(R.id.idbprice);
        House one = houseDAO.get((long) integers.get(0));
        House two = houseDAO.get((long) integers.get(1));
        ida.setText(one.getAddress());
        idb.setText(two.getAddress());
        idap.setText(String.valueOf(one.getPrice()));
        idbp.setText(String.valueOf(two.getPrice()));

        return view;
    }
}
