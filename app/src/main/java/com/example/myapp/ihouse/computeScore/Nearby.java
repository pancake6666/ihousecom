/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.myapp.ihouse.computeScore;

/**
 * @author changyuen
 */
public class Nearby {
    private int type;
    private int star;
    private double real;

    public Nearby() {
    }

    public Nearby(int type, int star, double real) {
        this.type = type;
        this.star = star;
        this.real = real;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }

}
