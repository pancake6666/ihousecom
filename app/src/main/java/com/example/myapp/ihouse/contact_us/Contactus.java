package com.example.myapp.ihouse.contact_us;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;

import com.example.myapp.ihouse.MainActivity;
import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.favorite.MyLoveActivity;
import com.example.myapp.ihouse.map.MapsActivity;
import com.example.myapp.ihouse.mod.ModActivity;

public class Contactus extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Button button;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);

//        ImageView back = (ImageView) findViewById(R.id.contact);
//        Resources r = this.getApplicationContext().getResources();
//        InputStream is = r.openRawResource(R.raw.contact2);
//        Bitmap bitmap = BitmapFactory.decodeStream(is);
//        BitmapDrawable drawable = new BitmapDrawable(bitmap);
//        back.setBackground(drawable);
//        button = (Button) findViewById(R.id.button);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "送出", Toast.LENGTH_SHORT).show();
//            }
//        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.main:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.favorite:
                startActivity(new Intent(this, MyLoveActivity.class));
                break;
            case R.id.map:
                startActivity(new Intent(this, MapsActivity.class));
                break;
            case R.id.mod:
                startActivity(new Intent(this, ModActivity.class));
                break;
        }
        return false;
    }
}
