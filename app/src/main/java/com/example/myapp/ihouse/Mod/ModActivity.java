package com.example.myapp.ihouse.mod;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapp.ihouse.MainActivity;
import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.contact_us.Contactus;
import com.example.myapp.ihouse.favorite.MyLoveActivity;
import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.map.MapsActivity;


public class ModActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static Button finish;
    private TabLayout mTabLayout;
    public static Hweight hweight;
    private int m_id;
    int a = 0;
    private GetURL getURL;
    private boolean check = true;
    private ProgressDialog progressDialog;
    public static DrawerLayout mDrawerLayout;
    private int[] mTabsIcons = {
            R.drawable.building,
            R.drawable.traffic,
            R.drawable.livelihood,
            R.drawable.nimby};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        m_id = preferences.getInt("ID", 0);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        getURL = new GetURL();

        loadgetHweight gethweight = new loadgetHweight();
        gethweight.execute(m_id);
        progressDialog = new ProgressDialog(ModActivity.this, 5);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("讀取中");
        progressDialog.show();
        // Setup the viewPager


        finish = (Button) findViewById(R.id.finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setTitle("提示");
                progressDialog.setMessage("儲存中");
                progressDialog.show();
                InsertHW ihw = new InsertHW();
                ihw.execute(m_id);
            }
        });


    }


    private class MyPagerAdapter extends FragmentPagerAdapter {

        public final int PAGE_COUNT = 4;

        private final String[] mTabsTitle = {"理想屋型", "交通", "生活機能", "鄰避設施"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View view = LayoutInflater.from(ModActivity.this).inflate(R.layout.custom_tab_register, null);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(mTabsTitle[position]);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(mTabsIcons[position]);
            return view;
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return HouseModFragment.newInstance(1);
                case 1:
                    return TrafficFragment.newInstance(2);
                case 2:
                    return FunctionFragment.newInstance(3);
                case 3:
                    return NIMBYFragment.newInstance(4);

            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }

    class loadgetHweight extends AsyncTask<Integer, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... integers) {
            hweight = getURL.getHweight(integers[0]);
            return null;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            hweight.setMid(m_id);
            a = 1;
            check = false;

            ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
            MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
            if (viewPager != null)
                viewPager.setAdapter(pagerAdapter);

            mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
            if (mTabLayout != null) {
                mTabLayout.setupWithViewPager(viewPager);

                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    TabLayout.Tab tab = mTabLayout.getTabAt(i);
                    if (tab != null)
                        tab.setCustomView(pagerAdapter.getTabView(i));
                }

                mTabLayout.getTabAt(0).getCustomView().setSelected(true);
            }
            progressDialog.hide();
        }
    }

    class InsertHW extends AsyncTask<Integer, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... integers) {
            getURL.putHweight(hweight);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            a = 2;
            check = false;
            progressDialog.hide();
            startActivity(new Intent(ModActivity.this, MainActivity.class));
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                return true;
            case R.id.favorite:
                startActivity(new Intent(getApplicationContext(), MyLoveActivity.class));
                return true;
            case R.id.mod:
                startActivity(new Intent(getApplicationContext(), MapsActivity.class));
                return true;
            case R.id.contact:
                startActivity(new Intent(getApplicationContext(), Contactus.class));
                return true;
            default:
                return true;
        }
    }

}


