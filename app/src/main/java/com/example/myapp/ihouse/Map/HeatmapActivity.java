package com.example.myapp.ihouse.map;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.example.myapp.ihouse.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HeatMapActivity extends BaseDemoActivity {

    private List<WeightedLatLng> data;
    private WeightedLatLng weightedLatLng;
    private HeatmapTileProvider mProvider;
    private TileOverlay mOverlay;
    private List<LatLng> list;
    private ProgressDialog progressDialog;
    private static final String REGEX_INPUT_BOUNDARY_BEGINNING = "\\A";

    @Override
    protected int getLayoutId() {
        return R.layout.heatmap;
    }

    @Override
    protected void startDemo() {
        LoadJsonTask loadJsonTask = new LoadJsonTask();
        InputStream inputStream = getResources().openRawResource(R.raw.estate_fix);
        loadJsonTask.execute(inputStream);
    }


    //非同步讀入json
    private class LoadJsonTask extends AsyncTask<InputStream, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(InputStream... params) {
            long startTime = System.currentTimeMillis();
            data = new ArrayList<>();
            list = new ArrayList<>();
            InputStream inputStream = params[0];
            try {
                String json = new Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next();
                JSONArray array = new JSONArray(json);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    double lat = object.getDouble("lat");
                    double lng = object.getDouble("lon");
                    LatLng latLng = new LatLng(lat, lng);
                    double tPrice = object.getDouble("price");
                    weightedLatLng = new WeightedLatLng(latLng, tPrice / 10);
                    list.add(latLng);
                    data.add(weightedLatLng);
                }
                long endTime = System.currentTimeMillis();
                long totTime = endTime - startTime;
                System.out.println("Using Time:" + totTime);
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                mProvider = new HeatmapTileProvider.Builder().weightedData(data).radius(50).build();
                mOverlay = getMap().addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                progressDialog.dismiss();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(HeatMapActivity.this, "", "載入中");

        }

    }
}
