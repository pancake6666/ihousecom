package com.example.myapp.ihouse.favorite;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.example.myapp.ihouse.R;

/**
 * Created by changyuen on 2016/9/20.
 */
public class CustomizeColor extends LinearLayout {
    private LayoutInflater inflater;
    private Paint paint = null;

    public CustomizeColor(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.cardviewcolor, this, true);
        paint = new Paint();
        setWillNotDraw(false);

    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(Color.BLUE);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(30);
        canvas.drawLine(0, 0, 0, getMeasuredHeight(), paint);
        invalidate();

    }
}
