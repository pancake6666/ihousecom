package com.example.myapp.ihouse.mod;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RatingBar;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.httpGet.Hweight;

import java.util.ArrayList;

/**
 * Created by Alex on 2016/8/9.
 */
public class TrafficFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    NumberPicker np1 = null;
    NumberPicker np2 = null;
    NumberPicker np3 = null;
    NumberPicker np4 = null;
    NumberPicker np5 = null;
    NumberPicker np6 = null;
    NumberPicker np7 = null;
    NumberPicker np8 = null;
    Hweight hw = ModActivity.hweight;


    public static TrafficFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        TrafficFragment fragment = new TrafficFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trafficfragment1, container, false);


        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.RatingBarId);
        final RatingBar ratingBar2 = (RatingBar) view.findViewById(R.id.RatingBarId2);
        final RatingBar ratingBar3 = (RatingBar) view.findViewById(R.id.RatingBarId3);
        final RatingBar ratingBar4 = (RatingBar) view.findViewById(R.id.RatingBarId4);

        ratingBar.setRating(hw.getMRTstar());
        ratingBar2.setRating(hw.getTRAstar());
        ratingBar3.setRating(hw.getTHSRstar());
        ratingBar4.setRating(hw.getFwaystar());

        np1 = (NumberPicker) view.findViewById(R.id.np1);
        np2 = (NumberPicker) view.findViewById(R.id.np2);
        np3 = (NumberPicker) view.findViewById(R.id.np3);
        np4 = (NumberPicker) view.findViewById(R.id.np4);
        np5 = (NumberPicker) view.findViewById(R.id.np5);
        np6 = (NumberPicker) view.findViewById(R.id.np6);
        np7 = (NumberPicker) view.findViewById(R.id.np7);
        np8 = (NumberPicker) view.findViewById(R.id.np8);

        final String[] values = new String[]{"0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800", "1900", "2000"};

        np1.setMaxValue(values.length - 1);
        np1.setDisplayedValues(values);
        np1.setMinValue(0);
        np1.setValue(FindIndex(values,hw.getMRTmin()));

        np2.setMaxValue(values.length - 1);
        np2.setMinValue(0);
        np2.setDisplayedValues(values);
        np2.setValue(FindIndex(values,hw.getMRTmax()));

        np3.setMaxValue(values.length - 1);
        np3.setDisplayedValues(values);
        np3.setMinValue(0);
        np3.setValue(FindIndex(values,hw.getTRAmin()));

        np4.setMaxValue(values.length - 1);
        np4.setMinValue(0);
        np4.setDisplayedValues(values);
        np4.setValue(FindIndex(values,hw.getTRAmax()));

        np5.setMaxValue(values.length - 1);
        np5.setDisplayedValues(values);
        np5.setMinValue(0);
        np5.setValue(FindIndex(values,hw.getTHSRmin()));

        np6.setMaxValue(values.length - 1);
        np6.setMinValue(0);
        np6.setDisplayedValues(values);
        np6.setValue(FindIndex(values,hw.getTHSRmax()));

        np7.setMaxValue(values.length - 1);
        np7.setDisplayedValues(values);
        np7.setMinValue(0);
        np7.setValue(FindIndex(values,hw.getFwaymin()));

        np8.setMaxValue(values.length - 1);
        np8.setMinValue(0);
        np8.setDisplayedValues(values);
        np8.setValue(FindIndex(values,hw.getFwaymax()));



        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setMRTstar((int)rating);
            }
        });

        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setTRAstar((int)rating);
            }
        });

        ratingBar3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setTHSRstar((int)rating);
            }
        });

        ratingBar4.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setFwaystar((int)rating);
            }
        });


        np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setMRTmin(Integer.valueOf(values[newVal]));
            }
        });

        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setMRTmax(Integer.valueOf(values[newVal]));
            }
        });

        np3.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setTRAmin(Integer.valueOf(values[newVal]));
            }
        });

        np4.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setTRAmax(Integer.valueOf(values[newVal]));
            }
        });

        np5.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setTHSRmin(Integer.valueOf(values[newVal]));
            }
        });

        np6.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setTHSRmax(Integer.valueOf(values[newVal]));
            }
        });

        np7.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setFwaymin(Integer.valueOf(values[newVal]));
            }
        });

        np8.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setFwaymax(Integer.valueOf(values[newVal]));
            }
        });




        Button clear = (Button) view.findViewById(R.id.clear);

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ratingBar.setRating(0);
                hw.setMRTstar(0);
                np1.setValue(0);
                hw.setMRTmin(0);
                np2.setValue(0);
                hw.setMRTmax(0);

            }
        });


        Button clear2 = (Button) view.findViewById(R.id.clear2);

        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ratingBar2.setRating(0);
                hw.setTRAstar(0);
                np3.setValue(0);
                hw.setTRAmin(0);
                np4.setValue(0);
                hw.setTRAmax(0);

            }
        });


        Button clear3 = (Button) view.findViewById(R.id.clear3);

        clear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ratingBar3.setRating(0);
                hw.setTHSRstar(0);
                np5.setValue(0);
                hw.setTHSRmin(0);
                np6.setValue(0);
                hw.setTHSRmax(0);

            }
        });


        Button clear4 = (Button) view.findViewById(R.id.clear4);

        clear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ratingBar4.setRating(0);
                hw.setFwaystar(0);
                np7.setValue(0);
                hw.setFwaymin(0);
                np8.setValue(0);
                hw.setFwaymax(0);

            }
        });


        return view;


    }

    public int FindIndex(String[] values, Integer num) {
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(num.toString())) {
                index = i;
            }

        }
        return index;
    }
}
