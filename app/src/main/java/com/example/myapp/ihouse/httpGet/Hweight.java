package com.example.myapp.ihouse.httpGet;

public class Hweight implements java.io.Serializable {
    private int EUmax;
    private int LCmax;
    private int Fwaystar;
    private int EULB;
    private int HTmax;
    private int Spacemin;
    private int LFmin;
    private int mid;
    private int type;
    private int WC;
    private int Fwaymax;
    private int THSRmax;
    private int LRoom;
    private int HTMS;
    private int Elevator;
    private int TRAmin;
    private int EUmin;
    private int Yearmax;
    private int TRAstar;
    private int HTIS;
    private int HTmin;
    private int EUJS;
    private int LCPS;
    private int HTGS;
    private int Spacemax;
    private int THSRstar;
    private int ID;
    private int EUHS;
    private int Room;
    private int Fwaymin;
    private int LFPS;
    private int HTAS;
    private int MRTstar;
    private int Yearmin;
    private int MRTmax;
    private int LFHS;
    private int Pricemax;
    private int LFDS;
    private int MRTmin;
    private int LCmin;
    private int LCSS;
    private int Car;
    private int TRAmax;
    private int LFSS;
    private int Pricemin;
    private int LFmax;
    private int EUES;
    private int THSRmin;
    private String Section;

    public int getEUmax() {
        return this.EUmax;
    }

    public void setEUmax(int EUmax) {
        this.EUmax = EUmax;
    }

    public int getLCmax() {
        return this.LCmax;
    }

    public void setLCmax(int LCmax) {
        this.LCmax = LCmax;
    }

    public int getFwaystar() {
        return this.Fwaystar;
    }

    public void setFwaystar(int Fwaystar) {
        this.Fwaystar = Fwaystar;
    }

    public int getEULB() {
        return this.EULB;
    }

    public void setEULB(int EULB) {
        this.EULB = EULB;
    }

    public int getHTmax() {
        return this.HTmax;
    }

    public void setHTmax(int HTmax) {
        this.HTmax = HTmax;
    }

    public int getSpacemin() {
        return this.Spacemin;
    }

    public void setSpacemin(int Spacemin) {
        this.Spacemin = Spacemin;
    }

    public int getLFmin() {
        return this.LFmin;
    }

    public void setLFmin(int LFmin) {
        this.LFmin = LFmin;
    }

    public int getMid() {
        return this.mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getWC() {
        return this.WC;
    }

    public void setWC(int WC) {
        this.WC = WC;
    }

    public int getFwaymax() {
        return this.Fwaymax;
    }

    public void setFwaymax(int Fwaymax) {
        this.Fwaymax = Fwaymax;
    }

    public int getTHSRmax() {
        return this.THSRmax;
    }

    public void setTHSRmax(int THSRmax) {
        this.THSRmax = THSRmax;
    }

    public int getLRoom() {
        return this.LRoom;
    }

    public void setLRoom(int LRoom) {
        this.LRoom = LRoom;
    }

    public int getHTMS() {
        return this.HTMS;
    }

    public void setHTMS(int HTMS) {
        this.HTMS = HTMS;
    }

    public int getElevator() {
        return this.Elevator;
    }

    public void setElevator(int Elevator) {
        this.Elevator = Elevator;
    }

    public int getTRAmin() {
        return this.TRAmin;
    }

    public void setTRAmin(int TRAmin) {
        this.TRAmin = TRAmin;
    }

    public int getEUmin() {
        return this.EUmin;
    }

    public void setEUmin(int EUmin) {
        this.EUmin = EUmin;
    }

    public int getYearmax() {
        return this.Yearmax;
    }

    public void setYearmax(int Yearmax) {
        this.Yearmax = Yearmax;
    }

    public int getTRAstar() {
        return this.TRAstar;
    }

    public void setTRAstar(int TRAstar) {
        this.TRAstar = TRAstar;
    }

    public int getHTIS() {
        return this.HTIS;
    }

    public void setHTIS(int HTIS) {
        this.HTIS = HTIS;
    }

    public int getHTmin() {
        return this.HTmin;
    }

    public void setHTmin(int HTmin) {
        this.HTmin = HTmin;
    }

    public int getEUJS() {
        return this.EUJS;
    }

    public void setEUJS(int EUJS) {
        this.EUJS = EUJS;
    }

    public int getLCPS() {
        return this.LCPS;
    }

    public void setLCPS(int LCPS) {
        this.LCPS = LCPS;
    }

    public int getHTGS() {
        return this.HTGS;
    }

    public void setHTGS(int HTGS) {
        this.HTGS = HTGS;
    }

    public int getSpacemax() {
        return this.Spacemax;
    }

    public void setSpacemax(int Spacemax) {
        this.Spacemax = Spacemax;
    }

    public int getTHSRstar() {
        return this.THSRstar;
    }

    public void setTHSRstar(int THSRstar) {
        this.THSRstar = THSRstar;
    }

    public int getID() {
        return this.ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getEUHS() {
        return this.EUHS;
    }

    public void setEUHS(int EUHS) {
        this.EUHS = EUHS;
    }

    public int getRoom() {
        return this.Room;
    }

    public void setRoom(int Room) {
        this.Room = Room;
    }

    public int getFwaymin() {
        return this.Fwaymin;
    }

    public void setFwaymin(int Fwaymin) {
        this.Fwaymin = Fwaymin;
    }

    public int getLFPS() {
        return this.LFPS;
    }

    public void setLFPS(int LFPS) {
        this.LFPS = LFPS;
    }

    public int getHTAS() {
        return this.HTAS;
    }

    public void setHTAS(int HTAS) {
        this.HTAS = HTAS;
    }

    public int getMRTstar() {
        return this.MRTstar;
    }

    public void setMRTstar(int MRTstar) {
        this.MRTstar = MRTstar;
    }

    public int getYearmin() {
        return this.Yearmin;
    }

    public void setYearmin(int Yearmin) {
        this.Yearmin = Yearmin;
    }

    public int getMRTmax() {
        return this.MRTmax;
    }

    public void setMRTmax(int MRTmax) {
        this.MRTmax = MRTmax;
    }

    public int getLFHS() {
        return this.LFHS;
    }

    public void setLFHS(int LFHS) {
        this.LFHS = LFHS;
    }

    public int getPricemax() {
        return this.Pricemax;
    }

    public void setPricemax(int Pricemax) {
        this.Pricemax = Pricemax;
    }

    public int getLFDS() {
        return this.LFDS;
    }

    public void setLFDS(int LFDS) {
        this.LFDS = LFDS;
    }

    public int getMRTmin() {
        return this.MRTmin;
    }

    public void setMRTmin(int MRTmin) {
        this.MRTmin = MRTmin;
    }

    public int getLCmin() {
        return this.LCmin;
    }

    public void setLCmin(int LCmin) {
        this.LCmin = LCmin;
    }

    public int getLCSS() {
        return this.LCSS;
    }

    public void setLCSS(int LCSS) {
        this.LCSS = LCSS;
    }

    public int getCar() {
        return this.Car;
    }

    public void setCar(int Car) {
        this.Car = Car;
    }

    public int getTRAmax() {
        return this.TRAmax;
    }

    public void setTRAmax(int TRAmax) {
        this.TRAmax = TRAmax;
    }

    public int getLFSS() {
        return this.LFSS;
    }

    public void setLFSS(int LFSS) {
        this.LFSS = LFSS;
    }

    public int getPricemin() {
        return this.Pricemin;
    }

    public void setPricemin(int Pricemin) {
        this.Pricemin = Pricemin;
    }

    public int getLFmax() {
        return this.LFmax;
    }

    public void setLFmax(int LFmax) {
        this.LFmax = LFmax;
    }

    public int getEUES() {
        return this.EUES;
    }

    public void setEUES(int EUES) {
        this.EUES = EUES;
    }

    public int getTHSRmin() {
        return this.THSRmin;
    }

    public void setTHSRmin(int THSRmin) {
        this.THSRmin = THSRmin;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }
}
