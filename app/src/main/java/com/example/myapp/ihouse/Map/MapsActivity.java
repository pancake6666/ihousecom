package com.example.myapp.ihouse.map;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.example.myapp.ihouse.MainActivity;
import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.analysis.Activity_Analysis;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.contact_us.Contactus;
import com.example.myapp.ihouse.favorite.MyLoveActivity;
import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.MyData;
import com.example.myapp.ihouse.httpGet.RealEstateDate;
import com.example.myapp.ihouse.httpGet.RealEstateDateLs;
import com.example.myapp.ihouse.httpGet.SaleHouse;
import com.example.myapp.ihouse.httpGet.SaleHouseLs;
import com.example.myapp.ihouse.mod.ModActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.rey.material.widget.ProgressView;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends BaseMapActivity implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<MarkerItem>, ClusterManager.OnClusterInfoWindowClickListener<MarkerItem>, ClusterManager.OnClusterItemClickListener<MarkerItem>, ClusterManager.OnClusterItemInfoWindowClickListener<MarkerItem>, View.OnClickListener
        , GoogleMap.OnCameraMoveListener, Animation.AnimationListener {

    private ClusterManager<MarkerItem> clusterManager;
    private ProgressDialog psDialog;
    private List<MarkerItem> realItems = new ArrayList<>();
    private List<MarkerItem> saleItems = new ArrayList<>();
    private List<MarkerItem> recommend = new ArrayList<>();
    private FloatingSearchView mSearchView;
    private LruCache<String, Bitmap> mMemoryCacheIcon;
    private static final String REGEX_INPUT_BOUNDARY_BEGINNING = "\\A";
    private SharedPreferences sharedPreferences;
    private GetURL httpGet;
    private int m_id;
    private Hweight hweight;
    Intent intent;

    Animation slide_to_down;
    Animation slide_to_up;
    Animation rotate;

    private final static String mLogTag = "GeoJsonDemo";
    private CardView panel;
    private MaterialBetterSpinner area_spinner;
    private MaterialBetterSpinner type_spinner;
    private MaterialBetterSpinner price_spinner;
    private MaterialBetterSpinner space_spinner;
    private MaterialBetterSpinner year_spinner;
    private ArrayList<String> advList = new ArrayList<>();
    private Button startBtn;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private FloatingActionButton fab_add;
    private FloatingActionButton fab_location;
    private Boolean locationGet = false;
    private boolean isLocation = false;
    private LatLng latLng;
    private String addressN;

    private BottomSheetBehavior sheet;
    private ProgressView pv_circular;
    private LinearLayout info;
    private Button anal;

    public static RealHouse realHouse;
    public static String analysistype;
    public static MyData myData;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_maps;
    }

    @Override
    protected void startDemo() {
        //地圖定位設定
        //getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.047908, 121.517315), 13));
        getMap().getUiSettings().setMapToolbarEnabled(false);
        getMap().setOnCameraMoveListener(this);

        //資料載入
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        m_id = sharedPreferences.getInt("ID", 0);
        clusterManager = new ClusterManager<>(this, getMap());
        LoadJsonTask loadJsonTask = new LoadJsonTask();
        loadJsonTask.execute(0);

//        Recommend recommend = new Recommend();
//        recommend.execute(0);


        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);

        fab_add = (FloatingActionButton) findViewById(R.id.fab_add);
        fab_location = (FloatingActionButton) findViewById(R.id.fab_location);

        navigationViewL = (NavigationView) findViewById(R.id.nav_view);
        navigationViewL.setNavigationItemSelectedListener(this);
        mSearchView.attachNavigationDrawerToMenuButton(mDrawerLayout);
        setupFloatingSearch();
        mSearchView.bringToFront();

        panel = (CardView) findViewById(R.id.advancedPanel);
        panel.setBackgroundColor(Color.argb(255, 255, 255, 255));
        startBtn = (Button) findViewById(R.id.panelbtn);
        startBtn.setOnClickListener(this);
        enableMyLocation();
        locationStatus();
        //setupFloatingSearch();
        setFloatingBtn();
        //showMissingPermissionError();

        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCacheIcon = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };


//        advList.set(0, "%25");//區
//        advList.set(1, "*");//屋型
//        //價錢
//        advList.set(2, "0");
//        advList.set(3, "1000");
//        //坪數
//        advList.set(4, "0");
//        advList.set(5, "1000");
//        //屋齡
//        advList.set(6, "0");
//        advList.set(7, "1000");


        area_spinner = (MaterialBetterSpinner) findViewById(R.id.area_spinner);
        type_spinner = (MaterialBetterSpinner) findViewById(R.id.type_spinner);
        price_spinner = (MaterialBetterSpinner) findViewById(R.id.price_spinner);
        space_spinner = (MaterialBetterSpinner) findViewById(R.id.space_spinner);
        year_spinner = (MaterialBetterSpinner) findViewById(R.id.year_spinner);

        String[] areaList = {"不限", "中正區", "大同區", "中山區", "松山區", "大安區", "萬華區", "信義區", "士林區", "北投區", "內湖區", "南港區", "文山區"};
        String[] typeList = {"不限", "公寓", "住宅大樓", "套房", "透天厝", "華廈"};
        String[] priceList = {"不限", "20萬以下", "20~30萬", "30~50萬", "50~70萬", "70萬以上"};
        String[] spaceList = {"不限", "20坪以下", "20~30坪", "30~40坪", "40~50坪", "50坪以上"};
        String[] yearList = {"不限", "5年以下", "5~10年", "10~20年", "20~30年", "30~40年", "40年以上"};

        final ArrayAdapter<String> areaAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, areaList);
        final ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, typeList);
        final ArrayAdapter<String> priceAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, priceList);
        final ArrayAdapter<String> spaceAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, spaceList);
        final ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, yearList);

        area_spinner.setAdapter(areaAdapter);
        //area_spinner.setText(areaAdapter.getItem(0));
        area_spinner.setHintTextColor(Color.BLACK);
        area_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (area_spinner.getText().toString().equals("不限")) {
                    advList.set(0, "%25");
                } else {
                    advList.set(0, area_spinner.getText().toString());
                }
            }
        });

        type_spinner.setAdapter(typeAdapter);
        type_spinner.setHintTextColor(Color.BLACK);
        type_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                switch (type_spinner.getText().toString()) {
                    case "不限":
                        advList.set(1, "*");
                        break;
                    case "公寓":
                        advList.set(1, "1");
                        break;
                    case "住宅大樓":
                        advList.set(1, "2");
                        break;
                    case "套房":
                        advList.set(1, "4");
                        break;
                    case "透天厝":
                        advList.set(1, "5");
                        break;
                    case "華廈":
                        advList.set(1, "6");
                        break;
                }

            }
        });

        price_spinner.setAdapter(priceAdapter);
        //price_spinner.setText(priceAdapter.getItem(0));
        price_spinner.setHintTextColor(Color.BLACK);
        price_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                switch (price_spinner.getText().toString()) {
                    case "不限":
                        advList.set(2, "0");
                        advList.set(3, "1000");
                        break;
                    case "20萬以下":
                        advList.set(2, "0");
                        advList.set(3, "20");
                        break;
                    case "20~30萬":
                        advList.set(2, "20");
                        advList.set(3, "30");
                        break;
                    case "30~50萬":
                        advList.set(2, "30");
                        advList.set(3, "50");
                        break;
                    case "50~70萬":
                        advList.set(2, "50");
                        advList.set(3, "70");
                        break;
                    case "70萬以上":
                        advList.set(2, "70");
                        advList.set(3, "1000");
                        break;
                }
            }
        });

        space_spinner.setAdapter(spaceAdapter);
        //space_spinner.setText(spaceAdapter.getItem(0));
        space_spinner.setHintTextColor(Color.BLACK);
        space_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                switch (space_spinner.getText().toString()) {
                    case "不限":
                        advList.set(4, "0");
                        advList.set(5, "1000");
                        break;
                    case "20坪以下":
                        advList.set(4, "0");
                        advList.set(5, "20");
                        break;
                    case "20~30坪":
                        advList.set(4, "20");
                        advList.set(5, "30");
                        break;
                    case "30~40坪":
                        advList.set(4, "30");
                        advList.set(5, "40");
                        break;
                    case "40~50坪":
                        advList.set(4, "40");
                        advList.set(5, "50");
                        break;
                    case "50坪以上":
                        advList.set(4, "50");
                        advList.set(5, "1000");
                        break;
                }
            }
        });

        year_spinner.setAdapter(yearAdapter);
        //year_spinner.setText(yearAdapter.getItem(0));
        year_spinner.setHintTextColor(Color.BLACK);
        year_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                switch (year_spinner.getText().toString()) {
                    case "不限":
                        advList.set(6, "0");
                        advList.set(7, "1000");
                        break;
                    case "5年以下":
                        advList.set(6, "0");
                        advList.set(7, "5");
                        break;
                    case "5~10年":
                        advList.set(6, "5");
                        advList.set(7, "10");
                        break;
                    case "10~20年":
                        advList.set(6, "10");
                        advList.set(7, "20");
                        break;
                    case "20~30年":
                        advList.set(6, "20");
                        advList.set(7, "30");
                        break;
                    case "30~40年":
                        advList.set(6, "30");
                        advList.set(7, "40");
                        break;
                    case "40年以上":
                        advList.set(6, "40");
                        advList.set(7, "1000");
                        break;
                }
            }
        });

        fab_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager status = (LocationManager) (getApplication().getSystemService(LOCATION_SERVICE));
                if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplication(), "請打開位置權限", Toast.LENGTH_LONG).show();
                    final Intent i = new Intent();
                    i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.setData(Uri.parse("package:" + getPackageName()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(new Intent(i));
                } else {
                    if (status.isProviderEnabled(LocationManager.GPS_PROVIDER) || status.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        onMyLocationButtonClick();
                        try {
                            getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(getMap().getMyLocation().getLatitude(), getMap().getMyLocation().getLongitude()), 15));
                            isLocation = true;
                        } catch (Exception e) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    } else {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }
            }
        });

        sheet = BottomSheetBehavior.from(findViewById(R.id.bottom_sheet));
        pv_circular = (ProgressView) findViewById(R.id.progress_pv_circular);

        info = (LinearLayout) findViewById(R.id.infoTable);
        Button close = (Button) findViewById(R.id.close_button);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        anal = (Button) findViewById(R.id.anal_button);
        intent = new Intent(this, Activity_Analysis.class);

        slide_to_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_in);
        slide_to_down.setAnimationListener(this);

        slide_to_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up_out);
        slide_to_up.setAnimationListener(this);

        rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
        rotate.setAnimationListener(this);
    }

    private void locationStatus() {
        LocationManager status = (LocationManager) (this.getSystemService(LOCATION_SERVICE));
        System.out.println(status.isProviderEnabled(LocationManager.GPS_PROVIDER));
        System.out.println(status.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        if (status.isProviderEnabled(LocationManager.GPS_PROVIDER) || status.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationGet = true;
        } else {
            locationGet = false;
            System.out.println(locationGet);
            getMap().getUiSettings().setMyLocationButtonEnabled(locationGet);
//            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }


    private void setFloatingBtn() {
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CusHouseDialog cusHouseDialog = new CusHouseDialog();
                if (locationGet || isLocation) {
                    try {
                        Double lat = getMap().getMyLocation().getLatitude();
                        Double lng = getMap().getMyLocation().getLongitude();
                        if ((!lat.isNaN() || !lng.isNaN()) && getMap().getProjection().getVisibleRegion().latLngBounds.contains(new LatLng(lat, lng))) {
                            cusHouseDialog.setLat(getMap().getMyLocation().getLatitude());
                            cusHouseDialog.setLng(getMap().getMyLocation().getLongitude());
                            System.out.println("定");
                            cusHouseDialog.show(getFragmentManager(), "CusHouseDialog");
                        } else if (latLng != null && getMap().getProjection().getVisibleRegion().latLngBounds.contains(latLng)) {
                            cusHouseDialog.setLat(latLng.latitude);
                            cusHouseDialog.setLng(latLng.longitude);
                            System.out.println(addressN);
                            System.out.println("搜");
                            cusHouseDialog.show(getFragmentManager(), "CusHouseDialog");
                        } else {
                            Toast.makeText(getApplication(), "未定位或查詢", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        if (latLng != null && getMap().getProjection().getVisibleRegion().latLngBounds.contains(latLng)) {
                            cusHouseDialog.setLat(latLng.latitude);
                            cusHouseDialog.setLng(latLng.longitude);
                            System.out.println(addressN);
                            System.out.println("搜");
                            cusHouseDialog.show(getFragmentManager(), "CusHouseDialog");
                        } else {
                            Toast.makeText(getApplication(), "未定位或查詢", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getApplication(), "未定位或查詢", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void pointR() {
        real.setChecked(true);
        real.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && sale.isChecked()) {
                    clusterManager.clearItems();
                    clusterManager.addItems(realItems);
                    clusterManager.addItems(saleItems);
                    clusterLoad(1000000);
                } else if (b && !sale.isChecked()) {
                    clusterManager.clearItems();
                    clusterManager.addItems(realItems);
                    clusterLoad(1000000);
                } else if (!b && sale.isChecked()) {
                    clusterManager.clearItems();
                    clusterManager.addItems(saleItems);
                    clusterLoad(1000000);
                }
            }
        });

        sale.setChecked(true);
        sale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b && real.isChecked()) {
                    clusterManager.clearItems();
                    clusterManager.addItems(realItems);
                    clusterManager.addItems(saleItems);
                    clusterLoad(1000000);
                } else if (b && !real.isChecked()) {
                    clusterManager.clearItems();
                    clusterManager.addItems(saleItems);
                    clusterLoad(1000000);
                } else if (!b && real.isChecked()) {
                    clusterManager.clearItems();
                    clusterManager.addItems(realItems);
                    clusterLoad(1000000);
                }
            }
        });
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    android.Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (getMap() != null) {
            // Access to the location has been granted to the app.
            locationGet = true;
            System.out.println(locationGet);
            getMap().setMyLocationEnabled(locationGet);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
            showMissingPermissionError();
        }
    }


    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCacheIcon.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCacheIcon.get(key);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                return true;
            case R.id.favorite:
                startActivity(new Intent(getApplicationContext(), MyLoveActivity.class));
                return true;
            case R.id.mod:
                startActivity(new Intent(getApplicationContext(), ModActivity.class));
                return true;
            case R.id.contact:
                startActivity(new Intent(getApplicationContext(), Contactus.class));
                return true;
            default:
                return true;
        }
    }


    //cluster載入
    private void clusterLoad(int range) {
        //getMap().clear();
        clusterManager.setRenderer(new markerRender(range));
        getMap().setOnCameraChangeListener(clusterManager);
        getMap().setOnMarkerClickListener(clusterManager);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterInfoWindowClickListener(this);
        clusterManager.cluster();
    }


    @Override
    public boolean onClusterClick(Cluster<MarkerItem> cluster) {
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MarkerItem> cluster) {
    }

    //marker觸發
    @Override
    public boolean onClusterItemClick(MarkerItem markerItem) {
        getMap().setOnCameraMoveListener(null);
        if (sheet.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            sheet.setState(BottomSheetBehavior.STATE_EXPANDED);
            if (markerItem.type == 1) {//實價登錄＝1
                loadData data = new loadData();
                data.execute(new int[]{markerItem.houseId, m_id, 1});
            } else {
                loadData data = new loadData();
                data.execute(new int[]{markerItem.houseId, m_id, 0});
            }
        } else {
            if (markerItem.type == 1) {
                loadData data = new loadData();
                data.execute(new int[]{markerItem.houseId, m_id, 1});
            } else {
                loadData data = new loadData();
                data.execute(new int[]{markerItem.houseId, m_id, 0});
            }
        }
        getMap().setOnCameraMoveListener(this);
        return false;
    }


    @Override
    public void onClusterItemInfoWindowClick(MarkerItem markerItem) {

    }

    @Override
    protected void onDestroy() {
        psDialog.dismiss();
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        System.out.println(advList.toString());
        DownloadJsonFile downloadGeoJsonFile = new DownloadJsonFile();
        downloadGeoJsonFile.execute(advList);
    }


    //FloatingSearchView setup
    private void setupFloatingSearch() {
        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_search:
                        onMapSearch(mSearchView.getQuery());
                        break;
                    case R.id.action_mode:
                        startActivity(new Intent(MapsActivity.this, CustomMapActivity.class));
                        break;
                    case R.id.action_layer:
                        mDrawerLayout.openDrawer(navigationViewR);
                        break;
                    case R.id.advancedPanel:
                        if (panel.getVisibility() != View.VISIBLE) {
                            panel.setVisibility(View.VISIBLE);
                            panel.startAnimation(slide_to_down);
                        } else {
                            panel.startAnimation(slide_to_up);
                            panel.setVisibility(View.INVISIBLE);
                        }


                        break;
                }
            }
        });
        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                List<Address> addressList = null;

                if (currentQuery != null || !currentQuery.equals("")) {
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        addressList = geocoder.getFromLocationName(currentQuery, 3);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (addressList.isEmpty() == false) {
                        Address address = addressList.get(0);
                        addressN = address.toString();
                        System.out.println(addressN);
                        latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        getMap().addMarker(new MarkerOptions().position(latLng).title(currentQuery).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        getMap().animateCamera(CameraUpdateFactory.newLatLng(latLng));
                        isLocation = true;
                    }
                }
            }
        });
    }

    //地址搜尋
    public void onMapSearch(String location) {
        List<Address> addressList = null;

        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 3);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addressList.isEmpty() == false) {
                Address address = addressList.get(0);
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        }
    }

    @Override
    public void onCameraMove() {
        if (sheet.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    //cluster marker 設定
    class markerRender extends DefaultClusterRenderer<MarkerItem> {

        public int range;

        public markerRender(int range) {
            super(getApplicationContext(), getMap(), clusterManager);
            this.range = range;
        }

        @Override
        protected void onBeforeClusterItemRendered(MarkerItem markerItem, MarkerOptions markerOptions) {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerItem.bitmap));
        }

    }

    //非同步讀入json
    private class LoadJsonTask extends AsyncTask<Integer, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Integer... params) {
            httpGet = new GetURL();
            hweight = httpGet.getHweight(m_id);
//            InputStream inputStream = params[0];
//            iconGenerator = new IconGenerator(getApplicationContext());
//            try {
//                String json = new Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next();
//                JSONArray array = new JSONArray(json);
//                for (int i = 0; i < array.length(); i++) {
//                    JSONObject object = array.getJSONObject(i);
//                    double lat = object.getDouble("lat");
//                    double lng = object.getDouble("lon");
//                    LatLng latLng = new LatLng(lat, lng);
//                    int tPrice = object.getInt("price");
//                    int houseId = object.getInt("ID");
//                    addBitmapToMemoryCache(String.valueOf(tPrice), iconGenerator.makeIcon(String.valueOf(tPrice)));
//                    if (tPrice < hweight.getPricemax() || tPrice > hweight.getPricemin()) {
//                        realItems.add(new MarkerItem(latLng.latitude, latLng.longitude, true, tPrice, getBitmapFromMemCache(String.valueOf(tPrice)), houseId));
//                    } else {
//                        realItems.add(new MarkerItem(latLng.latitude, latLng.longitude, false, tPrice, getBitmapFromMemCache(String.valueOf(tPrice)), houseId));
//                    }
//                }
//                clusterManager.addItems(realItems);
//                long endTime = System.currentTimeMillis();
//                long totTime = endTime - startTime;
//                System.out.println("Using Time:" + totTime);
//                return true;
//            } catch (JSONException e) {
//                e.printStackTrace();
//                return false;
//            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (hweight.getSection().equals("不限")) {
                advList.add("%25");//區
            } else {
                advList.add(hweight.getSection());//區
            }
            advList.add("*");//屋型
            //價錢
            advList.add(String.valueOf(hweight.getPricemin()));
            advList.add(String.valueOf(hweight.getPricemax()));
            //坪數
            advList.add("0");
            advList.add("1000");
            //屋齡
            advList.add("0");
            advList.add("1000");
            DownloadJsonFile downloadGeoJsonFile = new DownloadJsonFile();
            downloadGeoJsonFile.execute(advList);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

    }
    //http://blackwidowfju.azurewebsites.net/api/Values/Search?area=%&htype=*&pmin=0&pmax=1000&hamin=50&hamax=1000&hymin=0&hymax=1000

    private class DownloadJsonFile extends AsyncTask<ArrayList<String>, Void, ArrayList<JSONArray>> {

        @Override
        protected ArrayList<JSONArray> doInBackground(ArrayList<String>... params) {
            try {
                ArrayList<JSONArray> jsonArrays = new ArrayList<>();
                // Open a stream from the URL
                String realUrl = "http://blackwidowfju.azurewebsites.net/api/Values/Search?area=" + params[0].get(0) +
                        "&htype=" + params[0].get(1) +
                        "&pmin=" + params[0].get(2) + "&pmax=" + params[0].get(3) +
                        "&hamin=" + params[0].get(4) + "&hamax=" + params[0].get(5) +
                        "&hymin=" + params[0].get(6) + "&hymax=" + params[0].get(7);
                URLEncoder.encode(realUrl, "UTF-8");
                System.out.println(realUrl);
                InputStream stream = new URL(realUrl).openStream();

                String line;
                StringBuilder result = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

                while ((line = reader.readLine()) != null) {
                    // Read and save each line of the stream
                    result.append(line);
                }

                // Close the stream
                reader.close();
                stream.close();
                JSONArray real = new JSONArray(result.toString());
                jsonArrays.add(real);

                /////////////////////////////////////////////////

                // Open a stream from the URL
                String sellUrl = "http://blackwidowfju.azurewebsites.net/api/Values/SellHouseSearch?section=" + params[0].get(0) +
                        "&housetype=" + params[0].get(1) +
                        "&pricemin=" + params[0].get(2) + "&pricemax=" + params[0].get(3) +
                        "&hareamin=" + params[0].get(4) + "&hareamax=" + params[0].get(5) +
                        "&hyearmin=" + params[0].get(6) + "&hyearmax=" + params[0].get(7);
                URLEncoder.encode(sellUrl, "UTF-8");
                System.out.println(sellUrl);
                InputStream streamSell = new URL(sellUrl).openStream();

                String lineSell;
                StringBuilder resultSell = new StringBuilder();
                BufferedReader readerSell = new BufferedReader(new InputStreamReader(streamSell));

                while ((lineSell = readerSell.readLine()) != null) {
                    // Read and save each line of the stream
                    resultSell.append(lineSell);
                }

                // Close the stream
                readerSell.close();
                streamSell.close();
                JSONArray sell = new JSONArray(resultSell.toString());
                jsonArrays.add(sell);
                ////////////////////////////////////////////////////
                // Open a stream from the URL
                if (recommend.isEmpty()) {
                    String reUrl = "http://blackwidowfju.azurewebsites.net/api/Values/GetWantHouse?meid=" + m_id;
                    URLEncoder.encode(reUrl, "UTF-8");
                    System.out.println(reUrl);
                    InputStream streamRe = new URL(reUrl).openStream();

                    String lineRe;
                    StringBuilder resultRe = new StringBuilder();
                    BufferedReader readerRe = new BufferedReader(new InputStreamReader(streamRe));

                    while ((lineRe = readerRe.readLine()) != null) {
                        // Read and save each line of the stream
                        resultRe.append(lineRe);
                    }

                    // Close the stream
                    readerRe.close();
                    streamRe.close();
                    JSONArray recomm = new JSONArray(resultRe.toString());
                    jsonArrays.add(recomm);
                }


                return jsonArrays;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                Log.e(mLogTag, "GeoJSON file could not be converted to a JSONObject");
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<JSONArray> jsonArray) {
            recommendBox.setChecked(false);
            realItems.clear();
            saleItems.clear();
            clusterManager.clearItems();
            IconGenerator iconGenerator = new IconGenerator(getApplicationContext());
            IconGenerator iconGenerator2 = new IconGenerator(getApplicationContext());
            IconGenerator iconGenerator3 = new IconGenerator(getApplicationContext());
            if (jsonArray != null) {
                try {
                    for (int i = 0; i < jsonArray.get(0).length(); i++) {
                        JSONObject object;
                        object = jsonArray.get(0).getJSONObject(i);
                        double lat = object.getDouble("lat");
                        double lng = object.getDouble("lon");
                        LatLng latLng = new LatLng(lat, lng);
                        int tPrice = object.getInt("price");
                        int houseId = object.getInt("ID");
                        iconGenerator.setStyle(IconGenerator.STYLE_RED);
                        addBitmapToMemoryCache(String.valueOf(tPrice), iconGenerator.makeIcon(String.valueOf(tPrice)));
                        realItems.add(new MarkerItem(latLng.latitude, latLng.longitude, true, tPrice, getBitmapFromMemCache(String.valueOf(tPrice)), houseId, 1));
                    }
                    clusterManager.addItems(realItems);
                    for (int i = 0; i < jsonArray.get(1).length(); i++) {
                        JSONObject object1;
                        object1 = jsonArray.get(1).getJSONObject(i);
                        double lat = object1.getDouble("lat");
                        double lng = object1.getDouble("lon");
                        LatLng latLng = new LatLng(lat, lng);
                        int tPrice = object1.getInt("price");
                        int houseId = object1.getInt("ID");
                        iconGenerator2.setStyle(IconGenerator.STYLE_BLUE);
                        addBitmapToMemoryCache(latLng.toString(), iconGenerator2.makeIcon(String.valueOf(tPrice)));
                        saleItems.add(new MarkerItem(latLng.latitude, latLng.longitude, true, tPrice, getBitmapFromMemCache(latLng.toString()), houseId, 0));
                    }
                    clusterManager.addItems(saleItems);
                    if (jsonArray.size() == 3) {
                        if (jsonArray.get(2).length() != 0) {
                            for (int i = 0; i < jsonArray.get(2).length(); i++) {
                                JSONObject object;
                                object = jsonArray.get(2).getJSONObject(i);
                                double lat = object.getDouble("lat");
                                double lng = object.getDouble("lon");
                                LatLng latLng = new LatLng(lat, lng);
                                int tPrice = object.getInt("price");
                                int houseId = object.getInt("ID");
                                iconGenerator3.setStyle(IconGenerator.STYLE_GREEN);
                                addBitmapToMemoryCache(String.valueOf(houseId + lat), iconGenerator3.makeIcon(String.valueOf(tPrice)));
                                recommend.add(new MarkerItem(latLng.latitude, latLng.longitude, true, tPrice, getBitmapFromMemCache(String.valueOf(houseId + lat)), houseId, 2));
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            recommendBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {

                        sale.setChecked(false);
                        real.setChecked(false);
                        clusterManager.clearItems();
                        System.out.println(recommend.isEmpty());
                        clusterManager.addItems(recommend);
                        clusterLoad(100000);
                    } else {
                        pointR();
                    }
                }
            });
            clusterLoad(1000000);
            pointR();
            psDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            psDialog = ProgressDialog.show(MapsActivity.this, "提示", "資料載入中，請稍候...");
        }


    }


    private class loadData extends AsyncTask<int[], Integer, Boolean> {

        private Hweight hweight;
        private RealEstateDate real;
        private SaleHouse sale;
        private TextView locationV;
        private TextView priceV;
        private TextView nearV;
        private TextView spaceV;

        @Override
        protected Boolean doInBackground(int[]... integers) {
            int[] x = integers[0];
            if (x[2] == 1) {
                real = httpGet.real_detail(x[0], x[1]);
            } else {
                sale = httpGet.sale_detail(x[0]);
            }
            hweight = httpGet.getHweight(x[1]);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            sheet.setState(BottomSheetBehavior.STATE_EXPANDED);
            locationV = (TextView) findViewById(R.id.location);
            priceV = (TextView) findViewById(R.id.price);
            nearV = (TextView) findViewById(R.id.near);
            spaceV = (TextView) findViewById(R.id.space);
            System.out.println(real == null);
            System.out.println(sale == null);
            if (real != null) { //實價登錄
                locationV.setText(real.getRealAddr());
                double p = Double.parseDouble(real.getPrice());
                double pp = p / 10000;
                DecimalFormat df = new DecimalFormat("#.#");
                priceV.setText(df.format(pp));
                spaceV.setText(real.getHouseArea());

                ArrayList<RealEstateDateLs> ls = real.getLs();
                List<String> name = new ArrayList<>();
                for (RealEstateDateLs dateLs : ls) {
                    name.add(dateLs.getP_Name());
                }

                nearV.setText(name.toString());
                anal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Bundle realData = new Bundle();
                        realData.putSerializable("fromHou", real);
                        realData.putSerializable("hweight", hweight);
                        realData.putInt("addType", 0);
                        intent.putExtras(realData);
                        startActivity(intent);
                    }
                });

            } else { //交易中房屋
                locationV.setText(sale.getSellHouse_Address());
                priceV.setText(sale.getSellHouse_Price());
                ArrayList<SaleHouseLs> ls = sale.getLs();
                List<String> name = new ArrayList<>();
                for (SaleHouseLs dateLs : ls) {
                    name.add(dateLs.getP_Name());
                }
                spaceV.setText(sale.getSellHouse_Space());
                nearV.setText(name.toString());
                realHouse = new RealHouse();
                realHouse.setAddress(sale.getSellHouse_Address());
                realHouse.setPrice(Double.parseDouble(sale.getSellHouse_Price()) * 10000);
                realHouse.setSpace(Double.parseDouble(sale.getSellHouse_Space()));
                realHouse.setYears(Integer.parseInt(sale.getSellHouse_Year()));
                realHouse.setCar(Boolean.parseBoolean(sale.getSellHouse_Car()) ? 1 : 0);
                realHouse.setElec(Boolean.parseBoolean(sale.getSellHouse_Elevator()) ? 1 : 0);
                realHouse.setType(Integer.parseInt(sale.getSellHouse_Type()));
                realHouse.setRoom(Integer.parseInt(sale.getSellHouse_Room()));
                realHouse.setL_room(Integer.parseInt(sale.getSellHouse_LRoom()));
                realHouse.setBath(Integer.parseInt(sale.getSellHouse_WC()));
                anal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(MapsActivity.this)
                                .setTitle("分析方法")
                                .setMessage("選擇分析分法")
                                .setPositiveButton("快速分析", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        analysistype = "a";
                                        LoadAndCompute loadAndCompute = new LoadAndCompute();
                                        loadAndCompute.execute(m_id);
                                    }
                                })
                                .setNegativeButton("完整分析", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        analysistype = "f";
                                        LoadAndCompute loadAndCompute = new LoadAndCompute();
                                        loadAndCompute.execute(m_id);
                                    }
                                })
                                .create()
                                .show();
                    }
                });
            }
            pv_circular.stop();
            pv_circular.setVisibility(View.GONE);
            info.setVisibility(View.VISIBLE);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            info.setVisibility(View.INVISIBLE);
            pv_circular.start();
        }
    }


    @Override
    public void onBackPressed() {
        if (sheet.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            sheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }

    }

    class LoadAndCompute extends AsyncTask<Integer, Integer, Boolean> {

        ProgressDialog progressDialog;


        @Override
        protected Boolean doInBackground(Integer... integers) {
            httpGet = new GetURL();
            myData = httpGet.getScoreAndData(analysistype, realHouse.getAddress(), integers[0], realHouse);
            System.out.println(myData.getRealAddr());
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MapsActivity.this, "", "");
        }

        @Override
        protected void onPostExecute(Boolean finish) {
            super.onPreExecute();
            progressDialog.dismiss();
            Bundle myDATA = new Bundle();
            myDATA.putSerializable("fromCus", myData);
            myDATA.putSerializable("fromCusReal", realHouse);
            myDATA.putSerializable("hweight", hweight);
            intent.putExtras(myDATA);
            startActivity(intent);
        }

    }
}


//                FeatureCollection featureCollection = new FeatureCollection(GeoJSON.parse(inputStream).toJSON());
//                for (int i = 0; i < featureCollection.getFeatures().size(); i++) {
//                    Feature feature = featureCollection.getFeatures().get(i);
//                    if (feature.getProperties().getString("縣市").equals("臺北市") || feature.getProperties().getString("縣市").equals("新北市")) {
//                        Geometry geometry = feature.getGeometry();
//                        JSONArray jsonArray = geometry.toJSON().optJSONArray("coordinates");
//                        LatLng latLng = new LatLng(Double.parseDouble(String.valueOf(jsonArray.get(1))), Double.parseDouble(String.valueOf(jsonArray.get(0))));
//                        String pricestr = feature.getProperties().getString("單價每平方公尺");
//                        String date = feature.getProperties().getString("土地區段位置或建物區門牌");
//                        Double space = feature.getProperties().getDouble("建物移轉總面積平方公尺");
//                        MarkerOptions markerOptions;
//                        if (!feature.getProperties().getString("單價每平方公尺").equals("null")) {
//                            int price = (int) (Integer.parseInt(feature.getProperties().getString("單價每平方公尺")) * 3.31);
//                            if (price < 400000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_WHITE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 400000 && price < 500000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_BLUE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 500000 && price < 600000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 600000 && price < 700000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_GREEN);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else {
//                                iconGenerator.setStyle(IconGenerator.STYLE_RED);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            }
//                            realItems.add(new MarkerItem(latLng.latitude, latLng.longitude, markerOptions.getTitle(), price, markerOptions.getIcon(), date, space, false));
//                        } else {
//                            int price = (int) ((int) ((Integer.parseInt(feature.getProperties().getString("總價元"))) / (Double.parseDouble(feature.getProperties().getString("建物移轉總面積平方公尺")))) * 3.31);
//                            if (price < 400000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_WHITE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 400000 && price < 500000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_BLUE);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 500000 && price < 600000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_DEFAULT);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else if (price >= 600000 && price < 700000) {
//                                iconGenerator.setStyle(IconGenerator.STYLE_GREEN);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            } else {
//                                iconGenerator.setStyle(IconGenerator.STYLE_RED);
//                                addBitmapToMemoryCache(latLng.toString(), iconGenerator.makeIcon(String.valueOf(price / 10000)));
//                                markerOptions = new MarkerOptions().position(latLng).title("單價每平方公尺").snippet(pricestr).icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCache(latLng.toString())));
//                            }
//                            realItems.add(new MarkerItem(latLng.latitude, latLng.longitude, markerOptions.getTitle(), price, markerOptions.getIcon(), date, space, false));
//                        }
//                    }
//                }
//                for (MarkerItem markerItem : realItems) {
//                    clusterManager.addItem(markerItem);
//                }

