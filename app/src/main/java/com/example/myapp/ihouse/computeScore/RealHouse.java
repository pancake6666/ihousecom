/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.myapp.ihouse.computeScore;

import java.io.Serializable;

/**
 * @author changyuen
 */
public class RealHouse implements Serializable {
    private String address;
    private double price;
    private double space;
    private int type;
    private int room;
    private int l_room;
    private int bath;
    private int car;
    private int elec;
    private int years;

    public RealHouse() {
    }

    public RealHouse(String address, double price, double space, int type, int room, int l_room, int bath, int car, int elec, int years) {
        this.address = address;
        this.price = price;
        this.space = space;
        this.type = type;
        this.room = room;
        this.l_room = l_room;
        this.bath = bath;
        this.car = car;
        this.elec = elec;
        this.years = years;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSpace() {
        return space;
    }

    public void setSpace(double space) {
        this.space = space;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public int getL_room() {
        return l_room;
    }

    public void setL_room(int l_room) {
        this.l_room = l_room;
    }

    public int getBath() {
        return bath;
    }

    public void setBath(int bath) {
        this.bath = bath;
    }

    public int getCar() {
        return car;
    }

    public void setCar(int car) {
        this.car = car;
    }

    public int getElec() {
        return elec;
    }

    public void setElec(int elec) {
        this.elec = elec;
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

}
