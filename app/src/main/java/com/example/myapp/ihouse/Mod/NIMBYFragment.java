package com.example.myapp.ihouse.mod;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RatingBar;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.util.ArrayList;

/**
 * Created by Alex on 2016/8/9.
 */
public class NIMBYFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    NumberPicker np1 = null;
    NumberPicker np2 = null;
    protected PageFragmentCallbacks mCallbacks;
    private int mKey;
    Hweight hw = ModActivity.hweight;

    public static NIMBYFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        NIMBYFragment fragment = new NIMBYFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getInt(ARG_PAGE);
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nimbyfragment1, container, false);
        integers = new ArrayList<>();
        integers.add(0, -1);
        integers.add(1, -1);
        integers.add(2, -1);
        integers.add(3, -1);
        integers.add(4, -1);
        integers.add(5, -1);


        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.RatingBarId);
        final RatingBar ratingBar2 = (RatingBar) view.findViewById(R.id.RatingBarId2);
        final RatingBar ratingBar3 = (RatingBar) view.findViewById(R.id.RatingBarId3);
        final RatingBar ratingBar4 = (RatingBar) view.findViewById(R.id.RatingBarId4);


        ratingBar.setRating(hw.getHTGS());
        ratingBar2.setRating(hw.getHTIS());
        ratingBar3.setRating(hw.getHTMS());
        ratingBar4.setRating(hw.getHTAS());

        np1 = (NumberPicker) view.findViewById(R.id.np1);
        np2 = (NumberPicker) view.findViewById(R.id.np2);

        final String[] values = new String[]{"0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800", "1900", "2000"};

        np1.setMaxValue(values.length - 1);
        np1.setDisplayedValues(values);
        np1.setMinValue(0);
        np1.setValue(FindIndex(values,hw.getHTmin()));

        np2.setMaxValue(values.length - 1);
        np2.setMinValue(0);
        np2.setDisplayedValues(values);
        np2.setValue(FindIndex(values,hw.getHTmax()));

        np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
              hw.setHTmin(Integer.valueOf(values[newVal]));
            }
        });

        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hw.setHTmax(Integer.valueOf(values[newVal]));
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
               hw.setHTGS((int)rating);
            }
        });

        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setHTIS((int)rating);
            }
        });

        ratingBar3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setHTMS((int)rating);
            }
        });

        ratingBar4.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                hw.setHTAS((int)rating);
            }
        });


        Button clear = (Button) view.findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar.setRating(0);
                hw.setHTGS(0);

            }
        });

        Button clear2 = (Button) view.findViewById(R.id.clear2);
        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar2.setRating(0);
                hw.setHTIS(0);

            }
        });

        Button clear3 = (Button) view.findViewById(R.id.clear3);
        clear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar3.setRating(0);
                hw.setHTMS(0);

            }
        });

        Button clear4 = (Button) view.findViewById(R.id.clear4);
        clear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar4.setRating(0);
                hw.setHTAS(0);

            }
        });


        return view;


    }

    public int FindIndex(String[] values, Integer num) {
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(num.toString())) {
                index = i;
            }

        }
        return index;
    }

}