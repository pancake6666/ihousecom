package com.example.myapp.ihouse.register;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RatingBar;

import com.example.myapp.ihouse.R;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.util.ArrayList;

/**
 * Created by Alex on 2016/8/9.
 */
public class TrafficFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    NumberPicker np1 = null;
    NumberPicker np2 = null;
    NumberPicker np3 = null;
    NumberPicker np4 = null;
    NumberPicker np5 = null;
    NumberPicker np6 = null;
    NumberPicker np7 = null;
    NumberPicker np8 = null;
    private StepTrafficPage mPage;
    protected PageFragmentCallbacks mCallbacks;
    private String mKey;


    public static TrafficFragment newInstance(String pageNo) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, pageNo);
        TrafficFragment fragment = new TrafficFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_PAGE);
        mPage = (StepTrafficPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trafficfragment1, container, false);


        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.RatingBarId);
        final RatingBar ratingBar2 = (RatingBar) view.findViewById(R.id.RatingBarId2);
        final RatingBar ratingBar3 = (RatingBar) view.findViewById(R.id.RatingBarId3);
        final RatingBar ratingBar4 = (RatingBar) view.findViewById(R.id.RatingBarId4);

        ratingBar.setRating(mPage.getData().getInt(StepTrafficPage.MRT_DATA_KEY));
        ratingBar2.setRating(mPage.getData().getInt(StepTrafficPage.TRAIN_DATA_KEY));
        ratingBar3.setRating(mPage.getData().getInt(StepTrafficPage.THSR_DATA_KEY));
        ratingBar4.setRating(mPage.getData().getInt(StepTrafficPage.HIGH_DATA_KEY));


        np1 = (NumberPicker) view.findViewById(R.id.np1);
        np2 = (NumberPicker) view.findViewById(R.id.np2);
        np3 = (NumberPicker) view.findViewById(R.id.np3);
        np4 = (NumberPicker) view.findViewById(R.id.np4);
        np5 = (NumberPicker) view.findViewById(R.id.np5);
        np6 = (NumberPicker) view.findViewById(R.id.np6);
        np7 = (NumberPicker) view.findViewById(R.id.np7);
        np8 = (NumberPicker) view.findViewById(R.id.np8);

        np1.setSaveFromParentEnabled(false);
        np1.setSaveEnabled(false);
        np2.setSaveFromParentEnabled(false);
        np2.setSaveEnabled(false);
        np3.setSaveFromParentEnabled(false);
        np3.setSaveEnabled(false);
        np4.setSaveFromParentEnabled(false);
        np4.setSaveEnabled(false);
        np5.setSaveFromParentEnabled(false);
        np5.setSaveEnabled(false);
        np6.setSaveFromParentEnabled(false);
        np6.setSaveEnabled(false);
        np7.setSaveFromParentEnabled(false);
        np7.setSaveEnabled(false);
        np8.setSaveFromParentEnabled(false);
        np8.setSaveEnabled(false);

        final String[] values = new String[]{"0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800", "1900", "2000"};

        np1.setMaxValue(values.length - 1);
        np1.setDisplayedValues(values);
        np1.setMinValue(0);
        np1.setValue(mPage.getData().getInt(StepTrafficPage.MRT_MIN_VAL));

        np2.setMaxValue(values.length - 1);
        np2.setMinValue(0);
        np2.setDisplayedValues(values);
        np2.setValue(mPage.getData().getInt(StepTrafficPage.MRT_MAX_VAL));

        np3.setMaxValue(values.length - 1);
        np3.setDisplayedValues(values);
        np3.setMinValue(0);
        np3.setValue(mPage.getData().getInt(StepTrafficPage.TRAIN_MIN_VAL));

        np4.setMaxValue(values.length - 1);
        np4.setMinValue(0);
        np4.setDisplayedValues(values);
        np4.setValue(mPage.getData().getInt(StepTrafficPage.TRAIN_MAX_VAL));

        np5.setMaxValue(values.length - 1);
        np5.setDisplayedValues(values);
        np5.setMinValue(0);
        np5.setValue(mPage.getData().getInt(StepTrafficPage.THSR_MIN_VAL));

        np6.setMaxValue(values.length - 1);
        np6.setMinValue(0);
        np6.setDisplayedValues(values);
        np6.setValue(mPage.getData().getInt(StepTrafficPage.THSR_MAX_VAL));

        np7.setMaxValue(values.length - 1);
        np7.setDisplayedValues(values);
        np7.setMinValue(0);
        np7.setValue(mPage.getData().getInt(StepTrafficPage.HIGH_MIN_VAL));

        np8.setMaxValue(values.length - 1);
        np8.setMinValue(0);
        np8.setDisplayedValues(values);
        np8.setValue(mPage.getData().getInt(StepTrafficPage.HIGH_MAX_VAL));


        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepTrafficPage.MRT_DATA_KEY, (int) rating);
                mPage.notifyDataChanged();
            }
        });

        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepTrafficPage.TRAIN_DATA_KEY, (int) rating);
                mPage.notifyDataChanged();
            }
        });

        ratingBar3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepTrafficPage.THSR_DATA_KEY, (int) rating);
                mPage.notifyDataChanged();
            }
        });

        ratingBar4.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepTrafficPage.HIGH_DATA_KEY, (int) rating);
                mPage.notifyDataChanged();
            }
        });


        np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.MRT_MIN_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.MRT_MIN, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });

        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.MRT_MAX_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.MRT_MAX, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });

        np3.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.TRAIN_MIN_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.TRAIN_MIN, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });

        np4.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.TRAIN_MAX_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.TRAIN_MAX, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });

        np5.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.THSR_MIN_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.THSR_MIN, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });

        np6.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.THSR_MAX_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.THSR_MAX, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });

        np7.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.HIGH_MIN_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.HIGH_MIN, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });

        np8.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepTrafficPage.HIGH_MAX_VAL, newVal);
                mPage.getData().putInt(StepTrafficPage.HIGH_MAX, Integer.parseInt(values[newVal]));
                mPage.notifyDataChanged();
            }
        });


        Button clear = (Button) view.findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar.setRating(0);
                mPage.getData().putInt(StepTrafficPage.MRT_DATA_KEY, 0);
                np1.setValue(0);
                mPage.getData().putInt(StepTrafficPage.MRT_MIN_VAL, np1.getValue());
                mPage.getData().putInt(StepTrafficPage.MRT_MIN, 0);
                np2.setValue(0);
                mPage.getData().putInt(StepTrafficPage.MRT_MAX_VAL, np2.getValue());
                mPage.getData().putInt(StepTrafficPage.MRT_MAX, 0);
                mPage.notifyDataChanged();
            }
        });


        Button clear2 = (Button) view.findViewById(R.id.clear2);
        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar2.setRating(0);
                mPage.getData().putInt(StepTrafficPage.TRAIN_DATA_KEY, 0);
                np3.setValue(0);
                mPage.getData().putInt(StepTrafficPage.TRAIN_MIN_VAL, np3.getValue());
                mPage.getData().putInt(StepTrafficPage.TRAIN_MIN, 0);
                np4.setValue(0);
                mPage.getData().putInt(StepTrafficPage.TRAIN_MAX_VAL, np4.getValue());
                mPage.getData().putInt(StepTrafficPage.TRAIN_MAX, 0);
                mPage.notifyDataChanged();
            }
        });


        Button clear3 = (Button) view.findViewById(R.id.clear3);
        clear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar3.setRating(0);
                mPage.getData().putInt(StepTrafficPage.THSR_DATA_KEY, 0);
                np5.setValue(0);
                mPage.getData().putInt(StepTrafficPage.THSR_MIN_VAL, np5.getValue());
                mPage.getData().putInt(StepTrafficPage.THSR_MIN, 0);
                np6.setValue(0);
                mPage.getData().putInt(StepTrafficPage.THSR_MAX_VAL, np6.getValue());
                mPage.getData().putInt(StepTrafficPage.THSR_MAX, 0);
                mPage.notifyDataChanged();
            }
        });


        Button clear4 = (Button) view.findViewById(R.id.clear4);
        clear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar4.setRating(0);
                mPage.getData().putInt(StepTrafficPage.HIGH_DATA_KEY, 0);
                np7.setValue(0);
                mPage.getData().putInt(StepTrafficPage.HIGH_MIN_VAL, np7.getValue());
                mPage.getData().putInt(StepTrafficPage.HIGH_MIN, 0);
                np8.setValue(0);
                mPage.getData().putInt(StepTrafficPage.HIGH_MAX_VAL, np8.getValue());
                mPage.getData().putInt(StepTrafficPage.HIGH_MAX, 0);
                mPage.notifyDataChanged();
            }
        });

        return view;


    }


}
