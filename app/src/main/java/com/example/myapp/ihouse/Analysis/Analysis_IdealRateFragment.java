package com.example.myapp.ihouse.analysis;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.MyData;
import com.example.myapp.ihouse.httpGet.MyLoveSample;
import com.example.myapp.ihouse.httpGet.Part2;
import com.example.myapp.ihouse.httpGet.RealEstateDate;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Analysis_IdealRateFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    private FloatingActionButton fravorateic, mapic, questic;
    private Button trafficic, lifeic, badic, distroyic, conditionic;
    private FloatingActionButton toastButton2;
    private RadarChart mChart;
    private ExpandableListView expandableListView;
    public static RealEstateDate realEstateDate;
    public static MyData myData;
    public static Part2 part2;
    public static MyLoveSample myLoveSample;
    public static GetURL httpGet;
    public static Hweight hweight;
    public static RealHouse realHouse;
    public static int m_id;

    public static Analysis_IdealRateFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        Analysis_IdealRateFragment fragment = new Analysis_IdealRateFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.analysis_idealrate, container, false);
        TextView address = (TextView) view.findViewById(R.id.address);
        TextView totalScore = (TextView) view.findViewById(R.id.lifetotalpoints);
        toastButton2 = (FloatingActionButton) view.findViewById(R.id.fab1);
        httpGet = new GetURL();
        if (Activity_Analysis.realEstateDate != null) {
            if (realEstateDate.getAddLoveCheck().equals("1")) {
                toastButton2.setEnabled(false);
            }
            address.setText(Activity_Analysis.realEstateDate.getRealAddr());
            if (realEstateDate.getRealAddr().length() > 11) {
                address.setTextSize(30f);
            }
            double total = Double.parseDouble(realEstateDate.getMl_Score());
            DecimalFormat df = new DecimalFormat("#.#");
            totalScore.setText(df.format(total));
        } else if (Activity_Analysis.myData != null) {
            if (myData.getAddLoveCheck().equals("1")) {
                toastButton2.setEnabled(false);
            }
            address.setText(Activity_Analysis.myData.getRealAddr());
            if (myData.getRealAddr().length() > 11) {
                address.setTextSize(30f);
            }
            double total = Double.parseDouble(myData.getMl_Score());
            DecimalFormat df = new DecimalFormat("#.#");
            totalScore.setText(df.format(total));
        } else if (Activity_Analysis.myLoveSample != null) {
            toastButton2.setEnabled(false);
            address.setText(myLoveSample.getMl_address());
            if (myLoveSample.getMl_address().length() > 11) {
                address.setTextSize(30f);
            }
            double total = Double.parseDouble(myLoveSample.getMl_Score());
            DecimalFormat df = new DecimalFormat("#.#");
            totalScore.setText(df.format(total));
        }
        expandableListView = (ExpandableListView) view.findViewById(R.id.idealscore);
        setGroup();

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        mChart = (RadarChart) view.findViewById(R.id.chart1);

        //tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        mChart.setDescription("");
        mChart.setWebLineWidth(1.5f);
        mChart.setWebLineWidthInner(0.75f);
        mChart.setWebAlpha(100);

        setData();
//        mChart.animateXY(
//                1500, 1500,
//                Easing.EasingOption.EaseInOutQuad,
//                Easing.EasingOption.EaseInOutQuad);

        XAxis xAxis = mChart.getXAxis();
        //xAxis.setTypeface(tf);
        xAxis.setTextSize(18);

        YAxis yAxis = mChart.getYAxis();
        yAxis.setLabelCount(4, false);
        yAxis.setTextSize(10f);
        yAxis.setTextColor(0);
        yAxis.setAxisMinValue(1);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);

        if (mChart.isRotationEnabled())
            mChart.setRotationEnabled(false);
        else
            mChart.setRotationEnabled(true);

        mChart.invalidate();
        for (IDataSet<?> set : mChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());
        mChart.invalidate();
        return view;
    }

    public void setGroup() {
        ArrayList<String> groupName = new ArrayList<>();
        groupName.clear();
        if (Activity_Analysis.realEstateDate != null) {
            groupName.add("屋型條件總分        " + realEstateDate.getMl_HouseScore() + "分");
        } else if (Activity_Analysis.myData != null) {
            groupName.add("屋型條件總分        " + myData.getMl_HouseScore() + "分");
        } else if (Activity_Analysis.myLoveSample != null) {
            groupName.add("屋型條件總分        " + myLoveSample.getMl_HouseScore() + "分");
        }
        ArrayList<ArrayList<String>> groups = new ArrayList<>();
        ArrayList<String> children1 = new ArrayList<>();
        children1.add("房屋類型");
        children1.add("坪數");
        children1.add("單價");
        children1.add("屋齡");
        children1.add("房廳衛數");
        children1.add("電梯");
        children1.add("停車位");

        groups.add(children1);
        Score_ExpListAdapter adapter = new Score_ExpListAdapter(getContext(), groups, groupName);
        expandableListView.setAdapter(adapter);
    }

    private String[] mParties = new String[]{
            "交通", "     生活機能", "鄰避設施", "自然災害", "屋型條件     "
    };

    public void setData() {

        int cnt = mParties.length;

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        if (Activity_Analysis.realEstateDate != null) {
            float traffic = Float.parseFloat(realEstateDate.getMl_TRAScore()) +
                    Float.parseFloat(realEstateDate.getMl_MRTScore()) +
                    Float.parseFloat(realEstateDate.getMl_THSRScore()) +
                    Float.parseFloat(realEstateDate.getMl_FWAYScore());
            float disaster = Float.parseFloat(realEstateDate.getMl_DisasterScore());
            float hate = Float.parseFloat(realEstateDate.getMl_AirScore()) +
                    Float.parseFloat(realEstateDate.getMl_DieScore()) +
                    Float.parseFloat(realEstateDate.getMl_FireScore()) +
                    Float.parseFloat(realEstateDate.getMl_GasScore());
            float life = Float.parseFloat(realEstateDate.getMl_LifeScore());

            yVals1.add(new Entry(traffic, 0));
            yVals1.add(new Entry(life, 1));
            yVals1.add(new Entry(hate, 2));
            yVals1.add(new Entry(disaster, 3));
            yVals1.add(new Entry(Float.parseFloat(realEstateDate.getMl_HouseScore()), 4));
        } else if (Activity_Analysis.myData != null) {
            float traffic = Float.parseFloat(myData.getMl_MRTScore()) +
                    Float.parseFloat(myData.getMl_TRAScore()) +
                    Float.parseFloat(myData.getMl_THSRScore()) +
                    Float.parseFloat(myData.getMl_FWAYScore());
            float life = Float.parseFloat(myData.getMl_LifeScore());
            float disaster = Float.parseFloat(myData.getMl_DisasterScore());
            float hate = Float.parseFloat(myData.getMl_AirScore()) +
                    Float.parseFloat(myData.getMl_GasScore()) +
                    Float.parseFloat(myData.getMl_DieScore()) +
                    Float.parseFloat(myData.getMl_FireScore());
            float house = Float.parseFloat(myData.getMl_HouseScore());
            yVals1.add(new Entry(traffic, 0));
            yVals1.add(new Entry(life, 1));
            yVals1.add(new Entry(hate, 2));
            yVals1.add(new Entry(disaster, 3));
            yVals1.add(new Entry(house, 4));
        } else if (Activity_Analysis.myLoveSample != null) {
            float traffic = Float.parseFloat(myLoveSample.getMl_MRTScore()) +
                    Float.parseFloat(myLoveSample.getMl_TRAScore()) +
                    Float.parseFloat(myLoveSample.getMl_THSRScore()) +
                    Float.parseFloat(myLoveSample.getMl_FWAYScore());
            float life = Float.parseFloat(myLoveSample.getMl_LifeScore());
            float disaster = Float.parseFloat(myLoveSample.getMl_DisasterScore());
            float hate = Float.parseFloat(myLoveSample.getMl_AirScore()) +
                    Float.parseFloat(myLoveSample.getMl_GasScore()) +
                    Float.parseFloat(myLoveSample.getMl_DieScore()) +
                    Float.parseFloat(myLoveSample.getMl_FireScore());
            float house = Float.parseFloat(myLoveSample.getMl_HouseScore());
            yVals1.add(new Entry(traffic, 0));
            yVals1.add(new Entry(life, 1));
            yVals1.add(new Entry(hate, 2));
            yVals1.add(new Entry(disaster, 3));
            yVals1.add(new Entry(house, 4));
        }


        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < cnt; i++) {
            xVals.add(mParties[i]);
        }

        RadarDataSet set1 = new RadarDataSet(yVals1, "");
        set1.setColor(ColorTemplate.COLOR_NONE);
        set1.setFillColor(ColorTemplate.VORDIPLOM_COLORS[0]);
        set1.setDrawFilled(true);
        set1.setLineWidth(3f);

//        RadarDataSet set2 = new RadarDataSet(yVals2, "房屋2");
//        set2.setColor(ColorTemplate.VORDIPLOM_COLORS[4]);
//        set2.setFillColor(ColorTemplate.VORDIPLOM_COLORS[4]);
//        set2.setDrawFilled(true);
//        set2.setLineWidth(2f);

        ArrayList<IRadarDataSet> sets = new ArrayList<IRadarDataSet>();
        sets.add(set1);
        //sets.add(set2);

        RadarData data = new RadarData(xVals, sets);
        data.setValueTextSize(10f);
        data.setDrawValues(false);

        mChart.setData(data);
        mChart.invalidate();
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getButtonView();
        setButtonEvent();


        //設定按鈕的ClickListener
        toastButton2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                //當使用者按下按鈕時顯示Toast
                //Toast.LENGTH_LONG表示顯示時間較長，Toast.LENGTH_SHORT則表示顯示時間較短
                if (Activity_Analysis.realEstateDate != null) {
                    httpGet.addMyLoveEstate(m_id, realEstateDate);
                } else if (Activity_Analysis.myData != null) {
                    httpGet.addMyLoveReal(m_id, myData, realHouse.getElec(), realHouse.getCar());
                }
                toastButton2.setEnabled(false);
                Toast.makeText(view.getContext(), "成功加入最愛", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setButtonEvent() {
        //fravorateic.setOnClickListener(buttonListener);
        mapic.setOnClickListener(buttonListener);
        questic.setOnClickListener(buttonListener);
        trafficic.setOnClickListener(buttonListener);
        lifeic.setOnClickListener(buttonListener);
        badic.setOnClickListener(buttonListener);
        distroyic.setOnClickListener(buttonListener);
        conditionic.setOnClickListener(buttonListener);
    }

    public void getButtonView() {
        mapic = (FloatingActionButton) this.getView().findViewById(R.id.fab2);
        questic = (FloatingActionButton) this.getView().findViewById(R.id.fab3);
        trafficic = (Button) this.getView().findViewById(R.id.btn1);
        lifeic = (Button) this.getView().findViewById(R.id.btn2);
        badic = (Button) this.getView().findViewById(R.id.btn3);
        distroyic = (Button) this.getView().findViewById(R.id.btn4);
        conditionic = (Button) this.getView().findViewById(R.id.btn5);
    }

    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.fab2:
                    new AlertDialog.Builder(getContext())
                            .setTitle("即將前往地圖")
                            .setMessage("是否前往地圖查看地址位置？")
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setNegativeButton("否", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case R.id.fab3:
                    new AlertDialog.Builder(getActivity())
                            .setTitle("小提醒")
                            .setMessage("關於理想屋型達成率的說明")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case R.id.btn1:
                    new AlertDialog.Builder(getContext())
                            .setTitle("交通")
                            .setMessage("分數")
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setNegativeButton("否", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case R.id.btn2:
                    new AlertDialog.Builder(getContext())
                            .setTitle("生活機能")
                            .setMessage("分數")
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setNegativeButton("否", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case R.id.btn3:
                    new AlertDialog.Builder(getContext())
                            .setTitle("鄰避設施")
                            .setMessage("分數")
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setNegativeButton("否", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case R.id.btn4:
                    new AlertDialog.Builder(getContext())
                            .setTitle("自然災害")
                            .setMessage("分數")
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setNegativeButton("否", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;

                case R.id.btn5:
                    new AlertDialog.Builder(getContext())
                            .setTitle("屋型條件")
                            .setMessage("分數")
                            .setPositiveButton("是", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setNegativeButton("否", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
            }
        }
    };
}

