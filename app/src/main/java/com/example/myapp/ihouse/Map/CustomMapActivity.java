package com.example.myapp.ihouse.map;

//將加入分析與最愛改至點擊marker

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.example.myapp.ihouse.MainActivity;
import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.contact_us.Contactus;
import com.example.myapp.ihouse.favorite.MyLoveActivity;
import com.example.myapp.ihouse.mod.ModActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class CustomMapActivity extends BaseMapActivity implements GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraChangeListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private FloatingActionButton fab_add;
    private FloatingActionButton fab_location;
    private Boolean locationGet = false;
    private FloatingSearchView mSearchView;
    private boolean isLocation = false;
    private LatLng latLng;
    private String addressN;


    @Override
    protected void startDemo() {
        getMap().setOnMyLocationButtonClickListener(this);
        getMap().setOnCameraChangeListener(this);
        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        fab_add = (FloatingActionButton) findViewById(R.id.fab_add);
        fab_location = (FloatingActionButton) findViewById(R.id.fab_location);
        enableMyLocation();
        System.out.println(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED);
        locationstatus();
        setupFloatingSearch();
        setFloatingBtn();
        navigationViewR = (NavigationView) findViewById(R.id.nav_view);
        mSearchView.attachNavigationDrawerToMenuButton(mDrawerLayout);
        navigationViewR.setNavigationItemSelectedListener(this);


        fab_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager status = (LocationManager) (getApplication().getSystemService(LOCATION_SERVICE));
                if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplication(), "請打開位置權限", Toast.LENGTH_LONG).show();
                    final Intent i = new Intent();
                    i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.setData(Uri.parse("package:" + getPackageName()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(new Intent(i));
                } else {
                    if (status.isProviderEnabled(LocationManager.GPS_PROVIDER) || status.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        onMyLocationButtonClick();
                        try {
                            getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(getMap().getMyLocation().getLatitude(), getMap().getMyLocation().getLongitude()), 15));
                            isLocation = true;
                        } catch (Exception e) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    } else {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                }
            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                return true;
            case R.id.favorite:
                startActivity(new Intent(getApplicationContext(), MyLoveActivity.class));
                return true;
            case R.id.mod:
                startActivity(new Intent(getApplicationContext(), ModActivity.class));
                return true;
            case R.id.contact:
                startActivity(new Intent(getApplicationContext(), Contactus.class));
                return true;
            default:
                return true;
        }
    }

    private void setFloatingBtn() {
        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CusHouseDialog cusHouseDialog = new CusHouseDialog();
                if (locationGet || isLocation) {
                    try {
                        Double lat = getMap().getMyLocation().getLatitude();
                        Double lng = getMap().getMyLocation().getLongitude();
                        if ((!lat.isNaN() || !lng.isNaN()) && getMap().getProjection().getVisibleRegion().latLngBounds.contains(new LatLng(lat, lng))) {
                            cusHouseDialog.setLat(getMap().getMyLocation().getLatitude());
                            cusHouseDialog.setLng(getMap().getMyLocation().getLongitude());
                            System.out.println("定");
                            cusHouseDialog.show(getFragmentManager(), "CusHouseDialog");
                        } else if (latLng != null && getMap().getProjection().getVisibleRegion().latLngBounds.contains(latLng)) {
                            cusHouseDialog.setLat(latLng.latitude);
                            cusHouseDialog.setLng(latLng.longitude);
                            System.out.println(addressN);
                            System.out.println("搜");
                            cusHouseDialog.show(getFragmentManager(), "CusHouseDialog");
                        } else {
                            Toast.makeText(getApplication(), "未定位或查詢", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        if (latLng != null && getMap().getProjection().getVisibleRegion().latLngBounds.contains(latLng)) {
                            cusHouseDialog.setLat(latLng.latitude);
                            cusHouseDialog.setLng(latLng.longitude);
                            System.out.println(addressN);
                            System.out.println("搜");
                            cusHouseDialog.show(getFragmentManager(), "CusHouseDialog");
                        } else {
                            Toast.makeText(getApplication(), "未定位或查詢", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getApplication(), "未定位或查詢", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void locationstatus() {
        LocationManager status = (LocationManager) (this.getSystemService(LOCATION_SERVICE));
        //System.out.println(status.isProviderEnabled(LocationManager.GPS_PROVIDER));
        //System.out.println(status.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        if (status.isProviderEnabled(LocationManager.GPS_PROVIDER) || status.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationGet = true;
        } else {
            locationGet = false;
            System.out.println(locationGet);
            getMap().getUiSettings().setMyLocationButtonEnabled(locationGet);
//            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_cus_map;
    }


    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    android.Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (getMap() != null) {
            // Access to the location has been granted to the app.
            locationGet = true;
            System.out.println(locationGet);
            getMap().setMyLocationEnabled(locationGet);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
            showMissingPermissionError();
        }
    }


    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }


    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    public void onMapSearch(String location) {
        List<Address> addressList = null;

        if (location != null || !location.equals("")) {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 3);

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!addressList.isEmpty()) {
                Address address = addressList.get(0);
                addressN = address.toString();
                System.out.println(addressN);
                latLng = new LatLng(address.getLatitude(), address.getLongitude());
                getMap().addMarker(new MarkerOptions().position(latLng).title(location).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                getMap().animateCamera(CameraUpdateFactory.newLatLng(latLng));
                isLocation = true;
            }
        }
    }

    private void setupFloatingSearch() {

        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                if (item.getItemId() == R.id.action_search) {
                    onMapSearch(mSearchView.getQuery());
                    isLocation = true;
                } else if (item.getItemId() == R.id.action_mode) {
//                    startActivity(new Intent(CustomMapActivity.this, MapsActivity.class));
                    CustomMapActivity.super.onBackPressed();
                } else if (item.getItemId() == R.id.action_layer) {
                    mDrawerLayout.openDrawer(navigationViewL);
                }

            }
        });
        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                String location = currentQuery;
                List<Address> addressList = null;

                if (location != null || !location.equals("")) {
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    try {
                        addressList = geocoder.getFromLocationName(location, 3);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (!addressList.isEmpty()) {
                        getMap().clear();
                        Address address = addressList.get(0);
                        addressN = address.toString();
                        System.out.println(addressN);
                        latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        getMap().addMarker(new MarkerOptions().position(latLng).title(location).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                        isLocation = true;
                    }
                }
            }
        });
    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        return false;
    }
}
