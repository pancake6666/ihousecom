package com.example.myapp.ihouse.favorite;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.myapp.ihouse.MainActivity;
import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.analysis.Activity_Analysis;
import com.example.myapp.ihouse.contact_us.Contactus;
import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.MyLoveSample;
import com.example.myapp.ihouse.httpGet.Part2;
import com.example.myapp.ihouse.map.MapsActivity;
import com.example.myapp.ihouse.mod.ModActivity;
import com.google.common.collect.EvictingQueue;

import java.util.ArrayList;
import java.util.Queue;


public class MyLoveActivity extends AppCompatActivity implements CardAdapter.Listener, AdapterView.OnItemSelectedListener, View.OnClickListener, View.OnLongClickListener, NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private CardAdapter mAdapter;
    private ArrayList<MyLoveSample> myLoveSampleArrayList;
    private ArrayAdapter<String> spinnerList;
    private Queue<String> list = EvictingQueue.create(2);
    private Part2 part2;
    private DrawerLayout mDrawerLayout;
    private int m_id;
    private GetURL httpGet;
    private Hweight hweight;
    private loadMyLove myLove;
    private Bundle toDetail;
    private Intent intent;
    private ProgressDialog progressDialog;
    private TextView count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_love);

        Spinner spinner = (Spinner) findViewById(R.id.compare);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        count = (TextView) findViewById(R.id.count);


        httpGet = new GetURL();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        m_id = sharedPreferences.getInt("ID", 0);
        myLove = new loadMyLove();
        myLove.execute(m_id);
        intent = new Intent(MyLoveActivity.this, Activity_Analysis.class);

        assert mRecyclerView != null;
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CardAdapter(null, MyLoveActivity.this);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);
        count.setText(String.valueOf(list.size()));


        spinnerList = new ArrayAdapter<String>(MyLoveActivity.this,
                R.layout.support_simple_spinner_dropdown_item,
                android.R.id.text1);
        spinner.setAdapter(spinnerList);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


//    public void setOnAdpter(List<House> houses) {
//        mAdapter.getItems().clear();
//        mAdapter.getItems().addAll(houses);
//        mRecyclerView.setAdapter(mAdapter);
//        floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!removeList.isEmpty()) {
//                    for (Long id : removeList) {
//                        houseDAO.delete(id);
//                    }
//                    removeList.clear();
//                    startActivity(new Intent(MyLoveActivity.this, MyLoveActivity.class));
//                    finish();
//                } else {
//                    Toast.makeText(getApplicationContext(), "無勾選", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//    }


    @Override
    public void onItemClicked(MyLoveSample myLoveItem) { //短按
        if (myLoveItem != null) {
            EditDialog editDialog = new EditDialog();
            editDialog.show(getFragmentManager(), "editDialog");
        }
    }

    @Override
    public void onChecked(Long id, boolean b) {
//        if (id != null) {
//            if (b) {
//                removeList.add(id);
//                floatingActionButton.setVisibility(View.VISIBLE);
//            } else {
//                removeList.remove(id);
//                if (removeList.isEmpty()) {
//                    floatingActionButton.setVisibility(View.INVISIBLE);
//                }
//            }
//        }
    }

    @Override
    public void onItemLongClicked(MyLoveSample myLoveItem) { //長按
        if (myLoveItem != null && Boolean.parseBoolean(myLoveItem.getMl_type())) {
            EditDialog editDialog = new EditDialog();
            editDialog.show(getFragmentManager(), "editDialog");
        }
    }

    @Override
    public void onButtonClicked(final int Mlid, final int realid, final int type) {
        new AlertDialog.Builder(MyLoveActivity.this)
                .setMessage("您確定要刪除？")
                .setTitle("警告").setPositiveButton("刪除", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                httpGet.DeleteMyLove(Mlid, realid, type);
                loadMyLove refresh = new loadMyLove();
                refresh.execute(m_id);
                spinnerList.clear();
                spinnerList.notifyDataSetChanged();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();

    }

    @Override
    public void onButtonCompare(MyLoveSample myLoneItem) {
        if (myLoneItem != null) {
            if (!list.contains(myLoneItem.getMl_address())) {
                list.add(myLoneItem.getMl_address());
                count.setText(String.valueOf(list.size()));
                spinnerList.clear();
                spinnerList.addAll(list);
                spinnerList.notifyDataSetChanged();
            }

        }
    }

    @Override
    public void onButtonAnalysis(MyLoveSample myLoveItem) {
        if (myLoveItem != null) {
            toDetail = new Bundle();
            intent = new Intent(this, Activity_Analysis.class);
            getDetail detail = new getDetail();
            toDetail.putSerializable("fromLovePart1", myLoveItem);
            detail.execute(new String[]{myLoveItem.getMl_rid(), myLoveItem.getMl_type()});
        }
    }

//    @Override
//    public void onButtonClicked(int id) {
//        if (id != null) {
//            if (compare.contains(id)) {
//                Toast.makeText(getApplication(), "已在列表中", Toast.LENGTH_SHORT).show();
//            } else if (compare.size() < 2) {
//                compare.offer(id);
//                badge.setText(String.valueOf(compare.size()));
//                Toast.makeText(getApplication(), "已加入", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(getApplication(), "未加入，最多比較兩筆", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//        if (i == 0) {
//            houses = houseDAO.getAll();
//            mAdapter.notifyDataSetChanged();
//            setOnAdpter(houses);
//        } else if (i == 1) {
//            houses = houseDAO.getOrder("price", "DESC");
//            mAdapter.notifyDataSetChanged();
//            setOnAdpter(houses);
//        } else if (i == 2) {
//            houses = houseDAO.getOrder("price", "ASC");
//            mAdapter.notifyDataSetChanged();
//            setOnAdpter(houses);
//        } else if (i == 3) {
//            houses = houseDAO.getOrder("addtime", "DESC");
//            mAdapter.notifyDataSetChanged();
//            setOnAdpter(houses);
//        } else if (i == 4) {
//            houses = houseDAO.getOrder("addtype", "ASC");
//            setOnAdpter(houses);
//        } else if (i == 5) {
//            houses = houseDAO.getOrder("addtype", "DESC");
//            setOnAdpter(houses);
//        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
//        if (!badge.getText().equals("2") || badge.getText() == null) {
//            System.out.println("比較列無物件");
//        } else {
//            System.out.println("比較～");
//            Bundle bundle = new Bundle();
//            ArrayList<Integer> comparelist = new ArrayList<>();
//            for (long id : compare) {
//                comparelist.add((int) id);
//            }
//            bundle.putIntegerArrayList("id", comparelist);
//            Intent i = new Intent(this, CompareActivity.class);
//            i.putExtras(bundle);
//            startActivity(i);
//        }
    }

    @Override
    public boolean onLongClick(View view) {
//        compare.clear();
//        badge.setText(String.valueOf(compare.size()));
//        Toast.makeText(getApplication(), "已清空", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                return true;
            case R.id.favorite:
                startActivity(new Intent(getApplicationContext(), MapsActivity.class));
                return true;
            case R.id.mod:
                startActivity(new Intent(getApplicationContext(), ModActivity.class));
                return true;
            case R.id.contact:
                startActivity(new Intent(getApplicationContext(), Contactus.class));
                return true;
            default:
                return true;
        }
    }

    class loadMyLove extends AsyncTask<Integer, Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... integers) {
            myLoveSampleArrayList = httpGet.getMylovelist(integers[0], "a");
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MyLoveActivity.this, "提示", "讀取中");
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mAdapter.getItems().clear();
            mAdapter.getItems().addAll(myLoveSampleArrayList);
            mRecyclerView.setAdapter(mAdapter);
            progressDialog.dismiss();
        }
    }

    class getCompare extends AsyncTask<Integer, Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... integers) {
            myLoveSampleArrayList = httpGet.getMylovelist(integers[0], "a");
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mAdapter.getItems().clear();
            mAdapter.getItems().addAll(myLoveSampleArrayList);
            mRecyclerView.setAdapter(mAdapter);
            progressDialog.dismiss();
        }
    }

    class getDetail extends AsyncTask<String[], Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(String[]... strings) {
            String[] x = strings[0];
            part2 = httpGet.getRealLove(x[0], x[1]);
            hweight = httpGet.getHweight(m_id);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            toDetail.putSerializable("fromLovePart2", part2);
            toDetail.putSerializable("hweight", hweight);
            intent.putExtras(toDetail);
            progressDialog.dismiss();
            startActivity(intent);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MyLoveActivity.this, "", "");
        }
    }
}
