package com.example.myapp.ihouse;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.register.StepActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;

/**
 * Activity to demonstrate basic retrieval of the Google user's ID, femail address, and basic
 * profile.
 */
public class SignInActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private TextView mStatusTextView;
    private TextView displayname;
    private ProgressDialog mProgressDialog;
    private GetURL httpGet;
    private String femail;
    private String gmail;
    private String token;
    private int type;
    private int m_id;
    SharedPreferences member_id;
    private static final String data = "DATA";
    private static final String id = "ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        httpGet = new GetURL();
        callbackManager = CallbackManager.Factory.create();
//        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setEnabled(false);
//        loginButton.setReadPermissions(Arrays.asList(
//                "public_profile", "email"));
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(final LoginResult loginResult) {
//                System.out.println("token " + loginResult.getAccessToken().getToken());
//                // App code
//                GraphRequest request = GraphRequest.newMeRequest(
//                        loginResult.getAccessToken(),
//                        new GraphRequest.GraphJSONObjectCallback() {
//                            @Override
//                            public void onCompleted(JSONObject object, GraphResponse response) {
//                                Log.v("LoginActivity", response.toString());
//                                // Application code
//                                try {
//                                    type = 0;
//                                    femail = response.getJSONObject().getString("email");
//                                    token = loginResult.getAccessToken().getToken();
//                                    System.out.println(femail);
//                                    System.out.println(loginResult.getAccessToken().getToken());
//                                    System.out.println(type);
//                                    Thread thread = new Thread(fmutiThread);
//                                    thread.start();
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//                        });
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,name,email");
//                request.setParameters(parameters);
//                request.executeAsync();
//                hideProgressDialog();
//                updateUI(true);
//            }
//
//
//            @Override
//            public void onCancel() {
//                updateUI(false);
//                System.out.println("Login attempt canceled.");
//            }
//
//            @Override
//            public void onError(FacebookException e) {
//                updateUI(false);
//                System.out.println(e.toString());
//            }
//        });
        member_id = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        // Views
        mStatusTextView = (TextView) findViewById(R.id.status);
        displayname = (TextView) findViewById(R.id.detail);

        // Button listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);


        // [START configure_signin]
        // Configure sign-in to request the user's ID, femail address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* AnalysisActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]

        // [START customize_button]
        // Customize sign-in button. The sign-in button can be displayed in
        // multiple sizes and color schemes. It can also be contextually
        // rendered based on the requested scopes. For example. a red button may
        // be displayed when Google+ scopes are requested, but a white button
        // may be displayed when only basic profile is requested. Try adding the
        // Scopes.PLUS_LOGIN scope to the GoogleSignInOptions to see the
        // difference.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        // [END customize_button]
    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            //showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    // [END onActivityResult]

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        Log.d(TAG, "handleSignInResult:" + result.getStatus());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            type = 1;
            GoogleSignInAccount acct = result.getSignInAccount();
            mStatusTextView.setText("歡迎您");
            assert acct != null;
            displayname.setText(acct.getDisplayName());
            gmail = acct.getEmail();
            token = acct.getIdToken();
            System.out.println(acct.getEmail());
            System.out.println(acct.getIdToken());
            ArrayList<String> data = new ArrayList<>();
            data.add(gmail);
            data.add(token);
            showProgressDialog();
            getGM_Id getgMId = new getGM_Id();
            getgMId.execute(data);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false, 0, -1);
        }
    }
    // [END handleSignInResult]

//    private void handleSignInResultforfacebook(LoginResult result) {
//        Log.d(TAG, "handleSignInResult:" + result);
//        Log.d(TAG, "handleSignInResult:" + result);
//        if (result.isSuccess()) {
//            // Signed in successfully, show authenticated UI.
//            type = 1;
//            GoogleSignInAccount acct = result.getSignInAccount();
//            mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getIdToken()));
//            gmail = acct.getEmail();
//            token = acct.getIdToken();
//            System.out.println(acct.getEmail());
//            System.out.println(acct.getIdToken());
//            Thread thread = new Thread(gmutiThread);
//            thread.start();
//            updateUI(true);
//        } else {
//            // Signed out, show unauthenticated UI.
//            updateUI(false);
//        }
//    }

    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]


    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        // [START_EXCLUDE]
                        updateUI(false, 0, -1);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void updateUI(boolean signedIn, int m_id, int isExist) {
//        || AccessToken.getCurrentAccessToken() != null
        if (signedIn) {
            System.out.println(m_id);
            member_id.edit().putInt(id, m_id).apply();
            System.out.println(member_id.contains("ID"));
            if (isExist == 1) {
                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                finish();
            } else if (isExist == 0) {
                startActivity(new Intent(SignInActivity.this, StepActivity.class));
                finish();
            }
        } else {
            mStatusTextView.setText(R.string.signed_out);
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            //findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;

        }
    }


    class getGM_Id extends AsyncTask<ArrayList<String>, Integer, Integer[]> {

        @Override
        protected Integer[] doInBackground(ArrayList<String>... arrayLists) {
            ArrayList<String> x = arrayLists[0];
            int m_id = httpGet.Login(x.get(0), x.get(1), 1);
            int isExist = httpGet.checkMyhouse(m_id);
            return new Integer[]{m_id, isExist};
        }

        @Override
        protected void onPostExecute(Integer[] integer) {
            super.onPostExecute(integer);
            member_id.edit().putInt(id, integer[0]).apply();
            hideProgressDialog();
            updateUI(true, integer[0], integer[1]);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }
    }

}
