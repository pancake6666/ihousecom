package com.example.myapp.ihouse.register;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;


public class StepHousePage extends Page {
    public static final String AREA_DATA_KEY = "area";
    public static final String AREA_DATA_VAL = "area_v";
    public static final String TYPE_DATA_KEY = "type";
    public static final String PRICE_MIN_DATA_KEY = "price_min";
    public static final String PRICE_MAX_DATA_KEY = "price_max";
    public static final String SPACE_MIN_DATA_KEY = "space_min";
    public static final String SPACE_MAX_DATA_KEY = "space_man";
    public static final String YEARS_MIN_DATA_KEY = "years_min";
    public static final String YEARS_MAX_DATA_KEY = "years_max";
    public static final String ROOM_DATA_KEY = "room";
    public static final String LROOM_DATA_KEY = "l_room";
    public static final String BATH_DATA_KEY = "bath";
    public static final String CAR_DATA_KEY = "car";
    public static final String ELEV_DATA_KEY = "ELEV";
    public static final String TYPE_DATA_VALUE = "type_v";
    public static final String PRICE_MIN_DATA_VALUE = "price_min_v";
    public static final String PRICE_MAX_DATA_VALUE = "price_max_v";
    public static final String SPACE_MIN_DATA_VALUE = "space_min_v";
    public static final String SPACE_MAX_DATA_VALUE = "space_man_v";
    public static final String YEARS_MIN_DATA_VALUE = "years_min_v";
    public static final String YEARS_MAX_DATA_VALUE = "years_max_v";
    public static final String ROOM_DATA_VALUE = "room_v";
    public static final String LROOM_DATA_VALUE = "l_room_v";
    public static final String BATH_DATA_VALUE = "bath_v";
    public static final String CAR_DATA_VALUE = "car_v";
    public static final String ELEV_DATA_VALUE = "ELEV_v";


    public StepHousePage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return HouseFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        getHouseData();
        String type = null;
        String car = null;
        String elev = null;
        switch (mData.getInt(TYPE_DATA_KEY)) {
            case 1:
                type = "公寓";
                break;
            case 2:
                type = "住宅大樓";
                break;
            case 4:
                type = "套房";
                break;
            case 5:
                type = "透天厝";
                break;
            case 6:
                type = "華廈";
                break;
            case -1:
                type = "不拘";
                break;
        }
        switch (mData.getInt(CAR_DATA_KEY)) {
            case 0:
                car = "無";
                break;
            case 1:
                car = "有";
                break;
            case -1:
                car = "不拘";
                break;
        }
        switch (mData.getInt(ELEV_DATA_KEY)) {
            case 0:
                elev = "無";
                break;
            case 1:
                elev = "有";
                break;
            case -1:
                elev = "不拘";
                break;
        }
        dest.add(new ReviewItem("屋型", type, getKey(), -1));
        dest.add(new ReviewItem("價位", String.format("%s ~ %s", mData.getString(PRICE_MIN_DATA_KEY), mData.getString(PRICE_MAX_DATA_KEY)), getKey(), -1));
        dest.add(new ReviewItem("坪數", String.format("%s ~ %s", mData.getString(SPACE_MIN_DATA_KEY), mData.getString(SPACE_MAX_DATA_KEY)), getKey(), -1));
        dest.add(new ReviewItem("屋齡", String.format("%s ~ %s", mData.getString(YEARS_MIN_DATA_KEY), mData.getString(YEARS_MAX_DATA_KEY)), getKey(), -1));
        dest.add(new ReviewItem("格局", String.format("房:%s 廳:%s 衛:%s", mData.getString(ROOM_DATA_KEY), mData.getString(LROOM_DATA_KEY), mData.getString(BATH_DATA_KEY)), getKey(), -1));
        dest.add(new ReviewItem("電梯", elev, getKey(), -1));
        dest.add(new ReviewItem("車位", car, getKey(), -1));
    }


    @Override
    public boolean isCompleted() {
        return mData.getInt(TYPE_DATA_KEY) != 0
                && (Integer.parseInt(mData.getString(PRICE_MIN_DATA_KEY)) <= Integer.parseInt(mData.getString(PRICE_MAX_DATA_KEY)))
                && Integer.parseInt(mData.getString(SPACE_MIN_DATA_KEY)) <= Integer.parseInt(mData.getString(SPACE_MAX_DATA_KEY))
                && Integer.parseInt(mData.getString(YEARS_MIN_DATA_KEY)) <= Integer.parseInt(mData.getString(YEARS_MAX_DATA_KEY));
    }

    public void getHouseData() {
//        houseData = new ArrayList<>();
//        houseData.add(mData.getInt(TYPE_DATA_KEY));
//        houseData.add(Integer.parseInt(mData.getString(PRICE_MIN_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(PRICE_MAX_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(SPACE_MIN_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(SPACE_MAX_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(YEARS_MIN_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(YEARS_MAX_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(ROOM_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(LROOM_DATA_KEY)));
//        houseData.add(Integer.parseInt(mData.getString(BATH_DATA_KEY)));
//        houseData.add(mData.getInt(ELEV_DATA_KEY));
//        houseData.add(mData.getInt(CAR_DATA_KEY));
        StepActivity.hweight.setSection(mData.getString(AREA_DATA_KEY));
        StepActivity.hweight.setType(mData.getInt(TYPE_DATA_KEY));
        StepActivity.hweight.setPricemin(Integer.parseInt(mData.getString(PRICE_MIN_DATA_KEY)));
        StepActivity.hweight.setPricemax(Integer.parseInt(mData.getString(PRICE_MAX_DATA_KEY)));
        StepActivity.hweight.setSpacemin(Integer.parseInt(mData.getString(SPACE_MIN_DATA_KEY)));
        StepActivity.hweight.setSpacemax(Integer.parseInt(mData.getString(SPACE_MAX_DATA_KEY)));
        StepActivity.hweight.setYearmin(Integer.parseInt(mData.getString(YEARS_MIN_DATA_KEY)));
        StepActivity.hweight.setYearmax(Integer.parseInt(mData.getString(YEARS_MAX_DATA_KEY)));
        StepActivity.hweight.setRoom(Integer.parseInt(mData.getString(ROOM_DATA_KEY)));
        StepActivity.hweight.setLRoom(Integer.parseInt(mData.getString(LROOM_DATA_KEY)));
        StepActivity.hweight.setWC(Integer.parseInt(mData.getString(BATH_DATA_KEY)));
        StepActivity.hweight.setElevator(mData.getInt(ELEV_DATA_KEY));
        StepActivity.hweight.setCar(mData.getInt(CAR_DATA_KEY));
    }

}
