package com.example.myapp.ihouse.analysis;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.httpGet.MyData;
import com.example.myapp.ihouse.httpGet.MyLoveSample;
import com.example.myapp.ihouse.httpGet.Part2;
import com.example.myapp.ihouse.httpGet.RealEstateDate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.geojson.GeoJsonLayer;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class Analysis_NaturalFragment extends Fragment implements OnMapReadyCallback {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;

    private ImageButton alertdlgs;
    //private FloatingActionButton toastButton2;
    public static RealEstateDate realEstateDate;
    public static MyData myData;
    public static MyData bySample;
    public static MyLoveSample myLoveSample;
    public MapView mapView;
    public GoogleMap mMap;
    public static RealHouse realHouse;
    public LatLng latLng;
    ProgressDialog progressDialog;
    public static Part2 part2;
    GeoJsonLayer soilhight;
    GeoJsonLayer soilmid;
    GeoJsonLayer soillow;
    GeoJsonLayer flood;
    GeoJsonLayer fault;

    public static Analysis_NaturalFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        Analysis_NaturalFragment fragment = new Analysis_NaturalFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.analysis_natural, container, false);
        TextView address = (TextView) view.findViewById(R.id.address);
        TextView isSoil = (TextView) view.findViewById(R.id.isSoil);
        TextView isFault = (TextView) view.findViewById(R.id.isFault);
        TextView isFlood = (TextView) view.findViewById(R.id.isFlood);
        if (Activity_Analysis.realEstateDate != null) {
            latLng = new LatLng(Double.parseDouble(realEstateDate.getLat()), Double.parseDouble(realEstateDate.getLon()));
            address.setText(realEstateDate.getRealAddr());
            if (realEstateDate.getRealAddr().length() > 11) {
                address.setTextSize(30f);
            }
            if (!realEstateDate.getSoilLiq_ID().equals("999")) {
                isSoil.setText("有");
            }
            if (!realEstateDate.getFault_ID().equals("999")) {
                isFault.setText("有");
            }
            if (!realEstateDate.getFlood_ID().equals("999")) {
                isFlood.setText("有");
            }
        } else if (Activity_Analysis.myData != null) {
            latLng = new LatLng(Double.parseDouble(myData.getLat()), Double.parseDouble(myData.getLon()));
            address.setText(myData.getRealAddr());
            if (!myData.getSoilLiq_ID().equals("999")) {
                isSoil.setText("有");
            }
            if (!myData.getFault_ID().equals("999")) {
                isFault.setText("有");
            }
            if (!myData.getFlood_ID().equals("999")) {
                isFlood.setText("有");
            }
        } else if (Activity_Analysis.myLoveSample != null) {
            latLng = new LatLng(Double.parseDouble(myLoveSample.getMl_lat()), Double.parseDouble(myLoveSample.getMl_lon()));
            address.setText(myLoveSample.getMl_address());
            if (!part2.getSoilLiqID().equals("999")) {
                isSoil.setText("有");
            }
            if (!part2.getFaultID().equals("999")) {
                isFault.setText("有");
            }
            if (!part2.getFloodID().equals("999")) {
                isFlood.setText("有");
            }
        }
        mapView = (MapView) view.findViewById(R.id.map);
        mapView.getMapAsync(this);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.onLowMemory();


        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getButtonView();
        setButtonEvent();

//        toastButton2 = (FloatingActionButton) this.getView().findViewById(R.id.fab2);
//        //設定按鈕的ClickListener
//        toastButton2.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //當使用者按下按鈕時顯示Toast
//                //Toast.LENGTH_LONG表示顯示時間較長，Toast.LENGTH_SHORT則表示顯示時間較短
//                Toast.makeText(view.getContext(), "成功加入最愛", Toast.LENGTH_LONG).show();
//            }
//        });
    }

    public void setButtonEvent() {
        alertdlgs.setOnClickListener(buttonListener);
    }

    public void getButtonView() {
        alertdlgs = (ImageButton) this.getView().findViewById(R.id.fab2);

    }

    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.fab2:
                    new AlertDialog.Builder(getActivity())
                            .setTitle("小提醒")
                            .setMessage("在自然災害情形中，您可以看到以您所選擇位置為中心周遭之土壤液化、斷層及淹水分布情形。")
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
            }
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        AddLayer addLayer = new AddLayer();
        addLayer.execute(googleMap);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class AddLayer extends AsyncTask<GoogleMap, Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(GoogleMap... googleMaps) {
            try {
                //soil = new GeoJsonLayer(googleMaps[0], R.raw.liquefaction, getContext());
                soilhight = new GeoJsonLayer(googleMaps[0], R.raw.liquefaction, getContext());
                soilhight.getDefaultPolygonStyle().setStrokeWidth(0);
                soilhight.getDefaultPolygonStyle().setFillColor(Color.argb(150, 200, 0, 0));

                soilmid = new GeoJsonLayer(googleMaps[0], R.raw.liquefactionmid, getContext());
                soilmid.getDefaultPolygonStyle().setStrokeWidth(0);
                soilmid.getDefaultPolygonStyle().setFillColor(Color.argb(150, 255, 255, 0));

                soillow = new GeoJsonLayer(googleMaps[0], R.raw.liquefactionlow, getContext());
                soillow.getDefaultPolygonStyle().setStrokeWidth(0);
                soillow.getDefaultPolygonStyle().setFillColor(Color.argb(150, 0, 255, 0));
                flood = new GeoJsonLayer(googleMaps[0], R.raw.flooding, getContext());
                flood.getDefaultPolygonStyle().setStrokeWidth(0);
                flood.getDefaultPolygonStyle().setFillColor(Color.argb(150, 0, 0, 255));
                fault = new GeoJsonLayer(googleMaps[0], R.raw.activefault, getContext());
                fault.getDefaultLineStringStyle().setColor(Color.argb(255, 255, 0, 0));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            soilhight.addLayerToMap();
            soilmid.addLayerToMap();
            soillow.addLayerToMap();
            flood.addLayerToMap();
            fault.addLayerToMap();
        }
    }
}
