package com.example.myapp.ihouse.httpGet;

public class MyLoveSample implements java.io.Serializable {
    private static final long serialVersionUID = 7944872134288335057L;
    private String ml_HosScore;
    private String ml_HighScore;
    private String ml_MIDScore;
    private String ml_id;
    private String mid;
    private String ml_SportScore;
    private String ml_AirScore;
    private String ml_LibScore;
    private String ml_HouseScore;
    private String ml_Score;
    private String ml_LeisureScore;
    private String ml_PostScore;
    private String ml_LifeScore;
    private String ml_GasScore;
    private String ml_FWAYScore;
    private String ml_type;
    private String ml_ELEScore;
    private String ml_address;
    private String ml_TRAScore;
    private String ml_lat;
    private String ml_THSRScore;
    private String ml_MarketScore;
    private String ml_DepartScore;
    private String ml_DisasterScore;
    private String ml_DieScore;
    private String ml_FireScore;
    private String ml_time;
    private String ml_MRTScore;
    private String ml_rid;
    private String ml_EduScore;
    private String ml_lon;
    private String ml_ParkScore;

    public String getMl_HosScore() {
        return this.ml_HosScore;
    }

    public void setMl_HosScore(String ml_HosScore) {
        this.ml_HosScore = ml_HosScore;
    }

    public String getMl_HighScore() {
        return this.ml_HighScore;
    }

    public void setMl_HighScore(String ml_HighScore) {
        this.ml_HighScore = ml_HighScore;
    }

    public String getMl_MIDScore() {
        return this.ml_MIDScore;
    }

    public void setMl_MIDScore(String ml_MIDScore) {
        this.ml_MIDScore = ml_MIDScore;
    }

    public String getMl_id() {
        return this.ml_id;
    }

    public void setMl_id(String ml_id) {
        this.ml_id = ml_id;
    }

    public String getMid() {
        return this.mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMl_SportScore() {
        return this.ml_SportScore;
    }

    public void setMl_SportScore(String ml_SportScore) {
        this.ml_SportScore = ml_SportScore;
    }

    public String getMl_AirScore() {
        return this.ml_AirScore;
    }

    public void setMl_AirScore(String ml_AirScore) {
        this.ml_AirScore = ml_AirScore;
    }

    public String getMl_LibScore() {
        return this.ml_LibScore;
    }

    public void setMl_LibScore(String ml_LibScore) {
        this.ml_LibScore = ml_LibScore;
    }

    public String getMl_HouseScore() {
        return this.ml_HouseScore;
    }

    public void setMl_HouseScore(String ml_HouseScore) {
        this.ml_HouseScore = ml_HouseScore;
    }

    public String getMl_Score() {
        return this.ml_Score;
    }

    public void setMl_Score(String ml_Score) {
        this.ml_Score = ml_Score;
    }

    public String getMl_LeisureScore() {
        return this.ml_LeisureScore;
    }

    public void setMl_LeisureScore(String ml_LeisureScore) {
        this.ml_LeisureScore = ml_LeisureScore;
    }

    public String getMl_PostScore() {
        return this.ml_PostScore;
    }

    public void setMl_PostScore(String ml_PostScore) {
        this.ml_PostScore = ml_PostScore;
    }

    public String getMl_LifeScore() {
        return this.ml_LifeScore;
    }

    public void setMl_LifeScore(String ml_LifeScore) {
        this.ml_LifeScore = ml_LifeScore;
    }

    public String getMl_GasScore() {
        return this.ml_GasScore;
    }

    public void setMl_GasScore(String ml_GasScore) {
        this.ml_GasScore = ml_GasScore;
    }

    public String getMl_FWAYScore() {
        return this.ml_FWAYScore;
    }

    public void setMl_FWAYScore(String ml_FWAYScore) {
        this.ml_FWAYScore = ml_FWAYScore;
    }

    public String getMl_type() {
        return this.ml_type;
    }

    public void setMl_type(String ml_type) {
        this.ml_type = ml_type;
    }

    public String getMl_ELEScore() {
        return this.ml_ELEScore;
    }

    public void setMl_ELEScore(String ml_ELEScore) {
        this.ml_ELEScore = ml_ELEScore;
    }

    public String getMl_address() {
        return this.ml_address;
    }

    public void setMl_address(String ml_address) {
        this.ml_address = ml_address;
    }

    public String getMl_TRAScore() {
        return this.ml_TRAScore;
    }

    public void setMl_TRAScore(String ml_TRAScore) {
        this.ml_TRAScore = ml_TRAScore;
    }

    public String getMl_lat() {
        return this.ml_lat;
    }

    public void setMl_lat(String ml_lat) {
        this.ml_lat = ml_lat;
    }

    public String getMl_THSRScore() {
        return this.ml_THSRScore;
    }

    public void setMl_THSRScore(String ml_THSRScore) {
        this.ml_THSRScore = ml_THSRScore;
    }

    public String getMl_MarketScore() {
        return this.ml_MarketScore;
    }

    public void setMl_MarketScore(String ml_MarketScore) {
        this.ml_MarketScore = ml_MarketScore;
    }

    public String getMl_DepartScore() {
        return this.ml_DepartScore;
    }

    public void setMl_DepartScore(String ml_DepartScore) {
        this.ml_DepartScore = ml_DepartScore;
    }

    public String getMl_DisasterScore() {
        return this.ml_DisasterScore;
    }

    public void setMl_DisasterScore(String ml_DisasterScore) {
        this.ml_DisasterScore = ml_DisasterScore;
    }

    public String getMl_DieScore() {
        return this.ml_DieScore;
    }

    public void setMl_DieScore(String ml_DieScore) {
        this.ml_DieScore = ml_DieScore;
    }

    public String getMl_FireScore() {
        return this.ml_FireScore;
    }

    public void setMl_FireScore(String ml_FireScore) {
        this.ml_FireScore = ml_FireScore;
    }

    public String getMl_time() {
        return this.ml_time;
    }

    public void setMl_time(String ml_time) {
        this.ml_time = ml_time;
    }

    public String getMl_MRTScore() {
        return this.ml_MRTScore;
    }

    public void setMl_MRTScore(String ml_MRTScore) {
        this.ml_MRTScore = ml_MRTScore;
    }

    public String getMl_rid() {
        return this.ml_rid;
    }

    public void setMl_rid(String ml_rid) {
        this.ml_rid = ml_rid;
    }

    public String getMl_EduScore() {
        return this.ml_EduScore;
    }

    public void setMl_EduScore(String ml_EduScore) {
        this.ml_EduScore = ml_EduScore;
    }

    public String getMl_lon() {
        return this.ml_lon;
    }

    public void setMl_lon(String ml_lon) {
        this.ml_lon = ml_lon;
    }

    public String getMl_ParkScore() {
        return this.ml_ParkScore;
    }

    public void setMl_ParkScore(String ml_ParkScore) {
        this.ml_ParkScore = ml_ParkScore;
    }
}
