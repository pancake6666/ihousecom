package com.example.myapp.ihouse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class StartActivity extends Activity {
    /*
    if sql == null => SignInActivity
    else if default house = null => registerActivity
    else if Main page
    */
    private ImageView imageView;

    Animation h_move, i_move, ii_move;

    TextView i;
    TextView h;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        i = (TextView) findViewById(R.id.i);
        h = (TextView) findViewById(R.id.house);

        h_move = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.font_move_h);

        i_move = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.font_move_i);

        ii_move = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.font_move_ii);
//        imageView = (ImageView) findViewById(R.id.imageView);
//        Glide.with(this).load(R.raw.black).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imageView);

    }

    public boolean isNetworkStatusAvialable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            if (netInfos != null) {
                return netInfos.isConnected();
            }
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        i.startAnimation(i_move);
        i_move.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                i.setVisibility(View.VISIBLE);
                i.startAnimation(ii_move);
                ii_move.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        h.startAnimation(h_move);
                        h.setVisibility(View.VISIBLE);
                        h_move.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                if (isNetworkStatusAvialable(getApplicationContext())) {
                                    startActivity(new Intent(StartActivity.this, SignInActivity.class));
                                    onDestroy();
                                    finish();
                                } else {
                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                }
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
