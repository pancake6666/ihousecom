/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.myapp.ihouse.map;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.myapp.ihouse.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.collect.EvictingQueue;
import com.google.maps.android.geojson.GeoJsonLayer;

import org.json.JSONException;

import java.io.IOException;
import java.util.Queue;


public abstract class BaseMapActivity extends FragmentActivity implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnMyLocationButtonClickListener, NavigationView.OnNavigationItemSelectedListener, CompoundButton.OnCheckedChangeListener {
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private GoogleMap mMap;
    private boolean mPermissionDenied = false;

    private GeoJsonLayer activeLayer;
    private GeoJsonLayer floodLayer;
    private GeoJsonLayer soilLayerhight;
    private GeoJsonLayer soilLayermid;
    private GeoJsonLayer soilLayerlow;
    private GeoJsonLayer postLayer;
    private GeoJsonLayer hosLayer;
    private GeoJsonLayer marketLayer;
    private GeoJsonLayer departLayer;
    private GeoJsonLayer parkLayer;
    private GeoJsonLayer sportLayer;
    private GeoJsonLayer gasLayer;
    private GeoJsonLayer fireLayer;
    private GeoJsonLayer dieLayer;
    private GeoJsonLayer JSLayer;
    private GeoJsonLayer JCLayer;
    private GeoJsonLayer JKLayer;
    private GeoJsonLayer libLayer;

    public CheckBox real;
    public CheckBox sale;
    public CheckBox recommendBox;

    RadioGroup radioGroup2;
    RadioGroup radioGroup3;
    RadioGroup radioGroup4;
    RadioGroup radioGroup5;

    CheckBox radioButton1;
    CheckBox radioButton2;
    CheckBox radioButton3;
    RadioButton radioButton4;
    RadioButton radioButton5;
    RadioButton radioButton6;
    RadioButton radioButton7;
    RadioButton radioButton8;
    RadioButton radioButton9;
    RadioButton radioButton10;
    RadioButton radioButton11;
    RadioButton radioButton12;
    RadioButton radioButton13;
    RadioButton radioButton14;
    RadioButton radioButton15;
    RadioButton radioButton16;

    private Queue<GeoJsonLayer> layers = EvictingQueue.create(1);
    private LruCache<String, Bitmap> mMemoryCache;

    public static DrawerLayout mDrawerLayout;
    public static NavigationView navigationViewL;
    public static NavigationView navigationViewR;
    public static Button open;

    public LinearLayout layer;


    protected int getLayoutId() {
        return R.layout.activity_maps;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        setUpMap();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMap();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        if (mMap != null) {
            return;

        }
        mMap = map;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.047908, 121.517315), 13));
        loadLayer layer = new loadLayer();
        layer.execute(true);
        startDemo();
    }

    private void setUpMap() {
        if (isNetworkStatusAvialable(this)) {
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);


            // Get max available VM memory, exceeding this amount will throw an
            // OutOfMemory exception. Stored in kilobytes as LruCache takes an
            // int in its constructor.
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

            // Use 1/8th of the available memory for this memory cache.
            final int cacheSize = maxMemory / 8;

            mMemoryCache = new LruCache<String, Bitmap>(cacheSize);

            addBitmapToMemoryCacheBase("L003", BitmapFactory.decodeResource(this.getResources(), R.drawable.pos));
            addBitmapToMemoryCacheBase("L004", BitmapFactory.decodeResource(this.getResources(), R.drawable.hos));
            addBitmapToMemoryCacheBase("L001", BitmapFactory.decodeResource(this.getResources(), R.drawable.market));
            addBitmapToMemoryCacheBase("L002", BitmapFactory.decodeResource(this.getResources(), R.drawable.mall));

            addBitmapToMemoryCacheBase("A001", BitmapFactory.decodeResource(this.getResources(), R.drawable.elem));
            addBitmapToMemoryCacheBase("A002", BitmapFactory.decodeResource(this.getResources(), R.drawable.jc));
            addBitmapToMemoryCacheBase("A003", BitmapFactory.decodeResource(this.getResources(), R.drawable.jk));
            addBitmapToMemoryCacheBase("A004", BitmapFactory.decodeResource(this.getResources(), R.drawable.lib));

            addBitmapToMemoryCacheBase("B001", BitmapFactory.decodeResource(this.getResources(), R.drawable.gas));
            addBitmapToMemoryCacheBase("B002", BitmapFactory.decodeResource(this.getResources(), R.drawable.fire));
            addBitmapToMemoryCacheBase("B003", BitmapFactory.decodeResource(this.getResources(), R.drawable.die));

            addBitmapToMemoryCacheBase("E001", BitmapFactory.decodeResource(this.getResources(), R.drawable.sport));
            addBitmapToMemoryCacheBase("E002", BitmapFactory.decodeResource(this.getResources(), R.drawable.park_ic));

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            navigationViewR = (NavigationView) findViewById(R.id.rigthPanel);
            mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    if (drawerView.equals(navigationViewR)) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, navigationViewL);
//                        open.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    if (drawerView.equals(navigationViewR)) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, navigationViewL);
//                        open.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    if (drawerView.equals(navigationViewR)) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, navigationViewL);
//                        open.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onDrawerStateChanged(int newState) {

                }
            });
            radio();
            layer = (LinearLayout) findViewById(R.id.layerpanel);
            Button reset = (Button) findViewById(R.id.reset);
            reset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    radioButton1.setChecked(false);
                    radioButton2.setChecked(false);
                    radioButton3.setChecked(false);
                    while (!layers.isEmpty()) {
                        layers.remove().removeLayerFromMap();
                        radioGroup2.clearCheck();
                        radioGroup3.clearCheck();
                        radioGroup4.clearCheck();
                        radioGroup5.clearCheck();
                    }
                    System.out.println("checkafter" + layers.isEmpty());
                    System.out.println(layers.toString());
                }
            });


        } else {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        }
    }

    /**
     * Run the demo-specific code.
     */
    protected abstract void startDemo();

    protected GoogleMap getMap() {
        return mMap;
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    android.Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    private void radio() {
        real = (CheckBox) findViewById(R.id.real);
        sale = (CheckBox) findViewById(R.id.sale);
        recommendBox = (CheckBox) findViewById(R.id.recommend);

        //radioGroup1 = (RadioGroup) findViewById(R.id.group1);
        radioGroup2 = (RadioGroup) findViewById(R.id.group2);
        radioGroup3 = (RadioGroup) findViewById(R.id.group3);
        radioGroup4 = (RadioGroup) findViewById(R.id.group4);
        radioGroup5 = (RadioGroup) findViewById(R.id.group5);


        radioButton1 = (CheckBox) findViewById(R.id.radio1);
        radioButton1.setOnCheckedChangeListener(this);
        radioButton2 = (CheckBox) findViewById(R.id.radio2);
        radioButton2.setOnCheckedChangeListener(this);
        radioButton3 = (CheckBox) findViewById(R.id.radio3);
        radioButton3.setOnCheckedChangeListener(this);
        radioButton4 = (RadioButton) findViewById(R.id.radio4);
        radioButton4.setOnCheckedChangeListener(this);
        radioButton5 = (RadioButton) findViewById(R.id.radio5);
        radioButton5.setOnCheckedChangeListener(this);
        radioButton6 = (RadioButton) findViewById(R.id.radio6);
        radioButton6.setOnCheckedChangeListener(this);
        radioButton7 = (RadioButton) findViewById(R.id.radio7);
        radioButton7.setOnCheckedChangeListener(this);
        radioButton8 = (RadioButton) findViewById(R.id.radio8);
        radioButton8.setOnCheckedChangeListener(this);
        radioButton9 = (RadioButton) findViewById(R.id.radio9);
        radioButton9.setOnCheckedChangeListener(this);
        radioButton10 = (RadioButton) findViewById(R.id.radio10);
        radioButton10.setOnCheckedChangeListener(this);
        radioButton11 = (RadioButton) findViewById(R.id.radio11);
        radioButton11.setOnCheckedChangeListener(this);
        radioButton12 = (RadioButton) findViewById(R.id.radio12);
        radioButton12.setOnCheckedChangeListener(this);
        radioButton13 = (RadioButton) findViewById(R.id.radio13);
        radioButton13.setOnCheckedChangeListener(this);
        radioButton14 = (RadioButton) findViewById(R.id.radio14);
        radioButton14.setOnCheckedChangeListener(this);
        radioButton15 = (RadioButton) findViewById(R.id.radio15);
        radioButton15.setOnCheckedChangeListener(this);
        radioButton16 = (RadioButton) findViewById(R.id.radio16);
        radioButton16.setOnCheckedChangeListener(this);
    }


    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }


    public boolean isNetworkStatusAvialable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            if (netInfos != null) {
                return netInfos.isConnected();
            }
        }
        return false;
    }


    class loadLayer extends AsyncTask<Boolean, Boolean, Boolean> {

        @Override
        protected Boolean doInBackground(Boolean... booleans) {
            if (booleans[0]) {
                try {
                    activeLayer = new GeoJsonLayer(getMap(), R.raw.activefault, getApplicationContext());
                    activeLayer.getDefaultLineStringStyle().setColor(Color.argb(255, 255, 0, 0));
                    floodLayer = new GeoJsonLayer(getMap(), R.raw.flooding, getApplicationContext());
                    floodLayer.getDefaultPolygonStyle().setStrokeWidth(0);
                    floodLayer.getDefaultPolygonStyle().setFillColor(Color.argb(150, 0, 0, 255));
                    soilLayerhight = new GeoJsonLayer(getMap(), R.raw.liquefaction, getApplicationContext());
                    soilLayerhight.getDefaultPolygonStyle().setStrokeWidth(0);
                    soilLayerhight.getDefaultPolygonStyle().setFillColor(Color.argb(150, 200, 0, 0));

                    soilLayermid = new GeoJsonLayer(getMap(), R.raw.liquefactionmid, getApplicationContext());
                    soilLayermid.getDefaultPolygonStyle().setStrokeWidth(0);
                    soilLayermid.getDefaultPolygonStyle().setFillColor(Color.argb(150, 255, 255, 0));

                    soilLayerlow = new GeoJsonLayer(getMap(), R.raw.liquefactionlow, getApplicationContext());
                    soilLayerlow.getDefaultPolygonStyle().setStrokeWidth(0);
                    soilLayerlow.getDefaultPolygonStyle().setFillColor(Color.argb(150, 0, 255, 0));

                    postLayer = new GeoJsonLayer(getMap(), R.raw.l003, getApplicationContext());
                    postLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("L003")));
                    hosLayer = new GeoJsonLayer(getMap(), R.raw.l004, getApplicationContext());
                    hosLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("L004")));
                    marketLayer = new GeoJsonLayer(getMap(), R.raw.l001, getApplicationContext());
                    marketLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("L001")));

                    departLayer = new GeoJsonLayer(getMap(), R.raw.l002, getApplicationContext());
                    departLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("L002")));
                    parkLayer = new GeoJsonLayer(getMap(), R.raw.e002, getApplicationContext());
                    parkLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("E002")));
                    sportLayer = new GeoJsonLayer(getMap(), R.raw.e001, getApplicationContext());
                    sportLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("E001")));

                    gasLayer = new GeoJsonLayer(getMap(), R.raw.b001, getApplicationContext());
                    gasLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("B001")));
                    fireLayer = new GeoJsonLayer(getMap(), R.raw.b002, getApplicationContext());
                    fireLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("B002")));
                    dieLayer = new GeoJsonLayer(getMap(), R.raw.b003, getApplicationContext());
                    dieLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("B003")));

                    JSLayer = new GeoJsonLayer(getMap(), R.raw.a001, getApplicationContext());
                    JSLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("A001")));
                    JCLayer = new GeoJsonLayer(getMap(), R.raw.a002, getApplicationContext());
                    JCLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("A002")));
                    JKLayer = new GeoJsonLayer(getMap(), R.raw.a003, getApplicationContext());
                    JKLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("A003")));
                    libLayer = new GeoJsonLayer(getMap(), R.raw.a004, getApplicationContext());
                    libLayer.getDefaultPointStyle().setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromMemCacheBase("A004")));

                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return false;
                }
            } else {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

        }

        @Override
        protected void onPreExecute() {

        }
    }

    //LruCache
    public void addBitmapToMemoryCacheBase(String key, Bitmap bitmap) {
        if (getBitmapFromMemCacheBase(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCacheBase(String key) {
        return mMemoryCache.get(key);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.radio1:
                if (soilLayerhight.isLayerOnMap()) {
                    soilLayerhight.removeLayerFromMap();
                    soilLayermid.removeLayerFromMap();
                    soilLayerlow.removeLayerFromMap();
                } else {
                    soilLayerhight.addLayerToMap();
                    soilLayermid.addLayerToMap();
                    soilLayerlow.addLayerToMap();
                }
                break;
            case R.id.radio2:
                if (floodLayer.isLayerOnMap()) {
                    floodLayer.removeLayerFromMap();
                } else {
                    floodLayer.addLayerToMap();
                }
                break;
            case R.id.radio3:
                if (activeLayer.isLayerOnMap()) {
                    activeLayer.removeLayerFromMap();
                } else {
                    activeLayer.addLayerToMap();
                }
                break;
            case R.id.radio4:
                //radioGroup1.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("4after" + layers.isEmpty());

                }
                postLayer.addLayerToMap();
                layers.offer(postLayer);
                break;
            case R.id.radio5:
                //radioGroup1.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("5after" + layers.isEmpty());
                }
                hosLayer.addLayerToMap();
                layers.offer(hosLayer);
                break;
            case R.id.radio6:
                //radioGroup1.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("6after" + layers.isEmpty());
                }
                marketLayer.addLayerToMap();
                layers.offer(marketLayer);
                break;
            case R.id.radio7:
                //radioGroup1.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("7after" + layers.isEmpty());
                }
                departLayer.addLayerToMap();
                layers.offer(departLayer);
                break;
            case R.id.radio8:
                radioGroup2.clearCheck();
                //radioGroup1.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("8after" + layers.isEmpty());
                }
                JSLayer.addLayerToMap();
                layers.offer(JSLayer);
                break;
            case R.id.radio9:
                radioGroup2.clearCheck();
                //radioGroup1.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("9after" + layers.isEmpty());
                }
                JCLayer.addLayerToMap();
                layers.offer(JCLayer);
                break;
            case R.id.radio10:
                radioGroup2.clearCheck();
                //radioGroup1.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("10after" + layers.isEmpty());
                }
                JKLayer.addLayerToMap();
                layers.offer(JKLayer);
                break;
            case R.id.radio11:
                radioGroup2.clearCheck();
                //radioGroup1.clearCheck();
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("11after" + layers.isEmpty());
                }
                libLayer.addLayerToMap();
                layers.offer(libLayer);
                break;
            case R.id.radio12:
                //radioGroup1.clearCheck();
                radioGroup2.clearCheck();
                radioGroup3.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("12after" + layers.isEmpty());
                }
                parkLayer.addLayerToMap();
                layers.offer(parkLayer);
                break;
            case R.id.radio13:
                //radioGroup1.clearCheck();
                radioGroup2.clearCheck();
                radioGroup3.clearCheck();
                radioGroup5.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("13after" + layers.isEmpty());
                }
                sportLayer.addLayerToMap();
                layers.offer(sportLayer);
                break;
            case R.id.radio14:
                radioGroup2.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                //radioGroup1.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("14after" + layers.isEmpty());
                }
                layers.offer(gasLayer);
                layers.peek().addLayerToMap();
                break;
            case R.id.radio15:
                radioGroup2.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                //radioGroup1.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("15after" + layers.isEmpty());
                }
                fireLayer.addLayerToMap();
                layers.offer(fireLayer);
                break;
            case R.id.radio16:
                radioGroup2.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                //radioGroup1.clearCheck();

                while (!layers.isEmpty()) {
                    layers.poll().removeLayerFromMap();
                    layers.clear();
                    System.out.println("16after" + layers.isEmpty());
                }
                dieLayer.addLayerToMap();
                layers.offer(dieLayer);
                break;

        }
        System.out.println("allafter" + layers.isEmpty());
    }
}
