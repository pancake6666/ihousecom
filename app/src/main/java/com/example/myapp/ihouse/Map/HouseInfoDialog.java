package com.example.myapp.ihouse.map;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.analysis.Activity_Analysis;
import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.RealEstateDate;

import java.text.DecimalFormat;


public class HouseInfoDialog extends DialogFragment {
    public int houseId;
    public String title;
    public String address;
    public Double lng;
    public Double lat;
    public int price;
    public Double space;
    private ProgressDialog mProgressDialog;
    public MarkerItem items;
    private GetURL getURL;
    private Hweight hweight;
    private RealEstateDate real;
    private TextView locationV;
    private TextView priceV;
    ProgressDialog progressDialog;
    AlertDialog alertDialog;
    Intent intent;
    private int m_id;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        intent = new Intent(this.getActivity(), Activity_Analysis.class);
        getURL = new GetURL();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        m_id = preferences.getInt("ID", 0);
        loadData loadData = new loadData();
        loadData.execute(new int[]{houseId, m_id});
        progressDialog = new ProgressDialog(getActivity(), getTheme());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("讀取中");
        progressDialog.show();
        return progressDialog;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSpace(Double space) {
        this.space = space;
    }


    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }


    class loadData extends AsyncTask<int[], Integer, Boolean> {

        @Override
        protected Boolean doInBackground(int[]... integers) {
            int[] x = integers[0];
            real = getURL.real_detail(x[0], x[1]);
            hweight = getURL.getHweight(x[1]);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressDialog.hide();
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            final View v = inflater.inflate(R.layout.add_dialog, null);
            locationV = (TextView) v.findViewById(R.id.location);
            priceV = (TextView) v.findViewById(R.id.price);
            locationV.setText(real.getRealAddr());
            double p = Double.parseDouble(real.getPrice());
            double pp = p / 10000;
            DecimalFormat df = new DecimalFormat("#.#");
//            int s = Integer.parseInt(real.getHouseArea());
//            int x = Integer.parseInt(real.getLandArea());
//            int sx = s + x;
            priceV.setText(df.format(pp));
            System.out.println(real.getMl_MRTScore());
            alertDialog = new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setTitle("房屋資訊")
                    .setView(v)
                    .setNegativeButton("進入分析", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Bundle realData = new Bundle();
                            realData.putSerializable("fromHou", real);
                            realData.putSerializable("hweight", hweight);
                            realData.putInt("addType", 0);
                            intent.putExtras(realData);
                            progressDialog.dismiss();
                            startActivity(intent);
                        }
                    })
                    .setPositiveButton("關閉", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            progressDialog.dismiss();
                            dismiss();
                        }
                    })
                    .create();
            alertDialog.show();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
}
