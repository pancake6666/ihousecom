package com.example.myapp.ihouse.httpGet;

import java.util.ArrayList;

public class SaleHouse implements java.io.Serializable {
    private static final long serialVersionUID = 3776236139633724963L;
    private String SellHouse_Elevator;
    private String SellHouse_Room;
    private String SellHouse_Year;
    private String SellHouse_URL;
    private String SellHouse_Address;
    private String SellHouse_Y;
    private String SellHouse_Type;
    private ArrayList<SaleHouseLs> ls;
    private String SellHouse_Lng;
    private String SellHouse_Section;
    private String SellHouse_X;
    private String SellHouse_LRoom;
    private String SellHouse_ID;
    private String SellHouse_Lat;
    private String SellHouse_Space;
    private String SellHouse_Price;
    private String SellHouse_WC;
    private String SellHouse_Car;

    public String getSellHouse_Elevator() {
        return this.SellHouse_Elevator;
    }

    public void setSellHouse_Elevator(String SellHouse_Elevator) {
        this.SellHouse_Elevator = SellHouse_Elevator;
    }

    public String getSellHouse_Room() {
        return this.SellHouse_Room;
    }

    public void setSellHouse_Room(String SellHouse_Room) {
        this.SellHouse_Room = SellHouse_Room;
    }

    public String getSellHouse_Year() {
        return this.SellHouse_Year;
    }

    public void setSellHouse_Year(String SellHouse_Year) {
        this.SellHouse_Year = SellHouse_Year;
    }

    public String getSellHouse_URL() {
        return this.SellHouse_URL;
    }

    public void setSellHouse_URL(String SellHouse_URL) {
        this.SellHouse_URL = SellHouse_URL;
    }

    public String getSellHouse_Address() {
        return this.SellHouse_Address;
    }

    public void setSellHouse_Address(String SellHouse_Address) {
        this.SellHouse_Address = SellHouse_Address;
    }

    public String getSellHouse_Y() {
        return this.SellHouse_Y;
    }

    public void setSellHouse_Y(String SellHouse_Y) {
        this.SellHouse_Y = SellHouse_Y;
    }

    public String getSellHouse_Type() {
        return this.SellHouse_Type;
    }

    public void setSellHouse_Type(String SellHouse_Type) {
        this.SellHouse_Type = SellHouse_Type;
    }


    public String getSellHouse_Lng() {
        return this.SellHouse_Lng;
    }

    public void setSellHouse_Lng(String SellHouse_Lng) {
        this.SellHouse_Lng = SellHouse_Lng;
    }

    public String getSellHouse_Section() {
        return this.SellHouse_Section;
    }

    public void setSellHouse_Section(String SellHouse_Section) {
        this.SellHouse_Section = SellHouse_Section;
    }

    public String getSellHouse_X() {
        return this.SellHouse_X;
    }

    public void setSellHouse_X(String SellHouse_X) {
        this.SellHouse_X = SellHouse_X;
    }

    public String getSellHouse_LRoom() {
        return this.SellHouse_LRoom;
    }

    public void setSellHouse_LRoom(String SellHouse_LRoom) {
        this.SellHouse_LRoom = SellHouse_LRoom;
    }

    public String getSellHouse_ID() {
        return this.SellHouse_ID;
    }

    public void setSellHouse_ID(String SellHouse_ID) {
        this.SellHouse_ID = SellHouse_ID;
    }

    public String getSellHouse_Lat() {
        return this.SellHouse_Lat;
    }

    public void setSellHouse_Lat(String SellHouse_Lat) {
        this.SellHouse_Lat = SellHouse_Lat;
    }

    public String getSellHouse_Space() {
        return this.SellHouse_Space;
    }

    public void setSellHouse_Space(String SellHouse_Space) {
        this.SellHouse_Space = SellHouse_Space;
    }

    public String getSellHouse_Price() {
        return this.SellHouse_Price;
    }

    public void setSellHouse_Price(String SellHouse_Price) {
        this.SellHouse_Price = SellHouse_Price;
    }

    public String getSellHouse_WC() {
        return this.SellHouse_WC;
    }

    public void setSellHouse_WC(String SellHouse_WC) {
        this.SellHouse_WC = SellHouse_WC;
    }

    public String getSellHouse_Car() {
        return this.SellHouse_Car;
    }

    public void setSellHouse_Car(String SellHouse_Car) {
        this.SellHouse_Car = SellHouse_Car;
    }

    public ArrayList<SaleHouseLs> getLs() {
        return ls;
    }

    public void setLs(ArrayList<SaleHouseLs> ls) {
        this.ls = ls;
    }
}
