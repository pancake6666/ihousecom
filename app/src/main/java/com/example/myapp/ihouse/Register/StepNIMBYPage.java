package com.example.myapp.ihouse.register;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

/**
 * Created by changyuen on 2016/9/4.
 */
public class StepNIMBYPage extends Page {
    public static String NEAR_DIS_MIN = "near_dis_min";
    public static String NEAR_DIS_MAX = "near_dis_max";
    public static String NEAR_DIS_MIN_VAL = "near_dis_min_v";
    public static String NEAR_DIS_MAX_VAL = "near_dis_max_v";

    public static String GAS_STAR = "gas_star";
    public static String FIRE_STAR = "fire_star";
    public static String DIE_STAR = "die_star";
    public static String AIR_STAR = "air_star";

    public StepNIMBYPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return NIMBYFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        setHateData();
        dest.add(new ReviewItem("項目", "鄰避設施", getKey(), -1));
        dest.add(new ReviewItem("", String.format("距離:%d~%d 權重: 加油站%d,焚化爐:%d,殯儀館:%d,機場:%d",
                mData.getInt(NEAR_DIS_MIN), mData.getInt(NEAR_DIS_MAX),
                mData.getInt(GAS_STAR), mData.getInt(FIRE_STAR),
                mData.getInt(DIE_STAR), mData.getInt(AIR_STAR)), getKey(), -1));
    }


    @Override
    public boolean isCompleted() {
        return true;
    }

    public void setHateData() {
//        hateData = new ArrayList<>();
//        hateData.add(mData.getInt(NEAR_DIS_MIN));
//        hateData.add(mData.getInt(NEAR_DIS_MAX));
//        hateData.add(mData.getInt(AIR_STAR));
//        hateData.add(mData.getInt(GAS_STAR));
//        hateData.add(mData.getInt(FIRE_STAR));
//        hateData.add(mData.getInt(DIE_STAR));
//        return hateData;

        StepActivity.hweight.setHTmin(mData.getInt(NEAR_DIS_MIN));
        StepActivity.hweight.setHTmax(mData.getInt(NEAR_DIS_MAX));
        StepActivity.hweight.setHTAS(mData.getInt(AIR_STAR));
        StepActivity.hweight.setHTGS(mData.getInt(GAS_STAR));
        StepActivity.hweight.setHTIS(mData.getInt(FIRE_STAR));
        StepActivity.hweight.setHTMS(mData.getInt(DIE_STAR));

    }
}
