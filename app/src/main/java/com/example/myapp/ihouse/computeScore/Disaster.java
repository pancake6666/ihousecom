/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.myapp.ihouse.computeScore;

/**
 * @author changyuen
 */
public class Disaster {

    private int at_liq;
    private int at_actvie;
    private int at_flood;

    public Disaster() {
    }

    public Disaster(int at_liq, int at_actvie, int at_flood) {
        this.at_liq = at_liq;
        this.at_actvie = at_actvie;
        this.at_flood = at_flood;
    }

    public int getAt_liq() {
        return at_liq;
    }

    public void setAt_liq(int at_liq) {
        this.at_liq = at_liq;
    }

    public int getAt_actvie() {
        return at_actvie;
    }

    public void setAt_actvie(int at_actvie) {
        this.at_actvie = at_actvie;
    }

    public int getAt_flood() {
        return at_flood;
    }

    public void setAt_flood(int at_flood) {
        this.at_flood = at_flood;
    }


}
