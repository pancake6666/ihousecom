package com.example.myapp.ihouse.analysis;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.httpGet.MyData;
import com.example.myapp.ihouse.httpGet.MyLoveSample;
import com.example.myapp.ihouse.httpGet.Part2;
import com.example.myapp.ihouse.httpGet.RealEstateDate;

import java.util.ArrayList;


public class Analysis_LifeTotalFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    private ExpandableListView expandableListView;
    private ImageButton alertdlgs;
    public static RealEstateDate realEstateDate;
    public static MyData myData;
    public static Part2 part2;
    public static MyData bySample;
    public static RealHouse realHouse;
    public static MyLoveSample myLoveSample;


    public static Analysis_LifeTotalFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        Analysis_LifeTotalFragment fragment = new Analysis_LifeTotalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.analysis_lifetotal, container, false);
        TextView address = (TextView) view.findViewById(R.id.address);
        TextView totalScore = (TextView) view.findViewById(R.id.lifetotalpoints);
        if (Activity_Analysis.realEstateDate != null) {
            address.setText(realEstateDate.getRealAddr());
            if (realEstateDate.getRealAddr().length() > 11) {
                address.setTextSize(30f);
            }
            int lifeScore = (int) Double.parseDouble(realEstateDate.getMl_LifeScore());
            totalScore.setText(String.valueOf(lifeScore));
        } else if (Activity_Analysis.myData != null) {
            address.setText(myData.getRealAddr());
            if (myData.getRealAddr().length() > 11) {
                address.setTextSize(30f);
            }
            int lifeScore = (int) Double.parseDouble(myData.getMl_LifeScore());
            totalScore.setText(String.valueOf(lifeScore));
        } else if (Activity_Analysis.myLoveSample != null) {
            address.setText(myLoveSample.getMl_address());
            if (myLoveSample.getMl_address().length() > 11) {
                address.setTextSize(30f);
            }
            int lifeScore = (int) Double.parseDouble(myLoveSample.getMl_LifeScore());
            totalScore.setText(String.valueOf(lifeScore));
        }

        expandableListView = (ExpandableListView) view.findViewById(R.id.lifetotalexpd);
        setGroup();

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandableListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        return view;
    }

    public void setGroup() {
        ArrayList<String> groupName = new ArrayList<String>();
        groupName.clear();
        groupName.add("交通");
        groupName.add("民生");
        groupName.add("教育");
        groupName.add("休閒");
        groupName.add("鄰避設施");
        ArrayList<ArrayList<String>> groups = new ArrayList<ArrayList<String>>();
        ArrayList<String> children1 = new ArrayList<String>();
        ArrayList<String> children2 = new ArrayList<String>();
        ArrayList<String> children3 = new ArrayList<String>();
        ArrayList<String> children4 = new ArrayList<String>();
        ArrayList<String> children5 = new ArrayList<String>();


        children1.add("捷運");
        children1.add("火車");
        children1.add("高鐵");
        children1.add("高速公路");
        groups.add(children1);

        children2.add("郵局");
        children2.add("醫院");
        children2.add("大賣場");
        children2.add("百貨公司");
        groups.add(children2);

        children3.add("國小");
        children3.add("國中");
        children3.add("高中");
        children3.add("圖書館");
        groups.add(children3);

        children4.add("公園");
        children4.add("運動中心");
        groups.add(children4);

        children5.add("加油站");
        children5.add("機場");
        children5.add("焚化爐");
        children5.add("殯儀館");
        groups.add(children5);

        LifeTotal_ExpListAdapter adapter = new LifeTotal_ExpListAdapter(getContext(), groups, groupName);
        expandableListView.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getButtonView();
        setButtonEvent();

//        toastButton2 = (FloatingActionButton) this.getView().findViewById(R.id.fab2);
//        //設定按鈕的ClickListener
//        toastButton2.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //當使用者按下按鈕時顯示Toast
//                //Toast.LENGTH_LONG表示顯示時間較長，Toast.LENGTH_SHORT則表示顯示時間較短
//                Toast.makeText(view.getContext(), "成功加入最愛", Toast.LENGTH_LONG).show();
//            }
//        });
    }

    public void setButtonEvent() {
        alertdlgs.setOnClickListener(buttonListener);
    }

    public void getButtonView() {
        alertdlgs = (ImageButton) this.getView().findViewById(R.id.fab2);
    }

    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.fab2:
                    new AlertDialog.Builder(getActivity())
                            .setTitle("小提醒")
                            .setMessage(R.string.info)
                            .setPositiveButton("確定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                    break;
            }
        }
    };


}
