package com.example.myapp.ihouse.analysis;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.MyData;
import com.example.myapp.ihouse.httpGet.Part2;
import com.example.myapp.ihouse.httpGet.RealEstateDate;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Score_ExpListAdapter extends BaseExpandableListAdapter {

    private ArrayList<ArrayList<String>> mGroups;
    private Context mContext;
    private ArrayList<String> mGroupsName;
    private ExpandableListView drawerList;
    public static Hweight hweight;
    public static Part2 part2;
    public static MyData myData;
    public static RealHouse realHouse;
    public static RealEstateDate realEstateDate;
    Calendar cal = Calendar.getInstance();


    public Score_ExpListAdapter(Context context, ArrayList<ArrayList<String>> groups, ArrayList<String> mGroupsName) {
        mContext = context;
        mGroups = groups;
        this.mGroupsName = mGroupsName;
    }


    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mGroups.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mGroups.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group, null);
        }

        if (isExpanded) {

        } else {
        }
        TextView textGroup = (TextView) convertView.findViewById(R.id.textGroup);
        textGroup.setText(mGroupsName.get(groupPosition));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.score_child_view, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.scorename1);
        TextView textscore2 = (TextView) convertView.findViewById(R.id.scorename2);
        TextView textiscore2 = (TextView) convertView.findViewById(R.id.idealscore2);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.evaluation);
        textView.setText(mGroups.get(groupPosition).get(childPosition));
        ////實價
        if (Activity_Analysis.realEstateDate != null) {
            if (childPosition == 0) {
//                textscore2.setText(realEstateDate.getHouseType());
                switch (Integer.parseInt(realEstateDate.getHouseType())) {
                    case 1:
                        textscore2.setText("公寓");
                        break;
                    case 2:
                        textscore2.setText("住宅大樓");
                        break;
                    case 4:
                        textscore2.setText("套房");
                        break;
                    case 5:
                        textscore2.setText("透天厝");
                        break;
                    case 6:
                        textscore2.setText("華廈");
                        break;
                    case -1:
                        textscore2.setText("不拘");
                        break;
                    default:
                        textscore2.setText("其他");
                }
                switch (hweight.getType()) {
                    case 1:
                        textiscore2.setText("公寓");
                        break;
                    case 2:
                        textiscore2.setText("住宅大樓");
                        break;
                    case 4:
                        textiscore2.setText("套房");
                        break;
                    case 5:
                        textiscore2.setText("透天厝");
                        break;
                    case 6:
                        textiscore2.setText("華廈");
                        break;
                    case -1:
                        textiscore2.setText("不拘");
                        break;
                }
                if (Integer.parseInt(realEstateDate.getHouseType()) != hweight.getType() && hweight.getType() != -1) {
                    imageView.setImageResource(R.drawable.close);
                } else if (hweight.getType() == -1) {
                    imageView.setImageResource(R.drawable.correct);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 1) {
                textscore2.setText(realEstateDate.getHouseArea());
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getSpacemin()), String.valueOf(hweight.getSpacemax())));
                if (Double.parseDouble(realEstateDate.getHouseArea()) > hweight.getSpacemax() || Double.parseDouble(realEstateDate.getHouseArea()) < hweight.getSpacemin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 2) {
                double price = Double.parseDouble(realEstateDate.getPrice()) / 10000;
                DecimalFormat df = new DecimalFormat("#.#");
                textscore2.setText(df.format(price));
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getPricemin()), String.valueOf(hweight.getPricemax())));
                if (price > hweight.getPricemax() || price < hweight.getPricemin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 3) {
                DateFormat formatter = new SimpleDateFormat("yyyy");
                Date date = new Date(realEstateDate.getHouseDate());
                int old = Integer.parseInt(formatter.format(date));
                int now = cal.get(Calendar.YEAR);
                System.out.println(now);
                int year = now - old;
                textscore2.setText(String.valueOf(year));
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getYearmin()), String.valueOf(hweight.getYearmax())));
                if (year > hweight.getYearmax() || year < hweight.getYearmin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 4) {
                textscore2.setText(String.format("房:%s 廳:%s 衛:%s", realEstateDate.getHouseRoom_1(), realEstateDate.getHouseRoom_2(), realEstateDate.getHouseRoom_3()));
                if (hweight.getRoom() == -1 && hweight.getLRoom() == -1 && hweight.getWC() == -1) {
                    textiscore2.setText("不拘");
                    imageView.setImageResource(R.drawable.correct);
                } else if (Integer.parseInt(realEstateDate.getHouseRoom_1()) != hweight.getRoom() ||
                        Integer.parseInt(realEstateDate.getHouseRoom_2()) != hweight.getLRoom() ||
                        Integer.parseInt(realEstateDate.getHouseRoom_3()) != hweight.getWC()) {
                    textiscore2.setText(String.format("房:%s 廳:%s 衛:%s", String.valueOf(hweight.getRoom()), String.valueOf(hweight.getLRoom()), String.valueOf(hweight.getWC())));
                    imageView.setImageResource(R.drawable.close);
                } else {
                    textiscore2.setText(String.format("房:%s 廳:%s 衛:%s", String.valueOf(hweight.getRoom()), String.valueOf(hweight.getLRoom()), String.valueOf(hweight.getWC())));

                }
            }
            if (childPosition == 5) {
                if (realEstateDate.getHouseType().equals("2") || realEstateDate.getHouseType().equals("6")) {
                    textscore2.setText("有");
                } else {
                    textscore2.setText("無");
                }

                if (hweight.getElevator() == -1) {
                    textiscore2.setText("不拘");
                } else if (hweight.getElevator() == 0) {
                    textiscore2.setText("無");
                } else if (hweight.getElevator() == 1) {
                    textiscore2.setText("有");
                }

                if ((hweight.getElevator() != -1)) {
                    if (!textscore2.getText().equals(textiscore2.getText())) {
                        imageView.setImageResource(R.drawable.close);
                    } else {
                        imageView.setImageResource(R.drawable.correct);
                    }
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 6) {
                if (realEstateDate.getParkingArea().equals("0")) {
                    textscore2.setText("無");
                } else {
                    textscore2.setText("有");
                }

                if (hweight.getCar() == -1) {
                    textiscore2.setText("不拘");
                } else if (hweight.getCar() == 0) {
                    textiscore2.setText("無");
                } else if (hweight.getCar() == 1) {
                    textiscore2.setText("有");
                }

                if ((hweight.getCar() != -1)) {
//                    (hweight.getCar() != Integer.parseInt(realEstateDate.getParkingArea()))
                    if (!textscore2.getText().equals(textiscore2.getText())) {
                        imageView.setImageResource(R.drawable.close);
                    } else {
                        imageView.setImageResource(R.drawable.correct);
                    }
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            //////自加
        } else if (Activity_Analysis.myData != null) {
            if (childPosition == 0) {
                switch (Integer.parseInt(myData.getHouseType())) {
                    case 1:
                        textscore2.setText("公寓");
                        break;
                    case 2:
                        textscore2.setText("住宅大樓");
                        break;
                    case 4:
                        textscore2.setText("套房");
                        break;
                    case 5:
                        textscore2.setText("透天厝");
                        break;
                    case 6:
                        textscore2.setText("華廈");
                        break;
                    case -1:
                        textscore2.setText("不拘");
                        break;
                }
                switch (hweight.getType()) {
                    case 1:
                        textiscore2.setText("公寓");
                        break;
                    case 2:
                        textiscore2.setText("住宅大樓");
                        break;
                    case 4:
                        textiscore2.setText("套房");
                        break;
                    case 5:
                        textiscore2.setText("透天厝");
                        break;
                    case 6:
                        textiscore2.setText("華廈");
                        break;
                    case -1:
                        textiscore2.setText("不拘");
                        break;
                }
                if (Integer.parseInt(myData.getHouseType()) != hweight.getType() && hweight.getType() != -1) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 1) {
                textscore2.setText(myData.getHouseArea().toString());
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getSpacemin()), String.valueOf(hweight.getSpacemax())));
                if (Double.parseDouble(myData.getHouseArea().toString()) > hweight.getSpacemax() || Double.parseDouble(myData.getHouseArea().toString()) < hweight.getSpacemin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 2) {
                double price = Double.parseDouble(myData.getPrice()) / 10000;
                DecimalFormat df = new DecimalFormat("#.#");
                textscore2.setText(df.format(price));
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getPricemin()), String.valueOf(hweight.getPricemax())));
                if (price > hweight.getPricemax() || price < hweight.getPricemin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 3) {
//                DateFormat formatter = new SimpleDateFormat("yyyy");
//                Date date = new Date(myData.getHouseDate());
//                int old = Integer.parseInt(formatter.format(date));
//                int now = cal.get(Calendar.YEAR);
//                System.out.println(now);
//                int year = now - old;
                textscore2.setText(String.valueOf(myData.getHouseDate()));
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getYearmin()), String.valueOf(hweight.getYearmax())));
                if (Integer.parseInt(myData.getHouseDate()) > hweight.getYearmax() || Integer.parseInt(myData.getHouseDate()) < hweight.getYearmin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 4) {
                textscore2.setText(String.format("房:%s 廳:%s 衛:%s", myData.getHouseRoom_1(), myData.getHouseRoom_2(), myData.getHouseRoom_3()));
                if (hweight.getRoom() == -1 && hweight.getLRoom() == -1 && hweight.getWC() == -1) {
                    textiscore2.setText("不拘");
                    imageView.setImageResource(R.drawable.correct);
                } else if (Integer.parseInt(myData.getHouseRoom_1()) != hweight.getRoom() ||
                        Integer.parseInt(myData.getHouseRoom_2()) != hweight.getLRoom() ||
                        Integer.parseInt(myData.getHouseRoom_3()) != hweight.getWC()) {
                    textiscore2.setText(String.format("房:%s 廳:%s 衛:%s", String.valueOf(hweight.getRoom()), String.valueOf(hweight.getLRoom()), String.valueOf(hweight.getWC())));
                    imageView.setImageResource(R.drawable.close);
                } else {
                    textiscore2.setText(String.format("房:%s 廳:%s 衛:%s", String.valueOf(hweight.getRoom()), String.valueOf(hweight.getLRoom()), String.valueOf(hweight.getWC())));
                }
            }
            if (childPosition == 5) {
                if (myData.getHouseType().equals("2") || myData.getHouseType().equals("6")) {
                    textscore2.setText("有");
                } else {
                    textscore2.setText("無");
                }

                if (hweight.getElevator() == -1) {
                    textiscore2.setText("不拘");
                } else if (hweight.getElevator() == 0) {
                    textiscore2.setText("無");
                } else if (hweight.getElevator() == 1) {
                    textiscore2.setText("有");
                }

                if ((hweight.getElevator() != -1)) {
                    if ((hweight.getType() != Integer.parseInt(myData.getHouseType()))) {
                        imageView.setImageResource(R.drawable.close);
                    }
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 6) {
                if (realHouse.getCar() == 0) {
                    textscore2.setText("無");
                } else {
                    textscore2.setText("有");
                }

                if (hweight.getCar() == -1) {
                    textiscore2.setText("不拘");
                } else if (hweight.getCar() == 0) {
                    textiscore2.setText("無");
                } else if (hweight.getCar() == 1) {
                    textiscore2.setText("有");
                }

                if ((hweight.getCar() != -1)) {
                    if ((hweight.getCar() != realHouse.getCar())) {
                        imageView.setImageResource(R.drawable.close);
                    }
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            /////////我的最愛
        } else if (Activity_Analysis.myLoveSample != null) {
            if (childPosition == 0) {
//                textscore2.setText(part2.getType());
                switch (Integer.parseInt(part2.getType())) {
                    case 1:
                        textscore2.setText("公寓");
                        break;
                    case 2:
                        textscore2.setText("住宅大樓");
                        break;
                    case 4:
                        textscore2.setText("套房");
                        break;
                    case 5:
                        textscore2.setText("透天厝");
                        break;
                    case 6:
                        textscore2.setText("華廈");
                        break;
                    case -1:
                        textscore2.setText("不拘");
                        break;
                }
                switch (hweight.getType()) {
                    case 1:
                        textiscore2.setText("公寓");
                        break;
                    case 2:
                        textiscore2.setText("住宅大樓");
                        break;
                    case 4:
                        textiscore2.setText("套房");
                        break;
                    case 5:
                        textiscore2.setText("透天厝");
                        break;
                    case 6:
                        textiscore2.setText("華廈");
                        break;
                    case -1:
                        textiscore2.setText("不拘");
                        break;
                }
                if (Integer.parseInt(part2.getType()) != hweight.getType() && hweight.getType() != -1) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 1) {
                textscore2.setText(part2.getSpace());
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getSpacemin()), String.valueOf(hweight.getSpacemax())));
                if (Double.parseDouble(part2.getSpace()) > hweight.getSpacemax() || Double.parseDouble(part2.getSpace()) < hweight.getSpacemin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 2) {
                double price = Double.parseDouble(part2.getPrice()) / 10000;
                DecimalFormat df = new DecimalFormat("#.#");
                textscore2.setText(df.format(price));
                textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getPricemin()), String.valueOf(hweight.getPricemax())));
                if (price > hweight.getPricemax() || price < hweight.getPricemin()) {
                    imageView.setImageResource(R.drawable.close);
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 3) {
                try {
                    DateFormat formatter = new SimpleDateFormat("yyyy");
                    Date date = new Date(part2.getYear());
                    int old = Integer.parseInt(formatter.format(date));
                    int now = cal.get(Calendar.YEAR);
                    System.out.println(now);
                    int year = now - old;
                    textscore2.setText(String.valueOf(year));
                    textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getYearmin()), String.valueOf(hweight.getYearmax())));
                    if (year > hweight.getYearmax() || year < hweight.getYearmin()) {
                        imageView.setImageResource(R.drawable.close);
                    } else {
                        imageView.setImageResource(R.drawable.correct);
                    }
                } catch (IllegalArgumentException e) {
                    textscore2.setText(part2.getYear());
                    textiscore2.setText(String.format("%s~%s", String.valueOf(hweight.getYearmin()), String.valueOf(hweight.getYearmax())));
                    if (Integer.parseInt(part2.getYear()) > hweight.getYearmax() || Integer.parseInt(part2.getYear()) < hweight.getYearmin()) {
                        imageView.setImageResource(R.drawable.close);
                    } else {
                        imageView.setImageResource(R.drawable.correct);
                    }
                }
            }
            if (childPosition == 4) {
                textscore2.setText(String.format("房:%s 廳:%s 衛:%s", part2.getRoom(), part2.getLRoom(), part2.getWC()));
                if (hweight.getRoom() == -1 && hweight.getLRoom() == -1 && hweight.getWC() == -1) {
                    textiscore2.setText("不拘");
                    imageView.setImageResource(R.drawable.correct);
                } else if (Integer.parseInt(part2.getRoom()) != hweight.getRoom() ||
                        Integer.parseInt(part2.getLRoom()) != hweight.getLRoom() ||
                        Integer.parseInt(part2.getWC()) != hweight.getWC()) {
                    textiscore2.setText(String.format("房:%s 廳:%s 衛:%s", String.valueOf(hweight.getRoom()), String.valueOf(hweight.getLRoom()), String.valueOf(hweight.getWC())));
                    imageView.setImageResource(R.drawable.close);
                } else {
                    textiscore2.setText(String.format("房:%s 廳:%s 衛:%s", String.valueOf(hweight.getRoom()), String.valueOf(hweight.getLRoom()), String.valueOf(hweight.getWC())));
                }
            }
            if (childPosition == 5) {
                if (part2.getElevator().equals("1")) {
                    textscore2.setText("有");
                } else {
                    textscore2.setText("無");
                }

                if (hweight.getElevator() == -1) {
                    textiscore2.setText("不拘");
                } else if (hweight.getElevator() == 0) {
                    textiscore2.setText("無");
                } else if (hweight.getElevator() == 1) {
                    textiscore2.setText("有");
                }

                if ((hweight.getElevator() != -1)) {
                    if ((hweight.getElevator() != Integer.parseInt(part2.getElevator()))) {
                        imageView.setImageResource(R.drawable.close);
                    }
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
            if (childPosition == 6) {
                if (part2.getCar().equals("0")) {
                    textscore2.setText("無");
                } else {
                    textscore2.setText("有");
                }

                if (hweight.getCar() == -1) {
                    textiscore2.setText("不拘");
                } else if (hweight.getCar() == 0) {
                    textiscore2.setText("無");
                } else if (hweight.getCar() == 1) {
                    textiscore2.setText("有");
                }

                if ((hweight.getCar() != -1)) {
                    if (hweight.getCar() != Integer.parseInt(part2.getCar())) {
                        imageView.setImageResource(R.drawable.close);
                    }
                } else {
                    imageView.setImageResource(R.drawable.correct);
                }
            }
        }

        textView.setTextColor(Color.BLACK);
        return convertView;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}