package com.example.myapp.ihouse.register;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;


public class StepTrafficPage extends Page {
    public static String MRT_DATA_KEY = "mrt_star";
    public static String MRT_MIN = "mrt_min";
    public static String MRT_MAX = "mrt_max";
    public static String MRT_MIN_VAL = "mrt_min_v";
    public static String MRT_MAX_VAL = "mrt_max_v";
    public static String TRAIN_DATA_KEY = "train_star";
    public static String TRAIN_MIN = "train_min";
    public static String TRAIN_MAX = "train_max";
    public static String TRAIN_MIN_VAL = "train_min_v";
    public static String TRAIN_MAX_VAL = "train_max_v";
    public static String THSR_DATA_KEY = "thsr_star";
    public static String THSR_MIN = "thsr_min";
    public static String THSR_MAX = "thsr_max";
    public static String THSR_MIN_VAL = "thsr_min_v";
    public static String THSR_MAX_VAL = "thsr_max_v";
    public static String HIGH_DATA_KEY = "high_star";
    public static String HIGH_MIN = "high_min";
    public static String HIGH_MAX = "high_max";
    public static String HIGH_MIN_VAL = "high_min_v";
    public static String HIGH_MAX_VAL = "high_max_v";

    public StepTrafficPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return TrafficFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        getTraffic();
        dest.add(new ReviewItem("項目", "交通", getKey(), -1));
        dest.add(new ReviewItem("捷運", String.format("權重:%d , 距離:%d~%d", mData.getInt(MRT_DATA_KEY), mData.getInt(MRT_MIN), mData.getInt(MRT_MAX)), getKey(), -1));
        dest.add(new ReviewItem("火車", String.format("權重:%d , 距離:%d~%d", mData.getInt(TRAIN_DATA_KEY), mData.getInt(TRAIN_MIN), mData.getInt(TRAIN_MAX)), getKey(), -1));
        dest.add(new ReviewItem("高鐵", String.format("權重:%d , 距離:%d~%d", mData.getInt(THSR_DATA_KEY), mData.getInt(THSR_MIN), mData.getInt(THSR_MAX)), getKey(), -1));
        dest.add(new ReviewItem("高速公路", String.format("權重:%d , 距離:%d~%d", mData.getInt(HIGH_DATA_KEY), mData.getInt(HIGH_MIN), mData.getInt(HIGH_MAX)), getKey(), -1));

    }


    @Override
    public boolean isCompleted() {
        return mData.getInt(StepTrafficPage.MRT_DATA_KEY) != 0 &&
                mData.getInt(StepTrafficPage.TRAIN_DATA_KEY) != 0 &&
                mData.getInt(StepTrafficPage.THSR_DATA_KEY) != 0 &&
                mData.getInt(StepTrafficPage.HIGH_DATA_KEY) != 0 &&
                mData.getInt(StepTrafficPage.MRT_MAX) > mData.getInt(StepTrafficPage.MRT_MIN) &&
                mData.getInt(StepTrafficPage.TRAIN_MAX) > mData.getInt(StepTrafficPage.TRAIN_MIN) &&
                mData.getInt(StepTrafficPage.THSR_MAX) > mData.getInt(StepTrafficPage.THSR_MIN) &&
                mData.getInt(StepTrafficPage.HIGH_MAX) > mData.getInt(StepTrafficPage.HIGH_MIN);
    }

    public void getTraffic() {
//        traffic = new ArrayList<>();
//        traffic.add(mData.getInt(StepTrafficPage.MRT_DATA_KEY));
//        traffic.add(mData.getInt(StepTrafficPage.MRT_MIN));
//        traffic.add(mData.getInt(StepTrafficPage.MRT_MAX));
//        traffic.add(mData.getInt(StepTrafficPage.TRAIN_DATA_KEY));
//        traffic.add(mData.getInt(StepTrafficPage.TRAIN_MIN));
//        traffic.add(mData.getInt(StepTrafficPage.TRAIN_MAX));
//        traffic.add(mData.getInt(StepTrafficPage.THSR_DATA_KEY));
//        traffic.add(mData.getInt(StepTrafficPage.THSR_MIN));
//        traffic.add(mData.getInt(StepTrafficPage.THSR_MAX));
//        traffic.add(mData.getInt(StepTrafficPage.HIGH_DATA_KEY));
//        traffic.add(mData.getInt(StepTrafficPage.HIGH_MIN));
//        traffic.add(mData.getInt(StepTrafficPage.HIGH_MAX));
//        return traffic;
        StepActivity.hweight.setMRTstar(mData.getInt(StepTrafficPage.MRT_DATA_KEY));
        StepActivity.hweight.setMRTmin(mData.getInt(StepTrafficPage.MRT_MIN));
        StepActivity.hweight.setMRTmax(mData.getInt(StepTrafficPage.MRT_MAX));
        StepActivity.hweight.setTRAstar(mData.getInt(StepTrafficPage.TRAIN_DATA_KEY));
        StepActivity.hweight.setTRAmin(mData.getInt(StepTrafficPage.TRAIN_MIN));
        StepActivity.hweight.setTRAmax(mData.getInt(StepTrafficPage.TRAIN_MAX));
        StepActivity.hweight.setTHSRstar(mData.getInt(StepTrafficPage.THSR_DATA_KEY));
        StepActivity.hweight.setTHSRmin(mData.getInt(StepTrafficPage.THSR_MIN));
        StepActivity.hweight.setTHSRmax(mData.getInt(StepTrafficPage.THSR_MAX));
        StepActivity.hweight.setFwaystar(mData.getInt(StepTrafficPage.HIGH_DATA_KEY));
        StepActivity.hweight.setFwaymin(mData.getInt(StepTrafficPage.HIGH_MIN));
        StepActivity.hweight.setFwaymax(mData.getInt(StepTrafficPage.HIGH_MAX));
    }
}
