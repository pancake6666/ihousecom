package com.example.myapp.ihouse.register;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RatingBar;

import com.example.myapp.ihouse.R;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.util.ArrayList;


public class FunctionFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    NumberPicker np1;
    NumberPicker np2;
    NumberPicker np3;
    NumberPicker np4;
    NumberPicker np5;
    NumberPicker np6;
    private StepFunctionPage mPage;
    protected PageFragmentCallbacks mCallbacks;
    private String mKey;


    public static FunctionFragment newInstance(String pageNo) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, pageNo);
        FunctionFragment fragment = new FunctionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_PAGE);
        mPage = (StepFunctionPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.functionfragment1, container, false);


        integers = new ArrayList<>();
        integers.add(0, -1);
        integers.add(1, -1);
        integers.add(2, -1);
        integers.add(3, -1);
        integers.add(4, -1);
        integers.add(5, -1);
        integers.add(6, -1);
        integers.add(7, -1);
        integers.add(8, -1);
        integers.add(9, -1);
        integers.add(10, -1);
        integers.add(11, -1);

        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.RatingBarId);
        final RatingBar ratingBar2 = (RatingBar) view.findViewById(R.id.RatingBarId2);
        final RatingBar ratingBar3 = (RatingBar) view.findViewById(R.id.RatingBarId3);
        final RatingBar ratingBar4 = (RatingBar) view.findViewById(R.id.RatingBarId4);
        final RatingBar ratingBar5 = (RatingBar) view.findViewById(R.id.RatingBarId5);
        final RatingBar ratingBar6 = (RatingBar) view.findViewById(R.id.RatingBarId6);
        final RatingBar ratingBar7 = (RatingBar) view.findViewById(R.id.RatingBarId7);
        final RatingBar ratingBar8 = (RatingBar) view.findViewById(R.id.RatingBarId8);
        final RatingBar ratingBar9 = (RatingBar) view.findViewById(R.id.RatingBarId9);
        final RatingBar ratingBar10 = (RatingBar) view.findViewById(R.id.RatingBarId10);

        ratingBar.setRating(mPage.getData().getInt(StepFunctionPage.POST_STAR));
        ratingBar2.setRating(mPage.getData().getInt(StepFunctionPage.HOS_STAR));
        ratingBar3.setRating(mPage.getData().getInt(StepFunctionPage.MARKET_STAR));
        ratingBar4.setRating(mPage.getData().getInt(StepFunctionPage.DEPART_STAR));
        ratingBar5.setRating(mPage.getData().getInt(StepFunctionPage.JS_STAR));
        ratingBar6.setRating(mPage.getData().getInt(StepFunctionPage.JC_STAR));
        ratingBar7.setRating(mPage.getData().getInt(StepFunctionPage.JK_STAR));
        ratingBar8.setRating(mPage.getData().getInt(StepFunctionPage.LIB_STAR));
        ratingBar9.setRating(mPage.getData().getInt(StepFunctionPage.PARK_STAR));
        ratingBar10.setRating(mPage.getData().getInt(StepFunctionPage.SPORT_STAR));

        np1 = (NumberPicker) view.findViewById(R.id.np1);
        np2 = (NumberPicker) view.findViewById(R.id.np2);
        np3 = (NumberPicker) view.findViewById(R.id.np3);
        np4 = (NumberPicker) view.findViewById(R.id.np4);
        np5 = (NumberPicker) view.findViewById(R.id.np5);
        np6 = (NumberPicker) view.findViewById(R.id.np6);

        final String[] values = new String[]{"0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800", "1900", "2000"};

        np1.setMaxValue(values.length - 1);
        np1.setDisplayedValues(values);
        np1.setMinValue(0);
        np1.setValue(mPage.getData().getInt(StepFunctionPage.FUNC_DIS_MIN_VAL));

        np2.setMaxValue(values.length - 1);
        np2.setMinValue(0);
        np2.setDisplayedValues(values);
        np2.setValue(mPage.getData().getInt(StepFunctionPage.FUNC_DIS_MAX_VAL));

        np3.setMaxValue(values.length - 1);
        np3.setMinValue(0);
        np3.setDisplayedValues(values);
        np3.setValue(mPage.getData().getInt(StepFunctionPage.EDU_DIS_MIN_VAL));

        np4.setMaxValue(values.length - 1);
        np4.setMinValue(0);
        np4.setDisplayedValues(values);
        np4.setValue(mPage.getData().getInt(StepFunctionPage.EDU_DIS_MAX_VAL));

        np5.setMaxValue(values.length - 1);
        np5.setMinValue(0);
        np5.setDisplayedValues(values);
        np5.setValue(mPage.getData().getInt(StepFunctionPage.LEI_DIS_MIN_VAL));

        np6.setMaxValue(values.length - 1);
        np6.setMinValue(0);
        np6.setDisplayedValues(values);
        np6.setValue(mPage.getData().getInt(StepFunctionPage.LEI_DIS_MAX_VAL));


        np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepFunctionPage.FUNC_DIS_MIN, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepFunctionPage.FUNC_DIS_MIN_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });

        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepFunctionPage.FUNC_DIS_MAX, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepFunctionPage.FUNC_DIS_MAX_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });
        np3.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepFunctionPage.EDU_DIS_MIN, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepFunctionPage.EDU_DIS_MIN_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });
        np4.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepFunctionPage.EDU_DIS_MAX, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepFunctionPage.EDU_DIS_MAX_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });
        np5.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepFunctionPage.LEI_DIS_MIN, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepFunctionPage.LEI_DIS_MIN_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });
        np6.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepFunctionPage.LEI_DIS_MAX, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepFunctionPage.LEI_DIS_MAX_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.POST_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });

        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.HOS_STAR, (int) rating);
                mPage.notifyDataChanged();
            }
        });

        ratingBar3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.MARKET_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });

        ratingBar4.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.DEPART_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });
        ratingBar5.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                mPage.getData().putInt(StepFunctionPage.JS_STAR, (int) rating);
                mPage.notifyDataChanged();
            }
        });
        ratingBar6.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.JC_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });
        ratingBar7.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.JK_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });
        ratingBar8.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.LIB_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });
        ratingBar9.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.PARK_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });
        ratingBar10.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepFunctionPage.SPORT_STAR, (int) rating);
                mPage.notifyDataChanged();

            }
        });


        Button clear = (Button) view.findViewById(R.id.clear);

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar.setRating(0);
                mPage.getData().putInt(StepFunctionPage.POST_STAR, 0);
                mPage.notifyDataChanged();
            }
        });

        Button clear2 = (Button) view.findViewById(R.id.clear2);

        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar2.setRating(0);
                mPage.getData().putInt(StepFunctionPage.HOS_STAR, 0);
                mPage.notifyDataChanged();
            }
        });

        Button clear3 = (Button) view.findViewById(R.id.clear3);

        clear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar3.setRating(0);
                mPage.getData().putInt(StepFunctionPage.MARKET_STAR, 0);
                mPage.notifyDataChanged();
            }
        });

        Button clear4 = (Button) view.findViewById(R.id.clear4);

        clear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar4.setRating(0);
                mPage.getData().putInt(StepFunctionPage.DEPART_STAR, 0);
                mPage.notifyDataChanged();
            }
        });

        Button clear5 = (Button) view.findViewById(R.id.clear5);

        clear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar5.setRating(0);
                mPage.getData().putInt(StepFunctionPage.JS_STAR, 0);
                mPage.notifyDataChanged();
            }
        });

        Button clear6 = (Button) view.findViewById(R.id.clear6);

        clear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar6.setRating(0);
                mPage.getData().putInt(StepFunctionPage.JC_STAR, 0);
                mPage.notifyDataChanged();
            }
        });

        Button clear7 = (Button) view.findViewById(R.id.clear7);

        clear7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar7.setRating(0);
                mPage.getData().putInt(StepFunctionPage.JK_STAR, 0);
                mPage.notifyDataChanged();

            }
        });

        Button clear8 = (Button) view.findViewById(R.id.clear8);

        clear8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar8.setRating(0);
                mPage.getData().putInt(StepFunctionPage.LIB_STAR, 0);
                mPage.notifyDataChanged();

            }
        });

        Button clear9 = (Button) view.findViewById(R.id.clear9);

        clear9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar9.setRating(0);
                mPage.getData().putInt(StepFunctionPage.PARK_STAR, 0);
                mPage.notifyDataChanged();
            }
        });

        Button clear10 = (Button) view.findViewById(R.id.clear10);

        clear10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar10.setRating(0);
                mPage.getData().putInt(StepFunctionPage.SPORT_STAR, 0);
                mPage.notifyDataChanged();
            }
        });


        return view;


    }


}