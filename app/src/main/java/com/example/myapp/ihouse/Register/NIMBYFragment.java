package com.example.myapp.ihouse.register;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RatingBar;

import com.example.myapp.ihouse.R;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.util.ArrayList;

/**
 * Created by Alex on 2016/8/9.
 */
public class NIMBYFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static ArrayList<Integer> integers;
    NumberPicker np1 = null;
    NumberPicker np2 = null;
    private StepNIMBYPage mPage;
    protected PageFragmentCallbacks mCallbacks;
    private String mKey;


    public static NIMBYFragment newInstance(String pageNo) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, pageNo);
        NIMBYFragment fragment = new NIMBYFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_PAGE);
        mPage = (StepNIMBYPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nimbyfragment1, container, false);
        integers = new ArrayList<>();
        integers.add(0, -1);
        integers.add(1, -1);
        integers.add(2, -1);
        integers.add(3, -1);
        integers.add(4, -1);
        integers.add(5, -1);


        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.RatingBarId);
        final RatingBar ratingBar2 = (RatingBar) view.findViewById(R.id.RatingBarId2);
        final RatingBar ratingBar3 = (RatingBar) view.findViewById(R.id.RatingBarId3);
        final RatingBar ratingBar4 = (RatingBar) view.findViewById(R.id.RatingBarId4);

        np1 = (NumberPicker) view.findViewById(R.id.np1);
        np2 = (NumberPicker) view.findViewById(R.id.np2);

        final String[] values = new String[]{"0", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000", "1100", "1200", "1300", "1400", "1500", "1600", "1700", "1800", "1900", "2000"};

        np1.setMaxValue(values.length - 1);
        np1.setDisplayedValues(values);
        np1.setMinValue(0);
        np1.setValue(mPage.getData().getInt(StepNIMBYPage.NEAR_DIS_MIN_VAL));

        np2.setMaxValue(values.length - 1);
        np2.setMinValue(0);
        np2.setDisplayedValues(values);
        np2.setValue(mPage.getData().getInt(StepNIMBYPage.NEAR_DIS_MAX_VAL));

        np1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepNIMBYPage.NEAR_DIS_MIN, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepNIMBYPage.NEAR_DIS_MIN_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });

        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mPage.getData().putInt(StepNIMBYPage.NEAR_DIS_MAX, Integer.parseInt(values[newVal]));
                mPage.getData().putInt(StepNIMBYPage.NEAR_DIS_MAX_VAL, newVal);
                mPage.notifyDataChanged();
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepNIMBYPage.GAS_STAR, (int) rating);
                mPage.notifyDataChanged();
            }
        });

        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepNIMBYPage.FIRE_STAR, (int) rating);
                mPage.notifyDataChanged();
            }
        });

        ratingBar3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepNIMBYPage.DIE_STAR, (int) rating);
                mPage.notifyDataChanged();
            }
        });

        ratingBar4.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mPage.getData().putInt(StepNIMBYPage.AIR_STAR, (int) rating);
                mPage.notifyDataChanged();
            }
        });


        Button clear = (Button) view.findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar.setRating(0);
                mPage.getData().putInt(StepNIMBYPage.GAS_STAR, 0);
                mPage.notifyDataChanged();

            }
        });

        Button clear2 = (Button) view.findViewById(R.id.clear2);
        clear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar2.setRating(0);
                mPage.getData().putInt(StepNIMBYPage.FIRE_STAR, 0);
                mPage.notifyDataChanged();

            }
        });

        Button clear3 = (Button) view.findViewById(R.id.clear3);
        clear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar3.setRating(0);
                mPage.getData().putInt(StepNIMBYPage.DIE_STAR, 0);
                mPage.notifyDataChanged();

            }
        });

        Button clear4 = (Button) view.findViewById(R.id.clear4);
        clear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ratingBar4.setRating(0);
                mPage.getData().putInt(StepNIMBYPage.AIR_STAR, 0);
                mPage.notifyDataChanged();

            }
        });


        return view;


    }


}