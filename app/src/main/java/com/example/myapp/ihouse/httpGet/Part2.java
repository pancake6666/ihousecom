package com.example.myapp.ihouse.httpGet;

public class Part2 implements java.io.Serializable {
    private static final long serialVersionUID = 2197090843089363629L;
    private String FaultID;
    private String River_Name;
    private String L002_Name;
    private String TRAD;
    private String Univ_Name;
    private String A004id;
    private String A002id;
    private String Univid;
    private String B003D;
    private String AportD;
    private String TRA_Name;
    private String THSRD;
    private String B002id;
    private String LRoom;
    private String B003_Name;
    private Object Fault_Name;
    private String A003D;
    private String L003_Name;
    private String FWAYD;
    private String L004D;
    private String E001D;
    private String ID;
    private String TRAid;
    private String E001id;
    private String Space;
    private String L003id;
    private String L001id;
    private String MRT_Name;
    private String B002D;
    private String ParkRD;
    private Object THSR_Name;
    private Object A002_Name;
    private String ParkR_Name;
    private String Price;
    private String Car;
    private String Aportid;
    private String Airport_Name;
    private String A002D;
    private Object Fway_Name;
    private String L001D;
    private String SoilLiqID;
    private String B001_Name;
    private String UnivD;
    private String FWAYorder;
    private String A004_Name;
    private String MRTid;
    private String A003id;
    private String WC;
    private String B001D;
    private String A001id;
    private String B001id;
    private String E002id;
    private Object A003_Name;
    private String ParkRid;
    private String Elevator;
    private String B003id;
    private String THSRid;
    private String E002_Name;
    private String L002D;
    private String Room;
    private String A001D;
    private String B002_Name;
    private String L004id;
    private String L002id;
    private String E001_Name;
    private String MRTD;
    private String Type;
    private String L004_Name;
    private String ParkB_Name;
    private String Year;
    private String L001_Name;
    private String TRAorder;
    private String E002D;
    private String THSRorder;
    private String FloodID;
    private String FWAYid;
    private String L003D;
    private String A001_Name;
    private String A004D;
    private String MRTorder;

    public String getFaultID() {
        return this.FaultID;
    }

    public void setFaultID(String FaultID) {
        this.FaultID = FaultID;
    }

    public String getRiver_Name() {
        return this.River_Name;
    }

    public void setRiver_Name(String River_Name) {
        this.River_Name = River_Name;
    }

    public String getL002_Name() {
        return this.L002_Name;
    }

    public void setL002_Name(String L002_Name) {
        this.L002_Name = L002_Name;
    }

    public String getTRAD() {
        return this.TRAD;
    }

    public void setTRAD(String TRAD) {
        this.TRAD = TRAD;
    }

    public String getUniv_Name() {
        return this.Univ_Name;
    }

    public void setUniv_Name(String Univ_Name) {
        this.Univ_Name = Univ_Name;
    }

    public String getA004id() {
        return this.A004id;
    }

    public void setA004id(String A004id) {
        this.A004id = A004id;
    }

    public String getA002id() {
        return this.A002id;
    }

    public void setA002id(String A002id) {
        this.A002id = A002id;
    }

    public String getUnivid() {
        return this.Univid;
    }

    public void setUnivid(String Univid) {
        this.Univid = Univid;
    }

    public String getB003D() {
        return this.B003D;
    }

    public void setB003D(String B003D) {
        this.B003D = B003D;
    }

    public String getAportD() {
        return this.AportD;
    }

    public void setAportD(String AportD) {
        this.AportD = AportD;
    }

    public String getTRA_Name() {
        return this.TRA_Name;
    }

    public void setTRA_Name(String TRA_Name) {
        this.TRA_Name = TRA_Name;
    }

    public String getTHSRD() {
        return this.THSRD;
    }

    public void setTHSRD(String THSRD) {
        this.THSRD = THSRD;
    }

    public String getB002id() {
        return this.B002id;
    }

    public void setB002id(String B002id) {
        this.B002id = B002id;
    }

    public String getLRoom() {
        return this.LRoom;
    }

    public void setLRoom(String LRoom) {
        this.LRoom = LRoom;
    }

    public String getB003_Name() {
        return this.B003_Name;
    }

    public void setB003_Name(String B003_Name) {
        this.B003_Name = B003_Name;
    }

    public Object getFault_Name() {
        return this.Fault_Name;
    }

    public void setFault_Name(Object Fault_Name) {
        this.Fault_Name = Fault_Name;
    }

    public String getA003D() {
        return this.A003D;
    }

    public void setA003D(String A003D) {
        this.A003D = A003D;
    }

    public String getL003_Name() {
        return this.L003_Name;
    }

    public void setL003_Name(String L003_Name) {
        this.L003_Name = L003_Name;
    }

    public String getFWAYD() {
        return this.FWAYD;
    }

    public void setFWAYD(String FWAYD) {
        this.FWAYD = FWAYD;
    }

    public String getL004D() {
        return this.L004D;
    }

    public void setL004D(String L004D) {
        this.L004D = L004D;
    }

    public String getE001D() {
        return this.E001D;
    }

    public void setE001D(String E001D) {
        this.E001D = E001D;
    }

    public String getID() {
        return this.ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTRAid() {
        return this.TRAid;
    }

    public void setTRAid(String TRAid) {
        this.TRAid = TRAid;
    }

    public String getE001id() {
        return this.E001id;
    }

    public void setE001id(String E001id) {
        this.E001id = E001id;
    }

    public String getSpace() {
        return this.Space;
    }

    public void setSpace(String Space) {
        this.Space = Space;
    }

    public String getL003id() {
        return this.L003id;
    }

    public void setL003id(String L003id) {
        this.L003id = L003id;
    }

    public String getL001id() {
        return this.L001id;
    }

    public void setL001id(String L001id) {
        this.L001id = L001id;
    }

    public String getMRT_Name() {
        return this.MRT_Name;
    }

    public void setMRT_Name(String MRT_Name) {
        this.MRT_Name = MRT_Name;
    }

    public String getB002D() {
        return this.B002D;
    }

    public void setB002D(String B002D) {
        this.B002D = B002D;
    }

    public String getParkRD() {
        return this.ParkRD;
    }

    public void setParkRD(String ParkRD) {
        this.ParkRD = ParkRD;
    }

    public Object getTHSR_Name() {
        return this.THSR_Name;
    }

    public void setTHSR_Name(Object THSR_Name) {
        this.THSR_Name = THSR_Name;
    }

    public Object getA002_Name() {
        return this.A002_Name;
    }

    public void setA002_Name(Object A002_Name) {
        this.A002_Name = A002_Name;
    }

    public String getParkR_Name() {
        return this.ParkR_Name;
    }

    public void setParkR_Name(String ParkR_Name) {
        this.ParkR_Name = ParkR_Name;
    }

    public String getPrice() {
        return this.Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getCar() {
        return this.Car;
    }

    public void setCar(String Car) {
        this.Car = Car;
    }

    public String getAportid() {
        return this.Aportid;
    }

    public void setAportid(String Aportid) {
        this.Aportid = Aportid;
    }

    public String getAirport_Name() {
        return this.Airport_Name;
    }

    public void setAirport_Name(String Airport_Name) {
        this.Airport_Name = Airport_Name;
    }

    public String getA002D() {
        return this.A002D;
    }

    public void setA002D(String A002D) {
        this.A002D = A002D;
    }

    public Object getFway_Name() {
        return this.Fway_Name;
    }

    public void setFway_Name(Object Fway_Name) {
        this.Fway_Name = Fway_Name;
    }

    public String getL001D() {
        return this.L001D;
    }

    public void setL001D(String L001D) {
        this.L001D = L001D;
    }

    public String getSoilLiqID() {
        return this.SoilLiqID;
    }

    public void setSoilLiqID(String SoilLiqID) {
        this.SoilLiqID = SoilLiqID;
    }

    public String getB001_Name() {
        return this.B001_Name;
    }

    public void setB001_Name(String B001_Name) {
        this.B001_Name = B001_Name;
    }

    public String getUnivD() {
        return this.UnivD;
    }

    public void setUnivD(String UnivD) {
        this.UnivD = UnivD;
    }

    public String getFWAYorder() {
        return this.FWAYorder;
    }

    public void setFWAYorder(String FWAYorder) {
        this.FWAYorder = FWAYorder;
    }

    public String getA004_Name() {
        return this.A004_Name;
    }

    public void setA004_Name(String A004_Name) {
        this.A004_Name = A004_Name;
    }

    public String getMRTid() {
        return this.MRTid;
    }

    public void setMRTid(String MRTid) {
        this.MRTid = MRTid;
    }

    public String getA003id() {
        return this.A003id;
    }

    public void setA003id(String A003id) {
        this.A003id = A003id;
    }

    public String getWC() {
        return this.WC;
    }

    public void setWC(String WC) {
        this.WC = WC;
    }

    public String getB001D() {
        return this.B001D;
    }

    public void setB001D(String B001D) {
        this.B001D = B001D;
    }

    public String getA001id() {
        return this.A001id;
    }

    public void setA001id(String A001id) {
        this.A001id = A001id;
    }

    public String getB001id() {
        return this.B001id;
    }

    public void setB001id(String B001id) {
        this.B001id = B001id;
    }

    public String getE002id() {
        return this.E002id;
    }

    public void setE002id(String E002id) {
        this.E002id = E002id;
    }

    public Object getA003_Name() {
        return this.A003_Name;
    }

    public void setA003_Name(Object A003_Name) {
        this.A003_Name = A003_Name;
    }

    public String getParkRid() {
        return this.ParkRid;
    }

    public void setParkRid(String ParkRid) {
        this.ParkRid = ParkRid;
    }

    public String getElevator() {
        return this.Elevator;
    }

    public void setElevator(String Elevator) {
        this.Elevator = Elevator;
    }

    public String getB003id() {
        return this.B003id;
    }

    public void setB003id(String B003id) {
        this.B003id = B003id;
    }

    public String getTHSRid() {
        return this.THSRid;
    }

    public void setTHSRid(String THSRid) {
        this.THSRid = THSRid;
    }

    public String getE002_Name() {
        return this.E002_Name;
    }

    public void setE002_Name(String E002_Name) {
        this.E002_Name = E002_Name;
    }

    public String getL002D() {
        return this.L002D;
    }

    public void setL002D(String L002D) {
        this.L002D = L002D;
    }

    public String getRoom() {
        return this.Room;
    }

    public void setRoom(String Room) {
        this.Room = Room;
    }

    public String getA001D() {
        return this.A001D;
    }

    public void setA001D(String A001D) {
        this.A001D = A001D;
    }

    public String getB002_Name() {
        return this.B002_Name;
    }

    public void setB002_Name(String B002_Name) {
        this.B002_Name = B002_Name;
    }

    public String getL004id() {
        return this.L004id;
    }

    public void setL004id(String L004id) {
        this.L004id = L004id;
    }

    public String getL002id() {
        return this.L002id;
    }

    public void setL002id(String L002id) {
        this.L002id = L002id;
    }

    public String getE001_Name() {
        return this.E001_Name;
    }

    public void setE001_Name(String E001_Name) {
        this.E001_Name = E001_Name;
    }

    public String getMRTD() {
        return this.MRTD;
    }

    public void setMRTD(String MRTD) {
        this.MRTD = MRTD;
    }

    public String getType() {
        return this.Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getL004_Name() {
        return this.L004_Name;
    }

    public void setL004_Name(String L004_Name) {
        this.L004_Name = L004_Name;
    }

    public String getParkB_Name() {
        return this.ParkB_Name;
    }

    public void setParkB_Name(String ParkB_Name) {
        this.ParkB_Name = ParkB_Name;
    }

    public String getYear() {
        return this.Year;
    }

    public void setYear(String Year) {
        this.Year = Year;
    }

    public String getL001_Name() {
        return this.L001_Name;
    }

    public void setL001_Name(String L001_Name) {
        this.L001_Name = L001_Name;
    }

    public String getTRAorder() {
        return this.TRAorder;
    }

    public void setTRAorder(String TRAorder) {
        this.TRAorder = TRAorder;
    }

    public String getE002D() {
        return this.E002D;
    }

    public void setE002D(String E002D) {
        this.E002D = E002D;
    }

    public String getTHSRorder() {
        return this.THSRorder;
    }

    public void setTHSRorder(String THSRorder) {
        this.THSRorder = THSRorder;
    }

    public String getFloodID() {
        return this.FloodID;
    }

    public void setFloodID(String FloodID) {
        this.FloodID = FloodID;
    }

    public String getFWAYid() {
        return this.FWAYid;
    }

    public void setFWAYid(String FWAYid) {
        this.FWAYid = FWAYid;
    }

    public String getL003D() {
        return this.L003D;
    }

    public void setL003D(String L003D) {
        this.L003D = L003D;
    }

    public String getA001_Name() {
        return this.A001_Name;
    }

    public void setA001_Name(String A001_Name) {
        this.A001_Name = A001_Name;
    }

    public String getA004D() {
        return this.A004D;
    }

    public void setA004D(String A004D) {
        this.A004D = A004D;
    }

    public String getMRTorder() {
        return this.MRTorder;
    }

    public void setMRTorder(String MRTorder) {
        this.MRTorder = MRTorder;
    }
}
