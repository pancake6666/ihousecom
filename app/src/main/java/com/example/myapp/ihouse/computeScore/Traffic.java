/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.myapp.ihouse.computeScore;

/**
 * @author changyuen
 */
public class Traffic {

    private int type;//1 = mrt,2 = train,3 = thsr,4 = high,0 = real
    private int star;
    private int min;
    private int max;
    private double real;

    public Traffic() {
    }

    public Traffic(int type, int star, int min, int max, double real) {
        this.type = type;
        this.star = star;
        this.min = min;
        this.max = max;
        this.real = real;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }

}
