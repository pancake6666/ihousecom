package com.example.myapp.ihouse.mod;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.NumberPicker;
import android.widget.RadioGroup;

import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

/**
 * Created by changyuen on 2016/8/10.
 */
public class HouseModFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private MaterialBetterSpinner area_spinner;
    private NumberPicker np;
    private NumberPicker np2;
    private NumberPicker np3;
    private NumberPicker np4;
    private NumberPicker np5;
    private NumberPicker np6;
    private NumberPicker np7;
    private NumberPicker np8;
    private NumberPicker np9;
    private CheckBox ch12;
    private CheckBox ch34;
    private CheckBox ch56;
    private CheckBox ch789;
    private RadioGroup radioGroup;
    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    protected PageFragmentCallbacks mCallbacks;
    private int mKey;
    Hweight hw = ModActivity.hweight;


    public static HouseModFragment newInstance(int pageNo) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNo);
        HouseModFragment fragment = new HouseModFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getInt(ARG_PAGE);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.housemod, container, false);

        area_spinner = (MaterialBetterSpinner) view.findViewById(R.id.area_spinner);
        final String[] areaList = {"不限", "中正區", "大同區", "中山區", "松山區", "大安區", "萬華區", "信義區", "士林區", "北投區", "內湖區", "南港區", "文山區"};
        final ArrayAdapter<String> areaAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, areaList);

        area_spinner.setAdapter(areaAdapter);
        area_spinner.setText(hw.getSection());
        area_spinner.setHintTextColor(Color.BLACK);
        area_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (area_spinner.getText().toString().equals("不限")) {
                    hw.setSection("不限");
                } else {
                    hw.setSection(editable.toString());
                }
            }
        });

        radioGroup = (RadioGroup) view.findViewById(R.id.rgroup1);
        switch (hw.getType()) {
            case 1:
                radioGroup.check(R.id.apartment);
                break;
            case 2:
                radioGroup.check(R.id.building);
                break;
            case 4:
                radioGroup.check(R.id.villa);
                break;
            case 5:
                radioGroup.check(R.id.sky);
                break;
            case 6:
                radioGroup.check(R.id.Landmark);
                break;
            case -1:
                radioGroup.check(R.id.other);
                break;

        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.apartment:
                        hw.setType(1);
                        break;
                    case R.id.building:
                        hw.setType(2);
                        break;
                    case R.id.villa:
                        hw.setType(4);
                        break;
                    case R.id.sky:
                        hw.setType(5);
                        break;
                    case R.id.Landmark:
                        hw.setType(6);
                        break;
                    case R.id.other:
                        hw.setType(-1);
                        break;
                }
            }
        });


        radioGroup1 = (RadioGroup) view.findViewById(R.id.rgroup2);
        switch (hw.getElevator()) {
            case 1:
                radioGroup1.check(R.id.elevyes);
                break;
            case 0:
                radioGroup1.check(R.id.elevno);
                break;
            case -1:
                radioGroup1.check(R.id.elevnomatter);
                break;


        }
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.elevyes:
                        hw.setElevator(1);
                        break;
                    case R.id.elevno:
                        hw.setElevator(0);
                        break;
                    case R.id.elevnomatter:
                        hw.setElevator(-1);
                        break;
                }
            }
        });

        radioGroup2 = (RadioGroup) view.findViewById(R.id.rgroup3);
        switch (hw.getCar()) {
            case 1:
                radioGroup2.check(R.id.pspaceyes);
                break;
            case 0:
                radioGroup2.check(R.id.pspaceno);
                break;
            case -1:
                radioGroup2.check(R.id.pspacenomatter);
                break;


        }
        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.pspaceyes:
                        hw.setCar(1);
                        break;
                    case R.id.pspaceno:
                        hw.setCar(0);
                        break;
                    case R.id.pspacenomatter:
                        hw.setCar(-1);
                        break;
                }
            }
        });

        np = (NumberPicker) view.findViewById(R.id.numberpicker1); //單價下限值
        final String[] values = new String[]{"5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "0"};
        np.setMaxValue(values.length - 1);
        np.setMinValue(0);
        np.setDisplayedValues(values);
        np.setFocusable(true);
        np.setFocusableInTouchMode(true);
        np.setValue(FindIndex(values, hw.getPricemin()));
        System.out.println(FindIndex(values, hw.getPricemin()));
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setPricemin(Integer.valueOf(values[i1]));
            }
        });
        //this.setNumberPickerValue(0);


        np2 = (NumberPicker) view.findViewById(R.id.numberpicker2); //單價上限值
        final String[] values2 = new String[]{"10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60", "70", "80", "90", "100", "110"};
        np2.setMaxValue(values2.length - 1);
        np2.setMinValue(0);
        np2.setDisplayedValues(values2);
        np2.setFocusable(true);
        np2.setFocusableInTouchMode(true);
        np2.setValue(FindIndex(values2, hw.getPricemax()));
        np2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setPricemax(Integer.valueOf(values2[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        ch12 = (CheckBox) view.findViewById(R.id.checkboxnp12);
        if (hw.getPricemin() == 0 && hw.getPricemax() == 110) {
            ch12.setChecked(true);
            np.setEnabled(false);
            np2.setEnabled(false);
        }
        ch12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch12.isChecked()) {
                    np.setEnabled(false);
                    np2.setEnabled(false);
                    hw.setPricemin(0);
                    hw.setPricemax(110);
                } else {
                    np.setEnabled(true);
                    np2.setEnabled(true);
                    hw.setPricemin(Integer.parseInt(values[np.getValue()]));
                    hw.setPricemax(Integer.parseInt(values[np2.getValue()]));
                }
            }
        });

        np3 = (NumberPicker) view.findViewById(R.id.numberpicker3); //坪數下限值
        final String[] values3 = new String[]{"0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"};
        np3.setMaxValue(values3.length - 1);
        np3.setMinValue(0);
        np3.setDisplayedValues(values3);
        np3.setFocusable(true);
        np3.setFocusableInTouchMode(true);
        np3.setValue(FindIndex(values3, hw.getSpacemin()));
        np3.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setSpacemin(Integer.valueOf(values3[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        np4 = (NumberPicker) view.findViewById(R.id.numberpicker4); //坪數上限值
        final String[] values4 = new String[]{"10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"};
        np4.setMaxValue(values4.length - 1);
        np4.setMinValue(0);
        np4.setDisplayedValues(values4);
        np4.setFocusable(true);
        np4.setFocusableInTouchMode(true);
        np4.setValue(FindIndex(values4, hw.getSpacemax()));
        np4.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setSpacemax(Integer.valueOf(values4[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        ch34 = (CheckBox) view.findViewById(R.id.checkboxnp34);
        if (hw.getSpacemin() == 0 && hw.getSpacemax() == 60) {
            ch34.setChecked(true);
            np3.setEnabled(false);
            np4.setEnabled(false);
        }
        ch34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch34.isChecked()) {
                    np3.setEnabled(false);
                    np4.setEnabled(false);
                    hw.setSpacemin(0);
                    hw.setSpacemax(60);
                } else {
                    np3.setEnabled(true);
                    np4.setEnabled(true);
                    hw.setSpacemin(Integer.parseInt(values[np3.getValue()]));
                    hw.setSpacemax(Integer.parseInt(values[np4.getValue()]));
                }
            }
        });

        np5 = (NumberPicker) view.findViewById(R.id.numberpicker5); //屋齡下限值
        final String[] values5 = new String[]{"0", "5", "10", "15", "20", "25", "30", "35", "40"};
        np5.setMaxValue(values5.length - 1);
        np5.setMinValue(0);
        np5.setDisplayedValues(values5);
        np5.setFocusable(true);
        np5.setFocusableInTouchMode(true);
        np5.setValue(FindIndex(values5, hw.getYearmin()));
        np5.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setYearmin(Integer.valueOf(values5[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        np6 = (NumberPicker) view.findViewById(R.id.numberpicker6); //屋齡上限值
        final String[] values6 = new String[]{"10", "15", "20", "25", "30", "35", "40"};
        np6.setMaxValue(values6.length - 1);
        np6.setMinValue(0);
        np6.setDisplayedValues(values6);
        np6.setFocusable(true);
        np6.setFocusableInTouchMode(true);
        np6.setValue(FindIndex(values6, hw.getYearmax()));
        np6.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setYearmax(Integer.valueOf(values6[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        ch56 = (CheckBox) view.findViewById(R.id.checkboxnp56);
        if (hw.getYearmin() == 0 && hw.getYearmax() == 40) {
            ch56.setChecked(true);
            np5.setEnabled(false);
            np6.setEnabled(false);
        }
        ch56.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch56.isChecked()) {
                    np5.setEnabled(false);
                    np6.setEnabled(false);
                    hw.setYearmin(0);
                    hw.setYearmax(40);
                } else {
                    np5.setEnabled(true);
                    np6.setEnabled(true);
                    hw.setYearmin(Integer.parseInt(values5[np5.getValue()]));
                    hw.setYearmax(Integer.parseInt(values6[np6.getValue()]));
                }
            }
        });

        np7 = (NumberPicker) view.findViewById(R.id.numberpicker7); //房數
        final String[] values7 = new String[]{"0", "1", "2", "3", "4", "5", "6"};
        np7.setMaxValue(values7.length - 1);
        np7.setMinValue(0);
        np7.setDisplayedValues(values7);
        np7.setFocusable(true);
        np7.setFocusableInTouchMode(true);
        np7.setValue(FindIndex(values7, hw.getRoom()));
        np7.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setRoom(Integer.valueOf(values7[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        np8 = (NumberPicker) view.findViewById(R.id.numberpicker8); //廳數
        final String[] values8 = new String[]{"0", "1", "2", "3"};
        np8.setMaxValue(values8.length - 1);
        np8.setMinValue(0);
        np8.setDisplayedValues(values8);
        np8.setFocusable(true);
        np8.setFocusableInTouchMode(true);
        np8.setValue(FindIndex(values8, hw.getLRoom()));
        np8.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setLRoom(Integer.valueOf(values8[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        np9 = (NumberPicker) view.findViewById(R.id.numberpicker9); //衛數
        final String[] values9 = new String[]{"0", "1", "2", "3", "4"};
        np9.setMaxValue(values9.length - 1);
        np9.setMinValue(0);
        np9.setDisplayedValues(values9);
        np9.setFocusable(true);
        np9.setFocusableInTouchMode(true);
        np9.setValue(FindIndex(values9, hw.getWC()));
        np9.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                hw.setWC(Integer.valueOf(values9[i1]));
            }
        });
        //this.setNumberPickerValue(0);

        ch789 = (CheckBox) view.findViewById(R.id.checkboxnp789);
        if (hw.getWC() == -1) {
            ch789.setChecked(true);
            np7.setEnabled(false);
            np8.setEnabled(false);
            np9.setEnabled(false);
        }
        ch789.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ch789.isChecked()) {
                    np7.setEnabled(false);
                    np8.setEnabled(false);
                    np9.setEnabled(false);
                    hw.setRoom(-1);
                    hw.setLRoom(-1);
                    hw.setWC(-1);
                } else {
                    np7.setEnabled(true);
                    np8.setEnabled(true);
                    np9.setEnabled(true);
                    hw.setRoom(Integer.parseInt(values7[np7.getValue()]));
                    hw.setLRoom(Integer.parseInt(values8[np8.getValue()]));
                    hw.setWC(Integer.parseInt(values9[np9.getValue()]));
                }
            }
        });

        return view;
    }

    public void setNumberPickerValue(int val) {
        if (np != null) {
            np.setValue(val / 10 - 1);
        }
    }


    public int FindIndex(String[] values, Integer num) {
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(num.toString())) {
                index = i;
            }

        }
        return index;
    }

}
