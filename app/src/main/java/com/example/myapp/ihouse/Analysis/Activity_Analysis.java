package com.example.myapp.ihouse.analysis;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapp.ihouse.MainActivity;
import com.example.myapp.ihouse.R;
import com.example.myapp.ihouse.computeScore.RealHouse;
import com.example.myapp.ihouse.contact_us.Contactus;
import com.example.myapp.ihouse.httpGet.GetURL;
import com.example.myapp.ihouse.httpGet.Hweight;
import com.example.myapp.ihouse.httpGet.MyData;
import com.example.myapp.ihouse.httpGet.MyLoveSample;
import com.example.myapp.ihouse.httpGet.Part2;
import com.example.myapp.ihouse.httpGet.RealEstateDate;
import com.example.myapp.ihouse.map.MapsActivity;
import com.example.myapp.ihouse.mod.ModActivity;

public class Activity_Analysis extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private TabLayout mTabLayout;
    public SharedPreferences sharedPreferences;
    public static int m_id;
    private ProgressDialog progressDialog;
    private GetURL httpGet;
    public Hweight hweight;
    public static RealEstateDate realEstateDate;
    public static RealHouse realHouse;
    public static MyData myData;
    public static Part2 part2;
    public static MyLoveSample myLoveSample;
    Bundle getData;
    public DrawerLayout drawerLayout;
    public NavigationView navigationView;


    private int[] mTabsIcons = {
            R.drawable.housenew,
            R.drawable.searchnew,
            R.drawable.warningnew
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);


        getData = this.getIntent().getExtras();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        m_id = sharedPreferences.getInt("ID", 0);
        Analysis_IdealRateFragment.m_id = m_id;
        httpGet = new GetURL();
        hweight = (Hweight) this.getIntent().getSerializableExtra("hweight");
        Analysis_IdealRateFragment.hweight = hweight;
        Score_ExpListAdapter.hweight = hweight;
        LifeTotal_ExpListAdapter.hweight = hweight;
//        progressDialog = ProgressDialog.show(Activity_Analysis.this, "", "");
//        runnable.run();
//        progressDialog.dismiss();
        System.out.println(hweight.getPricemax());
        System.out.println(hweight.getSpacemax());
        System.out.println(hweight.getCar());
        System.out.println(this.getIntent().hasExtra("fromHou"));
        System.out.println(this.getIntent().hasExtra("fromCus"));
        System.out.println(this.getIntent().hasExtra("fromLovePart1"));
        //addType = getData.getInt("addType");
        if (this.getIntent().hasExtra("fromHou")) {
            realEstateDate = (RealEstateDate) this.getIntent().getSerializableExtra("fromHou");//實價登錄
            Analysis_IdealRateFragment.realEstateDate = realEstateDate;
            Analysis_LifeTotalFragment.realEstateDate = realEstateDate;
            Analysis_NaturalFragment.realEstateDate = realEstateDate;

            Score_ExpListAdapter.realEstateDate = realEstateDate;
            LifeTotal_ExpListAdapter.realEstateDate = realEstateDate;
        } else if (this.getIntent().hasExtra("fromCus")) {
            // 自加房屋
            myData = (MyData) this.getIntent().getSerializableExtra("fromCus");
            realHouse = (RealHouse) this.getIntent().getSerializableExtra("fromCusReal");
            Analysis_NaturalFragment.myData = myData;
            Analysis_LifeTotalFragment.myData = myData;
            Analysis_IdealRateFragment.myData = myData;
            Analysis_IdealRateFragment.realHouse = realHouse;

            Score_ExpListAdapter.myData = myData;
            Score_ExpListAdapter.realHouse = realHouse;

            LifeTotal_ExpListAdapter.myData = myData;
            LifeTotal_ExpListAdapter.realHouse = realHouse;

        } else if (this.getIntent().hasExtra("fromLovePart1") && this.getIntent().hasExtra("fromLovePart2")) {  //從我的最愛
            myLoveSample = (MyLoveSample) this.getIntent().getSerializableExtra("fromLovePart1");
            part2 = (Part2) this.getIntent().getSerializableExtra("fromLovePart2");
            Analysis_IdealRateFragment.myLoveSample = myLoveSample;
            Analysis_NaturalFragment.myLoveSample = myLoveSample;
            Analysis_LifeTotalFragment.myLoveSample = myLoveSample;

            Analysis_IdealRateFragment.part2 = part2;
            Analysis_NaturalFragment.part2 = part2;
            Analysis_LifeTotalFragment.part2 = part2;

            Score_ExpListAdapter.part2 = part2;
            LifeTotal_ExpListAdapter.part2 = part2;
        }


        // Setup the viewPager
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        if (viewPager != null)
            viewPager.setAdapter(pagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        if (mTabLayout != null) {
            mTabLayout.setupWithViewPager(viewPager);

            for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }

            mTabLayout.getTabAt(0).getCustomView().setSelected(true);
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                return true;
            case R.id.favorite:
                startActivity(new Intent(getApplicationContext(), MapsActivity.class));
                finish();
                return true;
            case R.id.mod:
                startActivity(new Intent(getApplicationContext(), ModActivity.class));
                finish();
                return true;
            case R.id.contact:
                startActivity(new Intent(getApplicationContext(), Contactus.class));
                finish();
                return true;
            default:
                return true;
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public final int PAGE_COUNT = 3;

        private final String[] mTabsTitle = {"理想屋型達成率", "生活機能總分", "自然災害分布"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View view = LayoutInflater.from(Activity_Analysis.this).inflate(R.layout.user_tab, null);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(mTabsTitle[position]);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(mTabsIcons[position]);
            return view;
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return Analysis_IdealRateFragment.newInstance(0);
                case 1:
                    return Analysis_LifeTotalFragment.newInstance(1);
                case 2:
                    return Analysis_NaturalFragment.newInstance(2);
            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }
    }


//    class getHweigth extends AsyncTask<Integer, Integer, Boolean> {
//
//        @Override
//        protected Boolean doInBackground(Integer... integer) {
//            hweight = httpGet.getHweight(m_id);
//            return true;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = ProgressDialog.show(Activity_Analysis.this, "", "");
//        }
//
//        @Override
//        protected void onPostExecute(Boolean finish) {
//            if (finish) {
//                progressDialog.dismiss();
//            }
//        }
//    }


    @Override
    public void onBackPressed() {
        realEstateDate = null;
        myData = null;
        part2 = null;
        myLoveSample = null;
        this.getIntent().removeExtra("fromCus");
        this.getIntent().removeExtra("fromHou");
        this.getIntent().removeExtra("fromLovePart1");
        super.onBackPressed();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hweight = httpGet.getHweight(m_id);
            Analysis_IdealRateFragment.hweight = hweight;
            Score_ExpListAdapter.hweight = hweight;
            LifeTotal_ExpListAdapter.hweight = hweight;
        }
    };

}
