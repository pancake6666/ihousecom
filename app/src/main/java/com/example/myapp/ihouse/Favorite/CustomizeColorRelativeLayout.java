package com.example.myapp.ihouse.favorite;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.example.myapp.ihouse.R;

public class CustomizeColorRelativeLayout extends RelativeLayout {
    private LayoutInflater inflater;
    private Paint paint = null;

    public CustomizeColorRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.relativelayout_customize_color, this, true);
        paint = new Paint();
        setWillNotDraw(false);

    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(Color.BLUE);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(30);
        canvas.drawLine(0, 0, 0, getMeasuredHeight(), paint);
        invalidate();

    }
}
