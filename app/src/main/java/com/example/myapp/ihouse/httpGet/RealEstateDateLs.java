package com.example.myapp.ihouse.httpGet;

public class RealEstateDateLs implements java.io.Serializable {
    private static final long serialVersionUID = 3325311183667306332L;
    private String P_Lat;
    private String P_Lng;
    private String P_X;
    private String P_Name;
    private String Pcode5_ID;
    private String P_Y;
    private String P_ID;
    private String P_Type;

    public String getP_Lat() {
        return this.P_Lat;
    }

    public void setP_Lat(String P_Lat) {
        this.P_Lat = P_Lat;
    }

    public String getP_Lng() {
        return this.P_Lng;
    }

    public void setP_Lng(String P_Lng) {
        this.P_Lng = P_Lng;
    }

    public String getP_X() {
        return this.P_X;
    }

    public void setP_X(String P_X) {
        this.P_X = P_X;
    }

    public String getP_Name() {
        return this.P_Name;
    }

    public void setP_Name(String P_Name) {
        this.P_Name = P_Name;
    }

    public String getPcode5_ID() {
        return this.Pcode5_ID;
    }

    public void setPcode5_ID(String Pcode5_ID) {
        this.Pcode5_ID = Pcode5_ID;
    }

    public String getP_Y() {
        return this.P_Y;
    }

    public void setP_Y(String P_Y) {
        this.P_Y = P_Y;
    }

    public String getP_ID() {
        return this.P_ID;
    }

    public void setP_ID(String P_ID) {
        this.P_ID = P_ID;
    }

    public String getP_Type() {
        return this.P_Type;
    }

    public void setP_Type(String P_Type) {
        this.P_Type = P_Type;
    }
}
