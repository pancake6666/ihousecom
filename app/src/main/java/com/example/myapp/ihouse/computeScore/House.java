/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.myapp.ihouse.computeScore;

/**
 * @author changyuen
 */
public class House {
    //不拘 max,min=-1
    //以上 max=-1
    //有 1 無 0
    private int price_min;
    private int price_max;
    private double space_min;
    private double space_max;
    private int years_min;
    private int years_max;
    private int type;
    private int room;
    private int l_room;
    private int bath;
    private int car;
    private int elec;


    public House() {
    }

    public House(int price_min, int price_max, double space_min, double space_max, int years_min, int years_max, int type, int room, int l_room, int bath, int car, int elec) {
        this.price_min = price_min;
        this.price_max = price_max;
        this.space_min = space_min;
        this.space_max = space_max;
        this.years_min = years_min;
        this.years_max = years_max;
        this.type = type;
        this.room = room;
        this.l_room = l_room;
        this.bath = bath;
        this.car = car;
        this.elec = elec;
    }

    public double getSpace_min() {
        return space_min;
    }

    public void setSpace_min(double space_min) {
        this.space_min = space_min;
    }

    /**
     * @return the price
     */
    public int getPrice_max() {
        return price_max;
    }

    /**
     * @param price_max the price to set
     */
    public void setPrice_max(int price_max) {
        this.price_max = price_max;
    }

    /**
     * @return the space
     */
    public double getSpace_max() {
        return space_max;
    }

    /**
     * @param space_max the space to set
     */
    public void setSpace_max(double space_max) {
        this.space_max = space_max;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the room
     */
    public int getRoom() {
        return room;
    }

    /**
     * @param room the room to set
     */
    public void setRoom(int room) {
        this.room = room;
    }

    /**
     * @return the l_room
     */
    public int getL_room() {
        return l_room;
    }

    /**
     * @param l_room the l_room to set
     */
    public void setL_room(int l_room) {
        this.l_room = l_room;
    }

    /**
     * @return the bath
     */
    public int getBath() {
        return bath;
    }

    /**
     * @param bath the bath to set
     */
    public void setBath(int bath) {
        this.bath = bath;
    }

    /**
     * @return the car
     */
    public int getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(int car) {
        this.car = car;
    }

    /**
     * @return the elec
     */
    public int getElec() {
        return elec;
    }

    /**
     * @param elec the elec to set
     */
    public void setElec(int elec) {
        this.elec = elec;
    }

    /**
     * @return the years
     */
    public int getYears_max() {
        return years_max;
    }

    /**
     * @param years_max the years to set
     */
    public void setYears_max(int years_max) {
        this.years_max = years_max;
    }

    public int getPrice_min() {
        return price_min;
    }

    public void setPrice_min(int price_min) {
        this.price_min = price_min;
    }

    public int getYears_min() {
        return years_min;
    }

    public void setYears_min(int years_min) {
        this.years_min = years_min;
    }

}
